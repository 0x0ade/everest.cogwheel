//@ts-check
const REGEX_BACKSLASH = /\\/g;
const interop = module.exports = {
  /**
   * @param {string} path
   */
  encodeFSPathToURL: (path) =>
    `cog://fs/${interop.encodeFSPath(path)}`,

  /**
   * @param {string} path
   */
  encodeFSPath: (path) =>
    path.replace(REGEX_BACKSLASH, "/").split("/").map(path => encodeURI(path)).join("/"),

  /**
   * @param {string} path
   */
  decodeFSPath: (path) =>
    path.split("/").map(path => decodeURI(path)).join("/"),

};
