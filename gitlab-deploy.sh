#!/bin/bash
INDEX="public/index.html" 

mkdir -p public

cp -r dist/*{-mac.zip,.exe,.exe.blockmap,latest*.yml} public 2> /dev/null || :

echo '<!DOCTYPE html><html><body><h3>files</h3><ul>' >> $INDEX

for f in public/*; do
  echo "<li><a href=\""${f:7}"\">"${f}"</a></li>" >> $INDEX
done

echo '</ul></body></html>' >> $INDEX
