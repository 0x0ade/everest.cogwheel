//@ts-check
import { CogBinEl, binprop } from "./binel.js";

/**
 * @typedef {import("./map.js").CogMap} CogMap
 * @typedef {import("./map.js").CogMapLevel} CogMapLevel
 */

export class CogMapEntityNode {
  /**
   * @param {CogMapEntity} entity
   * @param {number | CogBinEl} binOrX
   * @param {number} [y]
   */
  constructor(entity, binOrX, y) {
    this._entity = entity;
    if (binOrX instanceof CogBinEl) {
      this._bin = binOrX;
    } else {
      this._bin = new CogBinEl(null, null);
      this._bin.setAttr("x", binOrX);
      this._bin.setAttr("y", y);
    }

    /** @type {number} */
    this._index; Object.defineProperty(this, "_index", {
      configurable: true,
      enumerable: true,
  
      get: () => this._entity._nodes.indexOf(this)
    });

    /** @type {number} */
    this.x; binprop(this, "x");
    /** @type {number} */
    this.y; binprop(this, "y");
  }
}

export class CogMapEntity {
  static Node = CogMapEntityNode;

  /**
   * @param {CogMapLevel} level
   * @param {string} group
   * @param {string | CogBinEl} binOrName
   * @param {any} [data]
   */
  constructor(level, group, binOrName, data) {
    this._level = level;
    this._group = group;
    if (binOrName instanceof CogBinEl) {
      this._bin = binOrName;
    } else {
      this._bin = new CogBinEl(null, null);
      this._bin.name = binOrName;
      this._bin.setAttr("id", -1);
      this._bin.setAttr("x", 0);
      this._bin.setAttr("y", 0);
    }

    if (data)
      for (let i in data)
        this._bin.setAttr(i, data[i]);
    
    this.binRead();
  }

  binRead() {
    for (let entry of this._bin.attributes.entries()) {
      this[entry[0]] = entry[1];
    }

    this._nodes = this._bin.children.map(bin => new CogMapEntity.Node(this, bin));
  }

  binWrite() {
    for (let key of Reflect.ownKeys(this)) {
      key = key.toString();
      if (key[0] === "_")
        continue;
      this._bin.setAttr(key, this[key]);
    }

    this._bin.children = this._nodes.map(node => node._bin);
  }
}
