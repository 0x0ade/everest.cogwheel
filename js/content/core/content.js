//@ts-check
import { ERR_NOIMPL } from "./common.js";
import { CogBinaryReader, CogDataSrc } from "./data.js";

export class CogContent {
  /**
   * @param {CogDataSrc[]} sources
   */
  constructor(...sources) {
    this.sources = sources;
    /** @type {Map<string, CogAsset>} */    
    this.assets = new Map();
    /** @type {Map<string, typeof CogAsset>} */    
    this.types = new Map();
    /** @type {Map<string, CogAssetLoader>} */    
    this.loaders = new Map();

    this.debugLoad = false;
  }

  /**
   * @param {string} path
   * @returns {Promise<CogBinaryReader>}
   */
  async _loadBinary(path) {
    let elist = [];
    for (let src of this.sources)
      try {
        return await src.loadBinary(path);
      } catch (e) {
        elist.push(e);
      }
    throw elist;
  }

  /**
   * @param {string} path
   * @returns {Promise<string>}
   */
  async _loadText(path) {
    let elist = [];
    for (let src of this.sources)
      try {
        return await src.loadText(path);
      } catch (e) {
        elist.push(e);
      }
    throw elist;
  }

  /**
   * @template T
   * @param {T & typeof CogAsset} type
   * @param {string} id
   * @param {string} [path]
   * @param {any[]} args
   * @returns {Promise<T["prototype"]>}
   */
  async load(type, id, path, ...args) {
    /** @type {CogAsset} */
    let asset;
    // @ts-ignore T must inherit CogAsset, checked by param.
    if (id && (asset = this.get(type, id)))
      // @ts-ignore Type is checked by get.
      return asset;

    if (this.debugLoad)
      console.log("[CogContent]", "Loading", type.name, id, path, ...args);
    
    let loader = type.prototype.loader;
    this.types.set(type.name, type);
    this.loaders.set(loader.constructor.name, loader);

    let timeStart = performance.now();

    asset = Reflect.construct(type, [this, path]);

    if (!loader.isParser) {
      let loaded = false;
      let elist = [];
      for (let src of this.sources)
        try {
          asset = await loader.load(this, asset, src, path, ...args);
          loaded = true;
        } catch (e) {
          elist.push(e);
        }
      if (!loaded)
        throw elist;

    } else {
      asset = await loader.load(this, asset, null, path, ...args);
    }

    let timeEnd = performance.now();
    console.debug("[perf:CogContent]", "Loaded", type.name, id, path, ...args, "in", `${timeEnd - timeStart}ms`);

    if (id)
      this.assets.set(id, asset);
    // @ts-ignore Type is guaranteed to match.
    return asset;
  }

  /**
   * @template T
   * @param {T & typeof CogAsset} type
   * @param {string} id
   * @returns {T["prototype"]}
   */
  get(type, id) {
    let value = this.assets.get(id);
    if (!value)
      return null;
    if (type !== value.constructor)
      throw new Error(`Expected ${type.name}, got ${value.constructor.name}`);
    // @ts-ignore Type is checked above.
    return value;
  }
}

/**
 * @abstract
 */
export class CogAsset {
  /**
   * @abstract
   * @returns {CogAssetLoader}
   */
  get loader() {
    throw new Error(ERR_NOIMPL);
  }

  /**
   * @abstract
   * @returns {string}
   */
  get dataURL() {
    throw new Error(ERR_NOIMPL);
  }

  /**
   * @virtual
   * @returns {boolean}
   */
  get isPixiLoadable() {
    return Object.getOwnPropertyDescriptor(this, "dataURL") !== Object.getOwnPropertyDescriptor(CogAsset.prototype, "dataURL") && !!this.path;
  }

  /**
   * @param {CogContent} content
   * @param {string} path
   */
  constructor(content, path) {
    if (content)
      this._content = content;
    if (path)
      this.path = path;
  }
}

/**
 * @abstract
 * @template T
 */
export class CogAssetLoader {
  /**
   * @virtual
   * @returns {boolean}
   */
  get isParser() {
    return false;
  }

  /**
   * @abstract
   * @param {CogContent} content
   * @param {T} asset
   * @param {CogDataSrc} src
   * @param {string} path
   * @param {any[]} args
   * @returns {Promise<T>}
   */
  async load(content, asset, src, path, ...args) {
    throw new Error(ERR_NOIMPL);
  }
}
