//@ts-check
import { ERR_NOIMPL } from "./common.js";

/**
 * @abstract
 */
export class CogDataSrc {
  /**
   * @abstract
   * @param {string} path
   * @returns {Promise<CogBinaryReader>}
   */
  async loadBinary(path) {
    throw new Error(ERR_NOIMPL);
  }

  /**
   * @abstract
   * @param {string} path
   * @returns {Promise<string>}
   */
  async loadText(path) {
    throw new Error(ERR_NOIMPL);
  }
}

export class CogDataWebSrc extends CogDataSrc {
  /** @param {(path: string) => string} transformer */
  constructor(transformer) {
    super();
    this.transformer = transformer || (p => p);
  }

  /**
   * @param {string} path
   * @returns {Promise<CogBinaryReader>}
   */
  async loadBinary(path) {
    let url = this.transformer(path);
    if (!url)
      throw new Error(`Cannot read ${path}: Not supported`);
    return new CogBinaryReader(new DataView(await fetch(url).then(async r => {
      if (!r.ok)
        throw new Error(`Cannot read ${path}: ${r.statusText}`);
      if (r.headers.get("Content-Type") === "text/error")
        throw new Error(`Cannot read ${path}: ${await r.text()}`);
      // console.log("[CogDataWebSrc]", "GET BINARY:", path, "=>", url);
      return await r.arrayBuffer();
    })));
  }

  /**
   * @param {string} path
   * @returns {Promise<string>}
   */
  async loadText(path) {
    let url = this.transformer(path);
    if (!url)
      throw new Error(`Cannot read ${path}: Not supported`);
    return await fetch(url).then(async r => {
      if (!r.ok)
        throw new Error(`Cannot read ${path}: ${r.statusText}`);
      if (r.headers.get("Content-Type") === "text/error")
        throw new Error(`Cannot read ${path}: ${await r.text()}`);
      // console.log("[CogDataWebSrc]", "GET TEXT:", path, "=>", url);
      return await r.text();
    });
  }
}

export class CogDataFSSrc extends CogDataSrc {
  /**
   * @param {import("fs")} fs
   * @param {(path: string) => string} transformer
   */
  constructor(fs, transformer) {
    super();
    this.fs = fs;
    this.transformer = transformer || (p => p);
  }

  /**
   * @param {string} path
   * @returns {Promise<CogBinaryReader>}
   */
  async loadBinary(path) {
    let pathFull = this.transformer(path);
    if (!pathFull)
      throw new Error(`Cannot read ${path}: Not supported`);
    return new CogBinaryReader(new DataView(await new Promise((resolve, reject) => this.fs.readFile(pathFull, null, (err, data) => {
      if (err)
        return reject(err);
      resolve(data.buffer);
    }))));
  }

  /**
   * @param {string} path
   * @returns {Promise<string>}
   */
  async loadText(path) {
    let pathFull = this.transformer(path);
    if (!pathFull)
      throw new Error(`Cannot read ${path}: Not supported`);
    return await new Promise((resolve, reject) => this.fs.readFile(pathFull, null, (err, data) => {
      if (err)
        return reject(err);
      resolve(data.toString());
    }));
  }
}

export class CogBinaryReader {
  /**
   * @param {DataView} view
   */
  constructor(view) {
    this.view = view;
    this.position = 0;
    this.littleEndian = true;
  }

  readLEB128() {
    let result = 0;
    let shift = 0;
    while (true) {
      let byte = this.readByte();
      result |= (byte & 0x7F) << shift;
      if ((byte & 0x80) === 0)
        break;
      shift += 7;
    }
    return result;
  }

  readString() {
    return String.fromCharCode(...this.readBytes(this.readLEB128()));
  }
  
  readRLEString() {
    let bytes = this.readBytes(this.readInt16());
    let s = "";
    for (let i = 0; i < bytes.length; i += 2) {
      let c = String.fromCharCode(bytes[i + 1]);
      for (let j = bytes[i] - 1; j > -1; j--)
        s += c;
    }
    return s;
  }

  readBoolean() {
    return this.readByte() > 0;
  }

  /**
   * @param {number} length
   */
  readBytes(length) {
    let byteOffset = this.position;
    this.position += length;
    return new Uint8Array(this.view.buffer, byteOffset, length);
  }

  /**
  * Gets the Float32 value at the specified byte offset from the start of the view. There is
  * no alignment constraint; multi-byte values may be fetched from any offset.
  * @param {boolean} [littleEndian]
  * @returns {number}
  */
  readSingle(littleEndian) {
    let byteOffset = this.position;
    this.position += 4;
    return this.view.getFloat32(byteOffset, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * Gets the Float64 value at the specified byte offset from the start of the view. There is
   * no alignment constraint; multi-byte values may be fetched from any offset.
   * @param {boolean} [littleEndian]
   * @returns {number}
   */
  readDouble(littleEndian) {
    let byteOffset = this.position;
    this.position += 8;
    return this.view.getFloat64(byteOffset, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * Gets the Int8 value at the specified byte offset from the start of the view. There is
   * no alignment constraint; multi-byte values may be fetched from any offset.
   * @returns {number}
   */
  readSByte() {
    let byteOffset = this.position;
    this.position += 1;
    return this.view.getInt8(byteOffset);
  }
  
  /**
   * Gets the Int16 value at the specified byte offset from the start of the view. There is
   * no alignment constraint; multi-byte values may be fetched from any offset.
   * @param {boolean} [littleEndian]
   * @returns {number}
   */
  readInt16(littleEndian) {
    let byteOffset = this.position;
    this.position += 2;
    return this.view.getInt16(byteOffset, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  /**
   * Gets the Int32 value at the specified byte offset from the start of the view. There is
   * no alignment constraint; multi-byte values may be fetched from any offset.
   * @param {boolean} [littleEndian]
   * @returns {number}
   */
  readInt32(littleEndian) {
    let byteOffset = this.position;
    this.position += 4;
    return this.view.getInt32(byteOffset, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * Gets the Uint8 value at the specified byte offset from the start of the view. There is
   * no alignment constraint; multi-byte values may be fetched from any offset.
   * @returns {number}
   */
  readByte() {
    let byteOffset = this.position;
    this.position += 1;
    return this.view.getUint8(byteOffset);
  }
  
  /**
   * Gets the Uint16 value at the specified byte offset from the start of the view. There is
   * no alignment constraint; multi-byte values may be fetched from any offset.
   * @param {boolean} [littleEndian]
   * @returns {number}
   */
  readUint16(littleEndian) {
    let byteOffset = this.position;
    this.position += 2;
    return this.view.getUint16(byteOffset, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * Gets the Uint32 value at the specified byte offset from the start of the view. There is
   * no alignment constraint; multi-byte values may be fetched from any offset.
   * @param {boolean} [littleEndian]
   * @returns {number}
   */
  readUint32(littleEndian) {
    let byteOffset = this.position;
    this.position += 4;
    return this.view.getUint32(byteOffset, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }

}

export class CogBinaryWriter {
  /**
   * @param {DataView} view
   */
  constructor(view) {
    this.view = view;
    this.position = 0;
    this.littleEndian = true;
    /** @type {{ type: string, byteOffset: number, value: number, actionOrLE: (() => void) | boolean }[]} */
    this._queue = [];
  }

  flush() {
    if (!this.view)
      this.view = new DataView(new ArrayBuffer(this.position));
    const { view, _queue: queue } = this;
    this._queue = [];

    for (let { type, byteOffset, value, actionOrLE } of queue) {
      if (!type && actionOrLE instanceof Function) {
        actionOrLE();
        continue;
      }

      /** @type {typeof view["setUint8"]} */
      let func = view["set" + type];
      func.call(view, byteOffset, value, actionOrLE);
    }

  }

  /**
   * @param {string} type
   * @param {number} byteOffset
   * @param {number} value
   * @param {(() => void) | boolean} [actionOrLE]
   */
  enqueue(type, byteOffset, value, actionOrLE) {
    this._queue.push({ type, byteOffset, value, actionOrLE });
  }

  /**
   * @param {number} value
   */
  writeLEB128(value) {
    do {
      let byte = value & 0x7F;
      value >>= 7;
      if (value !== 0)
        byte |= 0x80;
      this.enqueue("Int8", this.position++, byte);
    } while (value != 0);
  }

  /**
   * @param {string} value
   */
  writeString(value) {
    this.writeLEB128(value.length);
    for (let i = 0; i < value.length; i++) {
      this.enqueue("Int8", this.position++, value.charCodeAt(i));
    }
  }
  
  /**
   * @param {string} value
   */
  writeRLEString(value) {
    let positionStart = this.position;

    // Write the length afterwards.
    this.position += 2;

    for (let i = 0; i < value.length; i++) {
      let c = value.charCodeAt(i);
      let repeat = 1;
      for (; i < value.length; i++) {
        let cr = value.charCodeAt(i + 1);
        if (cr !== c)
          break;
        repeat++;
      }
      while (repeat > 255) {
        this.enqueue("Int8", this.position++, 255);
        this.enqueue("Int8", this.position++, c);
        repeat -= 255;
      }
      this.enqueue("Int8", this.position++, repeat);
      this.enqueue("Int8", this.position++, c);
    }

    // Update the total length.
    this.enqueue("Int16", positionStart, this.position - (positionStart + 2), this.littleEndian);
  }

  /**
   * @param {boolean} value
   */
  writeBoolean(value) {
    this.enqueue("Uint8", this.position++, value ? 1 : 0);
  }

  /**
   * @param {Uint8Array} data
   */
  writeBytes(data) {
    let byteOffset = this.position;
    this.position += data.length;
    this.enqueue("Bytes", null, null, () => {
      new Uint8Array(this.view.buffer).set(data, byteOffset);
    });
  }

  /**
   * @param {number} value
   * @param {boolean} [littleEndian]
   */
  writeSingle(value, littleEndian) {
    let byteOffset = this.position;
    this.position += 4;
    this.enqueue("Float32", byteOffset, value, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * @param {number} value
   * @param {boolean} [littleEndian]
   */
  writeDouble(value, littleEndian) {
    let byteOffset = this.position;
    this.position += 8;
    this.enqueue("Float64", byteOffset, value, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * @param {number} value
   */
  writeSByte(value) {
    let byteOffset = this.position;
    this.position += 1;
    this.enqueue("Int8", byteOffset, value);
  }
  
  /**
   * @param {number} value
   * @param {boolean} [littleEndian]
   */
  writeInt16(value, littleEndian) {
    let byteOffset = this.position;
    this.position += 2;
    this.enqueue("Int16", byteOffset, value, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }

  /**
   * @param {number} value
   * @param {boolean} [littleEndian]
   */
  writeInt32(value, littleEndian) {
    let byteOffset = this.position;
    this.position += 4;
    this.enqueue("Int32", byteOffset, value, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * @param {number} value
   */
  writeByte(value) {
    let byteOffset = this.position;
    this.position += 1;
    this.enqueue("Uint8", byteOffset, value);
  }
  
  /**
   * @param {number} value
   * @param {boolean} [littleEndian]
   */
  writeUint16(value, littleEndian) {
    let byteOffset = this.position;
    this.position += 2;
    this.enqueue("Uint16", byteOffset, value, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }
  
  /**
   * @param {number} value
   * @param {boolean} [littleEndian]
   */
  writeUint32(value, littleEndian) {
    let byteOffset = this.position;
    this.position += 4;
    this.enqueue("Uint32", byteOffset, value, littleEndian !== undefined ? littleEndian : this.littleEndian);
  }

}
