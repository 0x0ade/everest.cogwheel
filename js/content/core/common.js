//@ts-check

export const ERR_NOIMPL = "Not implemented";
export const REGEX_BACKSLASH = /\\/g;
