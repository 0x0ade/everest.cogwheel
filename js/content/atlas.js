//@ts-check
import { CogContent, CogAsset, CogAssetLoader, CogDataSrc, REGEX_BACKSLASH } from "./core.js";
import { CogSubtexture, CogTexture } from "./texture.js";

export class CogAtlas extends CogAsset {
  get loader() {
    return CogAtlasLoader;
  }

  /**
   * @param {CogContent} content
   * @param {string} path
   */
  constructor(content, path) {
    super(content, path);

    /** @type {CogSubtexture[]} */
    this.sources = [];
    /** @type {Map<string, CogSubtexture>} */
    this.textures = new Map();
  }

  /**
   * @param {string} key
   */
  get(key) {
    return this.textures.get(key.replace(REGEX_BACKSLASH, "/"));
  }
  /**
   * @param {string} key
   * @param {CogSubtexture} value
   */
  set(key, value) {
    this.textures.set(key.replace(REGEX_BACKSLASH, "/"), value);
  }

  /**
   * @param {string} key
   * @returns {CogSubtexture[]}
   */
  getSubtextures(key) {
    /**
     * @param {string} key
     * @param {number} i
     */
    const getSubtexture = (key, i) => {
      let texture;
      if (i === 0 && (texture = this.get(key)))
        return texture;
      
      let suffix = i.toString();
      const maxLength = suffix.length + 6;
      while (suffix.length < maxLength) {
        texture = this.get(key + suffix);
        if (texture)
          return texture;
        suffix = "0" + suffix;
      }

      return null;
    }

    let list = [];
    // Celeste caches the result per key. Maybe Cogwheel should do the same.
    for (let i = 0; true; i++) {
      let texture = getSubtexture(key, i);
      if (!texture)
        break;
      list[i] = texture;
    }
    return list;
  }
}

export var CogAtlasLoader = new (class CogAtlasLoader extends CogAssetLoader {
  /**
   * @param {CogContent} content
   * @param {CogAtlas} atlas
   * @param {CogDataSrc} src
   * @param {string} path
   * @param {string} format
   * @returns {Promise<CogAtlas>}
   */
  async load(content, atlas, src, path, format) {
    let reader = await src.loadBinary(path + ".meta");

    if (format === "Packer") {
      reader.readInt32(); // ???
      reader.readString(); // ???
      reader.readInt32(); // ???

      let sources = reader.readInt16();
      for (let sourcei = 0; sourcei < sources; sourcei++) {
        let sourcePath = path.slice(0, path.lastIndexOf("/") + 1) + reader.readString() + ".data";
        let sourceTex = await content.load(CogTexture, null, sourcePath);
        let sourceSubTex = new CogSubtexture(sourceTex);
        atlas.sources.push(sourceSubTex);

        let subs = reader.readInt16();
        for (let subi = 0; subi < subs; subi++) {
          let name = reader.readString().replace(REGEX_BACKSLASH, "/");
          let clipX = reader.readInt16();
          let clipY = reader.readInt16();
          let clipWidth = reader.readInt16();
          let clipHeight = reader.readInt16();
          let offsX = reader.readInt16();
          let offsY = reader.readInt16();
          let width = reader.readInt16();
          let height = reader.readInt16();
          atlas.set(name, new CogSubtexture(
              sourceSubTex, name, [clipX, clipY, clipWidth, clipHeight],
              [-offsX, -offsY],
              width, height
          ));
        }
      }

    } else if (format === "PackerNoAtlas") {
      throw new Error("PackerNoAtlas atlases are currently not supported");
      
    } else {
      throw new Error(`Unknown atlas format: ${format}`);
    }

    return atlas;
  }
})();
