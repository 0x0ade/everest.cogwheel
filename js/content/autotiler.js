//@ts-check
import { CogContent, CogAsset, CogAssetLoader, CogDataSrc } from "./core.js";
import { CogAtlas } from "./atlas.js";
import { CogSubtexture } from "./texture.js";

let parser = new DOMParser();

export class CogAutotilerTerrainType {
  /**
   * @param {string} id
   */
  constructor(id) {
    this.id = id;

    /** @type {Set<string>} */
    this.ignores = new Set();
    /** @type {CogAutotiler.Masked[]} */
    this.masked = [];
    this.center = new CogAutotiler.Tiles();
    this.padded = new CogAutotiler.Tiles();
  }

  /**
   * @param {string} id
   */
  ignore(id) {
    return this.id !== id && (this.ignores.has(id) || this.ignores.has("*"));
  }
}

export class CogAutotilerMasked {
  constructor() {
    /** @type {number[]} */
    this.mask = new Array(9);
    this.tiles = new CogAutotiler.Tiles();
  }
}

export class CogAutotilerTiles {
  constructor() {
    /** @type {CogSubtexture[]} */
    this.textures = [];
    /** @type {string[]} */
    this.overlapSprites = [];
    this.hasOverlays = false;
  }
}

export class CogAutotiler extends CogAsset {
  static TerrainType = CogAutotilerTerrainType;
  static Masked = CogAutotilerMasked;
  static Tiles = CogAutotilerTiles;

  get loader() {
    return CogAutotilerLoader;
  }

  /**
   * @param {CogContent} content
   * @param {string} path
   */
  constructor(content, path) {
    super(content, path);
    
    /** @type {number[][]} */
    this.levelBounds = [];
    /** @type {Map<string, CogAutotiler.TerrainType>} */
    this.lookup = new Map();
    /** @type {number[]} */
    this.adjacent = [];
  }
}

export class CogTileset extends CogAsset {
  /**
   * @param {CogSubtexture} texture
   * @param {number} tileWidth
   * @param {number} tileHeight
   */
  constructor(texture, tileWidth, tileHeight) {
    super(null, null);

    this.texture = texture;
    this.tileWidth = tileWidth;
    this.tileHeight = tileHeight;

    /** @type {CogSubtexture[][]} */
    this.tiles = [];
    for (let x = 0; x < texture.width / tileWidth; x++) {
      this.tiles[x] = [];
      for (let y = 0; y < texture.height / tileHeight; y++) {
        this.tiles[x][y] = texture.getSubtexture(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
      }
    }

  }
}

export var CogAutotilerLoader = new (class CogAutotilerLoader extends CogAssetLoader {
  /**
   * @param {CogContent} content
   * @param {CogAutotiler} autotiler
   * @param {CogDataSrc} src
   * @param {string} path
   * @returns {Promise<CogAutotiler>}
   */
  async load(content, autotiler, src, path) {
    let text = await src.loadText(path);
    /** @type {XMLDocument} */
    let doc = parser.parseFromString(text, "text/xml");

    /** @type {Map<string, Element>} */
    let cache = new Map();

    for (let el of doc.getElementsByTagName("Tileset")) {
      let id = el.getAttribute("id");
      if (id.length !== 1)
        throw new Error("Tileset ID must be one byte long");
      
      let tileset = new CogTileset(content.get(CogAtlas, "Gameplay").get(`tilesets/${el.getAttribute("path")}`), 8, 8);
      let terrainType = new CogAutotiler.TerrainType(id);
      this.readInto(terrainType, tileset, el);

      let copyId = el.getAttribute("copy");
      if (copyId) {
        let copy = cache.get(copyId);
        if (!copy)
          throw new Error("Copied tilesets must be defined before the tilesets that copy them");
        this.readInto(terrainType, tileset, copy);
      }

      let ignores = el.getAttribute("ignores");
      if (ignores) {
        for (let ignoredId of ignores.split(","))
          if (ignoredId.length > 0)
            terrainType.ignores.add(ignoredId);
      }

      cache.set(id, el);
      autotiler.lookup.set(id, terrainType);
    }
    
    return autotiler;
  }

  /**
   * @param {CogAutotiler.TerrainType} data
   * @param {CogTileset} tileset
   * @param {Element} xml
   */
  readInto(data, tileset, xml) {
    for (let el = xml.firstElementChild; el; el = el.nextElementSibling) {
      let mask = el.getAttribute("mask");
      /** @type {CogAutotiler.Tiles} */
      let tiles;
      if (mask === "center") {
        tiles = data.center;

      } else if (mask === "padding") {
        tiles = data.padded;

      } else {
        let masked = new CogAutotiler.Masked();
        tiles = masked.tiles;
        let i = -1;
        for (let c of mask) {
          if (c === "0")
            masked.mask[++i] = 0;
          else if (c === "1")
            masked.mask[++i] = 1;
          else if (c === "x" || c === "X")
            masked.mask[++i] = 2;
        }
        data.masked.push(masked);
      }

      for (let tile of el.getAttribute("tiles").split(";")) {
        let xy = tile.split(",").map(c => parseInt(c));
        tiles.textures.push(tileset.tiles[xy[0]][xy[1]]);
      }

      let sprites = el.getAttribute("sprites");
      if (sprites) {
        tiles.overlapSprites.push(...sprites.split(","));
        tiles.hasOverlays = true;
      }
    }

    data.masked.sort((a, b) => {
      let x = 0, y = 0;
      for (let i = 0; i < 9; i++) {
        if (a.mask[i] === 2)
          x++;
        if (b.mask[i] === 2)
          y++;
      }
      return x - y;
    });
  }
})();
