import { CogBinEl } from "./binel.js";
import { VirtualArray } from "./core.js";
import { CogMapLevel } from "./map.js";

export class CogMapEntityNode {
  constructor(entity: CogMapEntity, binOrX: number | CogBinEl, y?: number);

  _entity: CogMapEntity;
  _bin: CogBinEl;
  _index: number;
  x: number;
  y: number;
}

export class CogMapEntity {
  static Node = CogMapEntityNode;

  constructor(level: CogMapLevel, group: string, binOrName: string | CogBinEl, data?: any);

  _level: CogMapLevel;
  _group: string;
  _bin: CogBinEl;

  _nodes: CogMapEntityNode[];

  id: number;
  x: number;
  y: number;

  [name: string]: any;

  binRead() : void;
  binWrite() : void;
}
