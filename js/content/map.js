//@ts-check
import { CogContent, CogAsset, CogAssetLoader, CogDataSrc } from "./core.js";
import { CogBinEl } from "./binel.js";
import { CogMapEntity, CogMapEntityNode } from "./map_.js";
export { CogMapEntity, CogMapEntityNode }

const REGEX_CARRIAGERETURN = /\r/g;
const REGEX_NUL = /\0/g;
const REGEX_ZEROESATEND = /0+(?=$|\n)/g
const REGEX_EMPTYLINESATEND = /\n+$/g

/**
 * @param {CogMapLevel} level
 * @param {string} group
 */
function entitylistRead(level, group) {
  let listBin = level._bin.get(group);
  if (!listBin)
    return [];
  return listBin.children.map(bin => new CogMapEntity(level, group, bin));
}

/**
 * @param {CogMapLevel} level
 * @param {string} group
 */
function entitylistWrite(level, group) {
  let bin = new CogBinEl(null, null);
  bin.name = group;

  /** @type {typeof level.entities} */
  let list = level[group];
  bin.children = list.map(entity => {
    entity.binWrite();
    return entity._bin;
  });

  level._bin.set(bin);
}

export class CogMapLevelTiles {
  /**
   * @param {CogMap.Level} level
   * @param {string} type
   */
  constructor(level, type) {
    this._level = level;
    this._type = type;
    this._bin = level._bin.get(type);
  }

  binRead() {
    const fallback = "0";
    const { width: levelWidth, height: levelHeight } = this._level;
    const width = levelWidth / 8;
    const height = levelHeight / 8;
    const count = width * height;

    /** @type {string[]} */
    const raw = this.raw = new Array(count);

    let text = this._bin.innerText;
    if (!text)
      text = new Array(height).join("\n");
    
    const rows = text.replace(REGEX_NUL, fallback).replace(REGEX_CARRIAGERETURN, "").split("\n");
    for (let y = 0; y < height; y++) {
      let row = y < rows.length ? rows[y] : "";
      for (let x = 0; x < row.length && x < width; x++) {
        raw[y * width + x] = row[x];
      }
      for (let x = row.length; x < width; x++) {
        raw[y * width + x] = fallback;
      }
    }
    for (let y = rows.length; y < height; y++) {
      for (let x = 0; x < width; x++) {
        raw[y * width + x] = fallback;
      }
    }
  }

  binWrite() {
    const { width: levelWidth, height: levelHeight } = this._level;
    const width = levelWidth / 8;
    const height = levelHeight / 8;
    let text = this.raw.map((id, i) => id + ((i + 1) % width === 0 ? "\n" : "")).join("");
    this._bin.innerText = text.replace(REGEX_ZEROESATEND, "").replace(REGEX_EMPTYLINESATEND, "");
  }

  /**
   * @param {number} x
   * @param {number} y
   */
  isInside(x, y) {
    const { width: levelWidth, height: levelHeight } = this._level;
    const width = levelWidth / 8;
    const height = levelHeight / 8;
    return (
      0 <= x && x < width &&
      0 <= y && y < height
    );
  }

  /**
   * @param {number} x
   * @param {number} y
   */
  getIndex(x, y) {
    x = Math.floor(x);
    y = Math.floor(y);
    const { width: levelWidth, height: levelHeight } = this._level;
    const width = levelWidth / 8;
    const height = levelHeight / 8;
    return (
      0 <= x && x < width &&
      0 <= y && y < height
    ) ?
      x + y * width :
      Math.max(0, Math.min(x, width - 1)) + Math.max(0, Math.min(y, height - 1)) * width
  }

  get width() {
    return this._level.width / 8;
  }
  get height() {
    return this._level.height / 8;
  }

  /**
   * @param {number} x
   * @param {number} y
   */
  get(x, y) {
    return this.raw[this.getIndex(x, y)];
  }

  /**
   * @param {number} x
   * @param {number} y
   * @param {string} value
   */
  set(x, y, value) {
    this.raw[this.getIndex(x, y)] = value;
  }

}

export class CogMapLevel {
  /**
   * @param {CogBinEl} bin
   */
  constructor(bin) {
    this._bin = bin;

    this.solids = new CogMap.LevelTiles(this, "solids");
    this.bg = new CogMap.LevelTiles(this, "bg");

    this.binRead();
  }

  binRead() {
    /** @type {string} */
    this.name = this._bin.getAttr("name");

    /** @type {number} */
    this.editorColorIndex = this._bin.getAttr("c");

    /** @type {number} */
    this.x = this._bin.getAttr("x");
    /** @type {number} */
    this.y = this._bin.getAttr("y");
    /** @type {number} */
    this.width = this._bin.getAttr("width");
    /** @type {number} */
    this.height = this._bin.getAttr("height");

    // Some maps ship with dimensions which aren't disivible by 8.
    this.width = Math.ceil(this.width / 8) * 8;
    this.height = Math.ceil(this.height / 8) * 8;

    this.solids.binRead();
    this.bg.binRead();

    this.bgdecals = entitylistRead(this, "bgdecals");
    this.fgdecals = entitylistRead(this, "fgdecals");

    this.entities = entitylistRead(this, "entities");
    this.triggers = entitylistRead(this, "triggers");
  }

  binWrite() {
    this._bin.setAttr("name", this.name);

    this._bin.setAttr("c", this.editorColorIndex);

    this._bin.setAttr("x", this.x);
    this._bin.setAttr("y", this.y);
    this._bin.setAttr("width", this.width);
    this._bin.setAttr("height", this.height);

    this.solids.binWrite();
    this.bg.binWrite();

    entitylistWrite(this, "bgdecals");
    entitylistWrite(this, "fgdecals");
    
    entitylistWrite(this, "entities");
    entitylistWrite(this, "triggers");
  }

  /**
   * @param {number} x
   * @param {number} y
   */
  isInside(x, y) {
    const { x: ox, y: oy, width, height } = this;
    return (
      ox <= x && x < ox + width &&
      oy <= y && y < oy + height
    );
  }
}

export class CogMap extends CogAsset {
  static Level = CogMapLevel;
  static LevelTiles = CogMapLevelTiles;
  static Entity = CogMapEntity;
  static EntityNode = CogMapEntityNode;

  get loader() {
    return CogMapLoader;
  }

  /**
   * @param {CogContent} content
   * @param {string} path
   */
  constructor(content, path) {
    super(content, path);

    /** @type {CogBinEl} */
    this._bin = null;

    /** @type {WeakMap<CogBinEl, CogMap.Level>} */
    this.__cache = new WeakMap();
  }

  [Symbol.iterator]() {
    let tmp = this[Symbol.iterator] = ((self) => function*() {
      for (let el of self._bin.get("levels").children)
        yield self.get(el);
    })(this);
    return tmp();
  }

  get bounds() {
    let bounds = [0, 0, 0, 0];
    for (let level of this) {
      bounds[0] = Math.min(bounds[0], level.x);
      bounds[1] = Math.min(bounds[1], level.y);
      bounds[2] = Math.max(bounds[2], level.x + level.width);
      bounds[3] = Math.max(bounds[3], level.y + level.height);
    }
    return bounds;
  }
  
  /**
   * @param {string | CogBinEl} nameOrEl
   */
  get(nameOrEl) {
    let bin = typeof nameOrEl === "string" ? this._bin.get("levels").find(el => el.getAttr("name") === nameOrEl) : nameOrEl;
    if (!bin)
      return null;

    let level = this.__cache.get(bin);
    if (level)
      return level;

    level = new CogMap.Level(bin);
    this.__cache.set(bin, level);
    return level;
  }

  /**
   * @param {CogMap.Level} value
   */
  set(value) {
    let levels = this._bin.get("levels");
    let index = levels.children.findIndex(el => el.getAttr("name") === value.name);
    if (index === -1)
      levels.children.push(value._bin);
    else
      levels.children[index] = value._bin;
  }

  /** @returns {ArrayBuffer} */
  toBinary() {
    for (let level of this)
      level.binWrite();

    return this._bin.toBinary();
  }

}

export var CogMapLoader = new (class CogMapLoader extends CogAssetLoader {
  /**
   * @returns {boolean}
   */
  get isParser() {
    return true;
  }

  /**
   * @param {CogContent} content
   * @param {CogMap} map
   * @param {CogDataSrc} src
   * @param {string} path
   * @returns {Promise<CogMap>}
   */
  async load(content, map, src, path) {
    map._bin = await content.load(CogBinEl, null, path);

    // Trim away all lvl_ level name prefixes.
    let name;
    for (let el of map._bin.get("levels").children)
      if ((name = el.getAttr("name")).startsWith("lvl_"))
        el.setAttr("name", name.slice(4));

    return map;
  }
})();
