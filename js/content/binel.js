//@ts-check
import { CogContent, CogAsset, CogAssetLoader, CogDataSrc, CogBinaryWriter } from "./core.js";

/**
 * @param {{ _bin: CogBinEl; }} self
 * @param {string} from
 * @param {string | symbol} [to]
 */
export function binprop(self, from, to) {
  to = to || from;
  let vCache = self._bin.getAttr(from);
  Object.defineProperty(self, to, {
    configurable: true,
    enumerable: true,

    get: () => vCache,
    set: v => { self._bin.setAttr(from, v); vCache = v; }
  });
}

export class CogBinEl extends CogAsset {
  get loader() {
    return CogBinElLoader;
  }

  /**
   * @param {CogContent} content
   * @param {string} path
   */
  constructor(content, path) {
    super(content, path);

    /** @type {string} */
    this.package = null;
    /** @type {string} */
    this.name = null;
    /** @type {Map<string, any>} */
    this.attributes = new Map();
    /** @type {Array<CogBinEl>} */
    this.children = [];

    /** @type {string} */
    this.innerText; Object.defineProperty(this, "innerText", {
      configurable: true,
      enumerable: true,
  
      get: () => this.getAttr("innerText"),
      set: v => this.setAttr("innerText", v)
    });
  }

  /**
   * @param {string} name
   * @returns {CogBinEl}
   */
  get(name) {
    if (!this.children)
      return undefined;

    for (let child of this.children)
      if (child.name === name)
        return child;

    return undefined;
  }

  /**
   * @param {CogBinEl} value
   */
  set(value) {
    if (!this.children)
      this.children = [];
    
    let index = this.children.findIndex(el => el.name === value.name);
    if (index === -1)
      this.children.push(value);
    else
      this.children[index] = value;
  }

  /**
   * @param {string} name
   * @returns {any}
   */
  getAttr(name) {
    return this.attributes.get(name);
  }

  /**
   * @param {string} name
   * @param {any} value
   */
  setAttr(name, value) {
    return this.attributes.set(name, value);
  }

  /**
   * @param {(el: CogBinEl) => boolean} predicate
   * @returns {CogBinEl}
   */
  find(predicate) {
    for (let child of this.children)
      if (predicate(child))
        return child;
    return null;
  }

  /** @returns {ArrayBuffer} */
  toBinary() {
    let writer = new CogBinaryWriter(null);

    writer.writeString("CELESTE MAP");
    writer.writeString(this.package || "");

    /** @type {Map<string, number>} */
    let stringLookupGenMap = new Map();
    /**
     * @param {string} string
     */
    function stringLookupAdd(string) {
      let count;
      stringLookupGenMap.get(string);
      if (!count)
        count = 0;
      stringLookupGenMap.set(string, count + 1);
    }

    /**
     * @param {CogBinEl} el
     */
    function preWriteElement(el) {
      stringLookupAdd(el.name);
      for (let entry of el.attributes.entries()) {
        let { 0: key, 1: value } = entry;
        stringLookupAdd(key);
        if (key !== "innerText" && typeof(value) === "string")
          stringLookupAdd(value);
      }
      for (let child of el.children)
        preWriteElement(child);
    }

    preWriteElement(this);

    let stringLookupEntries = Array
      .from(stringLookupGenMap.entries())
      .sort((a, b) => b[1] - a[1])
    writer.writeInt16(stringLookupEntries.length);
    for (let i = 0; i < stringLookupEntries.length; i++)
      writer.writeString(stringLookupEntries[i][0]);

    let stringLookupMap = new Map(stringLookupEntries.map((entry, i) => {
      /** @type {[string, number]} */
      let kvp = [entry[0], i];
      return kvp;
    }));

    /**
     * @param {CogBinEl} [el]
     */
    function writeElement(el) {
      writer.writeInt16(stringLookupMap.get(el.name));

      writer.writeByte(el.attributes.size);
      for (let entry of el.attributes.entries()) {
        let { 0: key, 1: value } = entry;

        writer.writeInt16(stringLookupMap.get(key));

        if (key === "innerText") {
          writer.writeByte(7);
          writer.writeRLEString(value);

        } else if (typeof(value) === "boolean") {
          writer.writeByte(0);
          writer.writeBoolean(value);

        } else if (typeof(value) === "string") {
          let lookup = stringLookupMap.get(value);
          if (lookup) {
            writer.writeByte(5);
            writer.writeInt16(lookup);

          } else {
            writer.writeByte(6);
            writer.writeString(value);
          }
          
        } else if (typeof(value) === "number") {
          if (Number.isInteger(value)) {
            if (0 <= value && value <= 255) {
              writer.writeByte(1);
              writer.writeByte(value);

            } else if (-32768 <= value && value <= 32767) {
              writer.writeByte(2);
              writer.writeInt16(value);

            } else {
              writer.writeByte(3);
              writer.writeInt32(value);
            }

          } else {
            writer.writeByte(4);
            writer.writeSingle(value);
          }

        } else {
          throw new Error("Unsupported property");
        }
      }

      writer.writeInt16(el.children.length);
      for (let child of el.children) {
        writeElement(child);
      }
    }
    
    writeElement(this);

    writer.flush();
    return writer.view.buffer;
  }
}

export var CogBinElLoader = new (class CogBinElLoader extends CogAssetLoader {
  /**
   * @param {CogContent} content
   * @param {CogBinEl} el
   * @param {CogDataSrc} src
   * @param {string} path
   * @returns {Promise<CogBinEl>}
   */
  async load(content, el, src, path) {
    let reader = await src.loadBinary(path);

    reader.readString(); // ???
    el.package = reader.readString();

    let stringLookup = new Array(reader.readInt16());
    for (let i = 0; i < stringLookup.length; i++)
      stringLookup[i] = reader.readString();

    /**
     * @param {CogBinEl} [el]
     * @returns {CogBinEl}
     */
    function readElement(el) {
      el = el || new CogBinEl(null, null);

      el.name = stringLookup[reader.readInt16()];

      let attributes = reader.readByte();
      for (let i = 0; i < attributes; i++) {
        let key = stringLookup[reader.readInt16()];
        let value;
        switch (reader.readByte()) {
          case 0: value = reader.readBoolean(); break;
          case 1: value = reader.readByte(); break;
          case 2: value = reader.readInt16(); break;
          case 3: value = reader.readInt32(); break;
          case 4: value = reader.readSingle(); break;
          case 5: value = stringLookup[reader.readInt16()]; break;
          case 6: value = reader.readString(); break;
          case 7: value = reader.readRLEString(); break;
        }
        el.setAttr(key, value);
      }

      let children = reader.readInt16();
      el.children = new Array(children);
      for (let i = 0; i < children; i++)
        el.children[i] = readElement();
      
      return el;
    }
    
    return readElement(el);
  }
})();
