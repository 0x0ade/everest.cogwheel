//@ts-check
import { CogContent, CogAsset, CogAssetLoader, CogDataSrc } from "./core.js";

/** @type {[number, number, number, number]} */
const COLORMASKS_RAW = [
  0xFF000000,
  0x00FF0000,
  0x0000FF00,
  0x000000FF
];

/** @type {[number, number, number, number]} */
const COLORMASKS_RAW_REORDER = [
  0x000000FF,
  0x0000FF00,
  0x00FF0000,
  0xFF000000
];

/** @type {[number, number, number, number]} */
const COLORMASKS_PNG = [
  0x000000FF,
  0x0000FF00,
  0x00FF0000,
  0xFF000000
];

/**
 * @param {string} path
 * @param {ArrayBuffer} buffer
 * @param {[number, number, number, number]} colormasks
 * @param {number} texWidth
 * @param {number} texHeight
 * @param {number} [doX]
 * @param {number} [doY]
 * @param {number} [crX]
 * @param {number} [crY]
 * @param {number} [crW]
 * @param {number} [crH]
 * @param {number} [w]
 * @param {number} [h]
 */
function dataToBMPB64(path, buffer, colormasks, texWidth, texHeight, doX, doY, crX, crY, crW, crH, w, h) {
  w = w || texWidth;
  h = h || texHeight;
  let size = w * h * 4;
  let basic = size === buffer.byteLength && !doX && !doY && !crX && !crY && !crW && !crH;
  let bmpBuffer = new ArrayBuffer(122 + 1 + (basic ? 0 : size));
  let bmpView = new DataView(bmpBuffer);

  // BITMAPFILEHEADER:
  bmpView.setUint16(0, 0x4D42, true); // "BM"
  bmpView.setUint32(2, size, true);
  bmpView.setUint32(6, 0, true); // Reserved.
  bmpView.setUint32(10, 123, true); // Data offset / header size. 122 + 1 padding.

  // BITMAPV4HEADER:
  bmpView.setUint32(14, 108, true); // sizeof(BITMAPV4HEADER)
  bmpView.setInt32(18, w, true);
  bmpView.setInt32(22, -h, true);
  bmpView.setUint16(26, 1, true); // Color planes.
  bmpView.setUint16(28, 32, true); // Bits per pixel.
  bmpView.setUint32(30, 3, true); // BI_BITFIELDS
  bmpView.setUint32(34, size, true);
  bmpView.setInt32(38, 0, true); // X pixels per meter.
  bmpView.setInt32(42, 0, true); // Y pixels per meter.
  bmpView.setUint32(46, 0, true); // Number of colors in color table.
  bmpView.setUint32(50, 0, true); // "0 means all colors are important" --Wikipedia
  bmpView.setUint32(54, colormasks[0], true); // R bitmask
  bmpView.setUint32(58, colormasks[1], true); // G bitmask
  bmpView.setUint32(62, colormasks[2], true); // B bitmask
  bmpView.setUint32(66, colormasks[3], true); // A bitmask
  bmpView.setUint32(70, 0x57696E20, true); // "Win "


  if (basic)
    // String.fromCharCode(...bmp) fails for long arrays.
    return `data:image/bmp;path=${encodeURIComponent(path)};base64,${btoa(String.fromCharCode(...new Uint8Array(bmpBuffer)))}${btoa(Array.prototype.slice.apply(new Uint8Array(buffer)).map(b => String.fromCharCode(b)).join(""))}`;

  let raw = new Uint8Array(buffer);
  let bmp = new Uint8Array(bmpBuffer, 123);
  doX = doX || 0;
  doY = doY || 0;
  crX = crX || 0;
  crY = crY || 0;
  crW = crW || w;
  crH = crH || h;
  for (let y = 0; y < crH; y++) {
    let to = (doX + (y + doY) * w) * 4;
    let from = (crX + (y + crY) * texWidth) * 4;
    bmp.set(raw.subarray(from, from + crW * 4), to);
  }

  // String.fromCharCode(...bmp) fails for long arrays.
  return `data:image/bmp;path=${encodeURIComponent(path)};base64,${btoa(Array.prototype.slice.apply(new Uint8Array(bmpBuffer)).map(b => String.fromCharCode(b)).join(""))}`;
}

export class CogTexture extends CogAsset {
  /**
   * @returns {CogAssetLoader}
   */
  get loader() {
    return CogTextureLoader;
  }

  /**
   * @returns {string}
   */
  get dataURL() {
    return dataToBMPB64(this.path, this.buffer, this.colorMasks, this.width, this.height);
  }

  /**
   * @param {CogContent} content
   * @param {string} path
   */
  constructor(content, path) {
    super(content, path);

    this.width = 0;
    this.height = 0;

    /** @type {ArrayBuffer} */
    this.buffer = null;
    this.bufferPreserve = true;

    this.colorMasks = COLORMASKS_RAW_REORDER;
  }
}

export class CogSubtexture extends CogAsset {
  /**
   * @returns {string}
   */
  get path() {
    if (!this.atlasPath)
      return null;

    let parent = "";
    if (this.parent instanceof CogAsset && this.parent.path)
      parent = this.parent.path + "/";
    return parent + this.atlasPath;
  }

  /**
   * @returns {string}
   */
  get dataURL() {
    return dataToBMPB64(this.path, this.texture.buffer, this.texture.colorMasks, this.texture.width, this.texture.height, ...this.drawOffset, ...this.clipRect, this.width, this.height);
  }

  /**
   * @param {CogSubtexture | CogTexture} parent
   * @param {string} atlasPath
   * @param {[number, number, number, number]} clipRect
   * @param {[number, number]} drawOffset
   * @param {number} width
   * @param {number} height
   */
  constructor(parent, atlasPath = null, clipRect = null, drawOffset = null, width = null, height = null) {
    super(null, null);
    
    this.parent = parent;
    this.atlasPath = atlasPath;

    /** @type {number} */
    this.width = width || parent.width;
    /** @type {number} */
    this.height = height || parent.height;
    this.clipRect = clipRect || [0, 0, this.width, this.height];
    this.drawOffset = drawOffset || [0, 0];
  }

  /** @returns {CogTexture} */
  get texture() {
    if (this.parent instanceof CogTexture)
      return this.parent;
    return this.parent.texture;
  }

  /**
   * @param {number} x
   * @param {number} y
   * @param {number} width
   * @param {number} height
   */
  getSubtexture(x, y, width, height) {
    return new CogSubtexture(
      this, `${this.atlasPath}@[${x},${y},${width},${height}]`,
      this.getRelativeRect(x, y, width, height),
      [-Math.min(x - this.drawOffset[0], 0), -Math.min(y - this.drawOffset[1], 0)],
      width, height
    );
  }

  /**
   * @param {number} cx
   * @param {number} cy
   * @param {number} cw
   * @param {number} ch
   * @returns {[number, number, number, number]}
   */
  getRelativeRect(cx, cy, cw, ch) {
    let parentRect = this.clipRect;
    let parentOffset = this.drawOffset;
    let a = parentRect[0] - parentOffset[0] + cx;
    let b = parentRect[1] - parentOffset[1] + cy;
    let x = Math.max(parentRect[0], Math.min(a, parentRect[0] + parentRect[2]));
    let y = Math.max(parentRect[1], Math.min(b, parentRect[1] + parentRect[3]));
    let w = Math.max(0, Math.min(a + cw, parentRect[0] + parentRect[2]) - x);
    let h = Math.max(0, Math.min(b + ch, parentRect[1] + parentRect[3]) - y);
    return [x, y, w, h];
  }
}

export var CogTextureLoader = new (class CogTextureLoader extends CogAssetLoader {
  /**
   * @param {CogContent} content
   * @param {CogTexture} texture
   * @param {CogDataSrc} src
   * @param {string} path
   * @returns {Promise<CogTexture>}
   */
  async load(content, texture, src, path) {
    let reader = await src.loadBinary(path);

    let ext = path.slice(path.lastIndexOf(".") + 1);
    if (ext === "data") {
      texture.width = reader.readInt32();
      texture.height = reader.readInt32();
      let hasAlpha = reader.readByte() === 1;
      let length = texture.width * texture.height;
      let buffer = texture.buffer = new ArrayBuffer(length * 4);
      let data = new Uint32Array(buffer);
      texture.colorMasks = COLORMASKS_RAW_REORDER;

      for (let i = 0; i < length;) {
        let linesize = reader.readByte();
        
        // Input data as uint: 0xRRGGBBAA
        // Well-formed data:   0xAABBGGRR

        if (hasAlpha) {
          let a = reader.readByte();
          if (a > 0) {
            reader.position--;
            let r = reader.readByte();
            let g = reader.readByte();
            let b = reader.readByte();
            let a = reader.readByte();
            data[i] = (r << 24) | (g << 16) | (b << 8) | a;
          } else {
            data[i] = 0;
          }
        } else {
            let b = reader.readByte();
            let g = reader.readByte();
            let r = reader.readByte();
            data[i] = (r << 24) | (g << 16) | (b << 8) | 0xFF;
        }

        if (linesize > 1) {
          let end = i + linesize;
          for (let to = i + 1; to < end; to++) {
            data[to] = data[i];
          }
        }

        i += linesize;
      }
      
    } else if (ext === "png") {
      let img = await new Promise((resolve, reject) => {
        let blob = new Blob([reader.view.buffer]);
        let img = new Image();
        img.onload = e => resolve(img);
        img.onerror = e => reject(e);
        img.src = URL.createObjectURL(blob);
      });

      let canvas = document.createElement("canvas");
      let ctx = canvas.getContext("2d");
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0, img.width, img.height);
      let data = ctx.getImageData(0, 0, img.width, img.height).data;
      URL.revokeObjectURL(img.src);

      texture.width = img.width;
      texture.height = img.height;
      texture.buffer = data.buffer;
      texture.colorMasks = COLORMASKS_PNG;

    } else {
      throw new Error(`Unknown texture format: ${ext}`);
    }

    return texture;
  }
})();
