// @ts-check
/*! Clusterize.js - v0.15.0 - 2015-12-02
* http://NeXTs.github.com/Clusterize.js/
* Copyright (c) 2015 Denis Lukov; Licensed MIT */

// Modified, based off of 0211bafaeb82d781cea320af1c2f04e36016c3d3 (last MIT-licensed commit)
// Contains changes inspired by https://github.com/NeXTs/Clusterize.js/pull/80

const Clusterize = (() => {

  const defaults = {
    itemHeight: 0,
    blockHeight: 0,
    rowsInBlock: 50,
    rowsInCluster: 0,
    clusterHeight: 0,
    blocksInCluster: 4,
    tag: null,
    contentTag: null,
    showNoDataRow: true,
    noDataClass: "clusterize-no-data",
    noDataText: "",
    keepParity: true,
    verifyChange: false,
    callbacks: {},
    scrollTop: 0
  }

  class Clusterize {
    /** @param {typeof defaults} data */
    constructor(data) {
      // public parameters

      /** @type {typeof defaults} */
      // @ts-ignore
      this.options = Object.assign({}, defaults, data);
  
      /** @type {HTMLElement} */
      this.scrollElem;
      /** @type {HTMLElement} */
      this.contentElem;

      for (let elem of [ "scroll", "content" ]) {
        if(!(this[elem + "Elem"] = data[elem + "Elem"] || document.getElementById(data[elem + "Id"])))
          throw new Error(`Error! Could not find ${elem} element`);
      }
  
      // tabindex forces the browser to keep focus on the scrolling list, fixes upstream#11
      if (!this.contentElem.hasAttribute("tabindex"))
        this.contentElem.setAttribute("tabindex", "0");
  
      this.rows = data.rows || this.fetchMarkup();
      this.cache = { data: "" };
      this.scrollTop = this.scrollElem.scrollTop;
  
      // get row height
      this.exploreEnvironment(this.rows);
  
      // append initial data
      this.insertToDOM(this.rows, this.cache, true);
  
      // restore the scroll position
      this.scrollElem.scrollTop = this.scrollTop;
  
      // adding scroll handler
      this.lastCluster = 0;
      this.scrollRebounce = 0;
      this.pointerEventsSet = false;
      this.resizeDebounce = 0;

      on("scroll", this.scrollElem, this.scrollEv = () => {
        if (this.lastCluster !== (this.lastCluster = this.getClusterNum()))
          this.insertToDOM(this.rows, this.cache, false);
      });

      this.refresh = this.refresh.bind(this);
      on("resize", window, this.resizeEv = () => {
        cancelAnimationFrame(this.resizeDebounce);
        this.resizeDebounce = requestAnimationFrame(this.refresh);
      });

      this.enabled = true;
    }

    // public methods
    disable(clean) {
      if (!this.enabled)
        return;
      this.enabled = false;
      off("scroll", this.scrollElem, this.scrollEv);
      off("resize", window, this.resizeEv);
      this.html(clean ? this.generateEmptyRow() : this.rows);
    }

    enable(keep) {
      if (this.enabled)
        return;
      this.enabled = true;
      on("scroll", this.scrollElem, this.scrollEv);
      on("resize", window, this.resizeEv);
      if (!keep)
        this.rows = this.fetchMarkup();
      this.insertToDOM(this.rows, this.cache, true);
    }

    refresh() {
      if (!this.getRowsHeight(this.rows))
        return;
      this.update(this.rows);
    }

    update(rows) {
      this.rows = rows || [];
      let scrollTop = this.scrollElem.scrollTop;
      // fixes upstream#39
      if (this.rows.length * this.options.itemHeight < scrollTop) {
        this.scrollElem.scrollTop = 0;
        this.lastCluster = 0;
      }
      this.insertToDOM(this.rows, this.cache, false);
      this.scrollElem.scrollTop = scrollTop;
    }

    clear() {
      this.update([]);
    }

    getRowsAmount() {
      return this.rows.length;
    }

    _add(where, newRows) {
      newRows = newRows || [];
      if (!newRows.length)
        return;
      this.rows = where === "append" ? this.rows.concat(newRows) : newRows.concat(this.rows);
      this.insertToDOM(this.rows, this.cache, false);
    }
    append(rows) {
      this._add("append", rows);
    }
    prepend(rows) {
      this._add("prepend", rows);
    }
  
    // fetch existing markup
    fetchMarkup() {
      return this.getChildNodes(this.contentElem);
    }

    // get tag name, content tag name, tag height, calc cluster height
    exploreEnvironment(rows) {
      let opts = this.options;
      opts.contentTag = this.contentElem.tagName.toLowerCase();
      if (!rows || !rows.length)
        return;
      if (this.contentElem.children.length <= 1)
        this.html([rows[0], rows[1], rows[2]]);
      if (!opts.tag)
        opts.tag = this.contentElem.children[0].tagName.toLowerCase();
      this.getRowsHeight(rows);
    }

    getRowsHeight(rows) {
      let opts = this.options;
      let prevItemHeight = opts.itemHeight;
      opts.clusterHeight = 0
      if (!rows || !rows.length)
        return;
      let nodes = this.contentElem.children;
      // @ts-ignore
      opts.itemHeight = nodes[Math.floor(nodes.length / 2)].offsetHeight;
      // consider table's border-spacing
      if (opts.tag == "tr" && getStyle("borderCollapse", this.contentElem) !== "collapse")
        opts.itemHeight += parseInt(getStyle("borderSpacing", this.contentElem)) || 0;
      opts.blockHeight = opts.itemHeight * opts.rowsInBlock;
      opts.rowsInCluster = opts.blocksInCluster * opts.rowsInBlock;
      opts.clusterHeight = opts.blocksInCluster * opts.blockHeight;
      return prevItemHeight !== opts.itemHeight;
    }

    // get current cluster number
    getClusterNum() {
      this.options.scrollTop = this.scrollElem.scrollTop;
      return Math.floor(this.options.scrollTop / (this.options.clusterHeight - this.options.blockHeight)) || 0;
    }

    // generate empty row if no data provided
    generateEmptyRow() {
      let opts = this.options;
      if (!opts.tag || !opts.showNoDataRow)
        return [];
      let emptyRow = document.createElement(opts.tag);
      let noDataContent = document.createTextNode(opts.noDataText)
      let td;
      emptyRow.className = opts.noDataClass;
      if (opts.tag === "tr") {
        td = document.createElement("td");
        td.appendChild(noDataContent);
      }
      emptyRow.appendChild(td || noDataContent);
      return [emptyRow];
    }

    // generate cluster for current scroll position
    generate(rows, clusterNum) {
      let opts = this.options;
      let rowsLen = rows.length;
      if (rowsLen < opts.rowsInBlock) {
        return {
          rowsAbove: 0,
          rows: rowsLen ? rows : this.generateEmptyRow()
        }
      }
      if (!opts.clusterHeight) {
        this.exploreEnvironment(rows);
      }
      let itemsStart = Math.max((opts.rowsInCluster - opts.rowsInBlock) * clusterNum, 0);
      let itemsEnd = itemsStart + opts.rowsInCluster;
      let topSpace = itemsStart * opts.itemHeight;
      let bottomSpace = (rowsLen - itemsEnd) * opts.itemHeight;
      let clusterRows = [];
      let rowsAbove = itemsStart;
      if (topSpace > 0) {
        opts.keepParity && clusterRows.push(this.renderExtraTag("keep-parity"));
        clusterRows.push(this.renderExtraTag("top-space", topSpace));
      } else {
        rowsAbove++;
      }
      for (let i = itemsStart; i < itemsEnd; i++) {
        rows[i] && clusterRows.push(rows[i]);
      }
      bottomSpace > 0 && clusterRows.push(this.renderExtraTag("bottom-space", bottomSpace));
      return {
        rowsAbove: rowsAbove,
        rows: clusterRows
      }
    }

    renderExtraTag(className, height) {
      let tag = document.createElement(this.options.tag);
      tag.className = `clusterize-extra-row clusterize-${className}`;
      if (height)
        tag.style.height = height + "px";
      return tag;
    }

    // if necessary verify data changed and insert to DOM
    insertToDOM(rows, cache, fast) {
      let data = this.generate(rows, fast ? this.lastCluster : this.getClusterNum());
      let outerData = data.rows;
      let callbacks = this.options.callbacks;
      if (!this.options.verifyChange || this.options.verifyChange && this.dataChanged(outerData, cache)) {
        callbacks.clusterWillChange && callbacks.clusterWillChange();
        this.html(outerData);
        this.options.contentTag === "ol" && this.contentElem.setAttribute("start", ""+data.rowsAbove);
        callbacks.clusterChanged && callbacks.clusterChanged();
      }
    }

    empty(element) {
      while (element.lastChild) {
        element.removeChild(element.lastChild);
      }
    }

    html(data) {
      let contentElem = this.contentElem;
      this.empty(contentElem);
      for (let i = 0; i < data.length; i++) {
        contentElem.appendChild(data[i]);
      }
    }

    getChildNodes(tag) {
      let childNodes = tag.children;
      let childNodesHelper = [];
      for (let i = 0; i < childNodes.length; i++) {
        childNodesHelper.push(childNodes[i]);
      }
      return Array.prototype.slice.call(childNodesHelper);
    }

    dataChanged(data, cache) {
      let currentData = JSON.stringify(data);
      let changed = currentData !== cache.data;
      return changed && (cache.data = currentData);
    }
  }

  // support functions
  function on(evt, element, fnc) {
    return element.addEventListener(evt, fnc, false);
  }

  function off(evt, element, fnc) {
    return element.removeEventListener(evt, fnc, false);
  }

  function getStyle(prop, elem) {
    return window.getComputedStyle ? window.getComputedStyle(elem)[prop] : elem.currentStyle[prop];
  }

  return Clusterize;
})();