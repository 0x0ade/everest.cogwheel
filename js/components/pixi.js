//@ts-check
import Stats from "../deps/Stats.js";
import CONSTS from "../consts.js";
import { CogAsset } from "../content/core.js";
import { CogAtlas } from "../content/atlas.js";
import { CogAutotiler } from "../content/autotiler.js";
import { CogTexture, CogSubtexture } from "../content/texture.js";
/** @type {typeof import("pixi-viewport").Viewport} */
// @ts-ignore
const Viewport = window["Viewport"].Viewport;

/**
 * @typedef {{ texture: PIXI.Texture }} PIXIResourceMini
 */

export class CogwheelPIXI {
  /**
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
    this.cogwheel = cogwheel;

    /** @type {Map<string, PIXIResourceMini>} */
    this._resources = new Map();

    this.debugLoad = false;
    this.firstError = false;
    this.errorHalt = false;
  }

  async start() {
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
    // Note: Keep antialiasing disabled to fix gaps between textures.
    this.app = new PIXI.Application({ width: 0, height: 0, antialias: false, preserveDrawingBuffer: false, transparent: false });

    this.app.ticker.update = ((ctx, orig) => function (currentTime) {
      if (ctx.errorHalt)
        return;
      try {
        orig(currentTime);
      } catch (e) {
        console.error(e);
        ctx.errorHalt = true;
        setTimeout(() => ctx.errorHalt = false, 1000);
        if (!ctx.firstError) {
          ctx.firstError = true;
          ctx.cogwheel.alert({ title: "Hello there, old chum.", text:
`*I'm gnot a gnelf. I'm gnot a goblin.*

**I'm an alert box and something went wrong.**

Cogwheel has just encountered a fatal error.  
Further operation might or might not be impacted.

Please create a backup of your map file.

You can help out by making 0x0ade aware of this:

<pre style="overflow-x: scroll">
\`\`\`
Cogwheel v${ctx.cogwheel.package.version}
${e.stack}
\`\`\`
</pre>

`
          });
        }
        ctx.cogwheel.snackbar({ text: "Cogwheel encountered a fatal error. Please back up your map before continuing on." });
      }
    })(this, this.app.ticker.update.bind(this.app.ticker));

    /** @type {PIXI.Renderer & { gl: any }} */
    // @ts-ignore
    this.renderer = this.app.renderer;
    this.renderer.backgroundColor = CONSTS.COLOR_BG;
    this.renderer.preserveDrawingBuffer = true;
    PIXI.settings.ROUND_PIXELS = true;
    // this.renderer.textureGC.mode = PIXI.GC_MODES.MANUAL;

    /** @type {PIXI.interaction.InteractionManager} */
    this.interaction = this.renderer.plugins.interaction;

    this.stats = Stats();
    this.stats.showPanel(0);

    this.stage = new PIXI.display.Stage();
    this.stage.group.enableSort = true;
    this.app.stage = this.stage;

    this.viewport = new Viewport({
      screenWidth: window.innerWidth,
      screenHeight: window.innerHeight,
      worldWidth: 1000,
      worldHeight: 1000,

      interaction: this.interaction
    });

    // Fix the viewport accepting wheel events which target other elements.
    {
      this.viewport["input"]["handleWheel"] = ((ctx, orig) => function handleWeelProxy(e) {
        if (document.elementFromPoint(e.clientX, e.clientY) !== ctx.app.view)
          return;
        orig.call(this, e);
      })(this, this.viewport["input"]["handleWheel"]);
    }

    this.app.stage.addChild(this.viewport);
    this.viewport
      .drag({ mouseButtons: "middle|right" })
      .pinch()
      .wheel({ smooth: 6 });
    
    // Custom wheel zoom logic. Lock onto integer zoom levels.
    {
      /** @type {any} */
      let wheel = this.viewport.plugins.get("wheel");

      wheel.update = ((ctx, orig) => function update() {
        if (!this.smoothingTarget)
          return;

        /** @type {PIXI.Point} */
        const smoothingCenterRaw = this.smoothingCenter;
        let smoothingCenter = this.parent.toLocal(smoothingCenterRaw);

        this.parent.scale.set(ctx.cogwheel.utils.lerp(this.parent.scale.x, this.smoothingTarget, (this.smoothingCount + 1) / this.options.smooth));
        this.parent.emit("zoomed", {
            viewport: this.parent,
            type: "wheel"
        });

        smoothingCenter = this.parent.toGlobal(smoothingCenter);
        this.parent.x += smoothingCenterRaw.x - smoothingCenter.x;
        this.parent.y += smoothingCenterRaw.y - smoothingCenter.y;

        this.smoothingCount++;
        if (this.smoothingCount >= this.options.smooth) {
          this.smoothingTarget = null;
          this.smoothingTargetPrev = null;
        }
      })(this, wheel.update);

      wheel.wheel = ((ctx, orig) => function wheel(e) {
        if (this.paused)
          return;
        
        /** @type {PIXI.Point} */
        let pos = this.parent.input.getPointerPosition(e);

        let dir = (this.options.reverse ? 1 : -1) * Math.sign(e.deltaY);
        let target = this.smoothingTarget || this.parent.scale.x;

        if (!this.smoothingTarget) {
          this.smoothingTargetPrev = target;
        }

        if (target < 1 || (target === 1 && dir < 0)) {
          target = 1 / target;
          target = Math.round(target - dir);
          target = 1 / target;
          if (isNaN(target) || !isFinite(target))
            target = 1;
        } else {
          target = Math.round(target + dir);
          if (target <= 0)
            target = 1;
        }

        if (this.smoothingTarget) {
          if (target > 2 && this.smoothingTargetPrev < 2)
            target = 2;
          else if (target < 1 && this.smoothingTargetPrev > 1)
            target = 1;
        }
        
        this.smoothingCount = 0;
        this.smoothingCenter = pos;
        this.smoothingTarget = target;

        this.parent.emit("moved", {
            viewport: this.parent,
            type: "wheel"
        });
        
        this.parent.emit("wheel", {
            viewport: this.parent,
            wheel: {
                dx: e.deltaX,
                dy: e.deltaY,
                dz: e.deltaZ
            },
            event: e
        });
        
        if (!this.parent.options.passiveWheel)
          e.preventDefault();
      })(this, wheel.wheel);
    }

    // Listen for window resize events.
    window.addEventListener("resize", () => this.resize());

    // Listen for frame updates.
    this.app.ticker.add((deltaTime) => this.update(deltaTime));
  }

  resize() {
    const parent = this.app.view.parentElement;
    if (!parent)
      return;
    const { clientWidth: width, clientHeight: height } = parent;
    this.renderer.resize(width, height);
    this.viewport.screenWidth = width;
    // @ts-ignore Broken .d.ts
    this.viewport.screenHeight = height;
  }

  /**
   * @param {number} deltaTime
   */
  update(deltaTime) {
    this.stats.end();
    this.stats.begin();
  }

  /**
   * @param {CogTexture} asset
   */
  _loadTexture(asset) {
    const { path: name } = asset;
    let res = this._resources.get(name);
    if (res)
      return res.texture;

    const { renderer } = this;
    const { buffer, width, height } = asset;

    let loadTextureCanvas = document.createElement("canvas");
    let loadTextureCanvasCtx = loadTextureCanvas.getContext("2d");
    loadTextureCanvas.width = width;
    loadTextureCanvas.height = height;
    loadTextureCanvasCtx.putImageData(new ImageData(new Uint8ClampedArray(buffer), width, height), 0, 0);

    let texBase = new PIXI.BaseTexture(loadTextureCanvas);
    // renderer.textureManager.updateTexture(texBase);
    texBase.scaleMode = PIXI.SCALE_MODES.NEAREST;

    // Get rid of the buffer afterwards, assuming we won't ever need it again.
    if (!asset.bufferPreserve)
      asset.buffer = null;

    let tex = new PIXI.Texture(texBase);
    this._resources.set(name, {
      texture: tex
    });
    return tex;
  }

  /**
   * @param {CogAsset} asset
   */
  _add(asset) {
    if (!this._addAction) {
      this._addPromise = new Promise((resolve, reject) => {
        this._addResolve = resolve;
        this._addReject = reject;
      });
      this._addAction = () => {
        clearTimeout(this._addTimeout);
        this._addTimeout = null;
        this._addAction = null;
        let resolve = this._addResolve;
        let reject = this._addReject;
  
        let iter = this._addQueue.values();
        this._addQueue = null;
        this._loadQueued = null;
        let retry = () => {
          if (PIXI.Loader.shared.loading)
            return requestAnimationFrame(retry);

  
          let timeStart = performance.now();
          let i;
          while (!(i = iter.next()).done) {
            let asset = i.value;
            let name = asset.path;
            if (this._resources.has(name) || PIXI.Loader.shared.resources[name])
              continue;

            if (this.debugLoad)
              console.debug("[pixiFeed]", "Adding:", asset);

            if (asset instanceof CogTexture) {
              this._loadTexture(asset);

            } else if (asset instanceof CogSubtexture) {
              let texMain = this._loadTexture(asset.texture);

              let drawOffset = asset.drawOffset;
              let drawOffset0 = drawOffset[0];
              let drawOffset1 = drawOffset[1];

              let clipRect = asset.clipRect;
              let clipRect0 = clipRect[0];
              let clipRect1 = clipRect[1];
              let clipRect2 = clipRect[2];
              let clipRect3 = clipRect[3];

              let texSub = new PIXI.Texture(
                texMain.baseTexture,
                new PIXI.Rectangle(clipRect0, clipRect1, clipRect2, clipRect3),
                new PIXI.Rectangle(-drawOffset0, -drawOffset1, asset.width, asset.height),
                new PIXI.Rectangle(drawOffset0, drawOffset1, clipRect2, clipRect3),
                0,
                null
              );

              this._resources.set(name, {
                texture: texSub
              });

            } else {
              PIXI.Loader.shared.add(name, asset.dataURL);
            }
  
            if ((performance.now() - timeStart) >= 30)
              return requestAnimationFrame(retry);
          }
          
          PIXI.Loader.shared
            .load((loader, resources) => resolve())
            .on("error", (...args) => reject(args));
        }
        retry();
      }
    }
    if (this._addTimeout) {
      clearTimeout(this._addTimeout);
    }

    if (!this._addQueue)
      /** @type {Set<CogAsset>} */
      this._addQueue = new Set();
    this._addQueue.add(asset);

    this._addTimeout = setTimeout(this._addAction, 200);
    return this._addPromise;
  }

  /**
   * @param {CogAsset} asset
   * @returns {Promise<PIXIResourceMini>}
   */
  load(asset) {
    /** @type {string} */
    let name = asset.path;

    let resource = this.get(asset);
    if (resource)
      return new Promise(resolve => resolve(resource));

    // Queued load.

    if (!this._loadQueued)
      /** @type {Map<string, Promise<PIXIResourceMini>>} */
      this._loadQueued = new Map();

    let queued = this._loadQueued.get(name);
    if (!queued) {
      queued = new Promise((resolve, reject) => {
        this._add(asset).then(() => resolve(this.get(name)), reject);
      });
      this._loadQueued.set(name, queued);
    }
    return queued;
  }

  /**
   * @param {string | CogAsset} id
   * @returns {PIXIResourceMini}
   */
  get(id) {
    /** @type {string} */
    let name;
    if (id instanceof CogAsset) {
      name = id.path;
    } else {
      name = id;
    }

    let resource = this._resources.get(name);
    if (resource)
      return resource;

    resource = PIXI.Loader.shared.resources[name];
    if (resource)
      this._resources.set(name, resource);
    return resource;
  }

  /**
   * @param {any} container
   * @returns {Promise<PIXIResourceMini[]>}
   */
  async feed(container) {
    /** @type {Set<CogAsset>} */
    let assets = new Set();

    /**
     * @param {any} container
     */
    let feed = (container) => {
      if (container[Symbol.iterator]) {
        for (let item of container)
          feed(item);

      } else if (container instanceof CogAtlas) {
        for (let value of container.textures.values())
          assets.add(value);

      } else if (container instanceof CogAutotiler) {
        for (let value of container.lookup.values())
           feed(value);

      } else if (container instanceof CogAutotiler.TerrainType) {
        container.center.textures.map(tex => assets.add(tex));
        container.padded.textures.map(tex => assets.add(tex));
        for (let id in container.masked)
          container.masked[id].tiles.textures.map(tex => assets.add(tex));

      } else {
        throw new Error(`pixiFeed doesn't support ${container.constructor.name}`);
      }
    }

    feed(container);

    console.log("[pixiFeed]", `Consuming ${assets.size} assets`);

    let p = Promise.all(Array.from(assets).map(asset => this.load(asset)));
    // Skip the timeout.
    if (this._addAction)
      this._addAction();
    return p;
  }

}
