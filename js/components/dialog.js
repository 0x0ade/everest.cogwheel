//@ts-check
import { rd, rdom, rd$, escape$, RDOMListHelper } from "../utils/rdom.js";
import mdcrd from "../utils/mdcrd.js";

/**
 * @typedef {import("material-components-web")} mdc
 */
/** @type {import("material-components-web")} */
const mdc = window["mdc"]; // mdc

export class CogwheelDialog {
  /**
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
    this.cogwheel = cogwheel;
  }

  async start() {
  }


  about() {
    let text = `**Cogwheel v${this.cogwheel.package.version} - ${this.cogwheel.package.versionCodename}**  \n`;
    let path = this.cogwheel.electron.remote.app.getAppPath();
    text += `[Open installation folder](${this.cogwheel.interop.encodeFSPathToURL(path)})  \n`;

    text += `Dependencies:\n`;

    const getDeps = (deps) => Reflect.ownKeys(deps).map(name => { return { name: name.toString(), version: deps[name] } })
    for (let dep of
      [
        { name: "electron", version: this.cogwheel.electron.remote.process.versions.electron },
        ...getDeps(this.cogwheel.package.dependencies),
        // ...getDeps(this.cogwheel.package.devDependencies), // Inaccessible in release builds.
        ...getDeps(this.cogwheel.package.optionalDependencies),
      ].sort((a, b) => a.name.localeCompare(b.name, undefined, { numeric: true, sensitivity: "base" }))
    )
      text += `- ${dep.name} ${dep.version}\n`;
    text += "\n";

    text += "Source code: [https://github.com/EverestAPI/cogwheel](https://github.com/EverestAPI/cogwheel)  \n"
    text += "<sup>Baked with 🍓 by the Everest Team x Celestial Cartographers</sup>\n"

    this.cogwheel.alert({
      title: "About",
      text: text
    });
  }


  async selectCeleste() {
    await this.cogwheel.alert({
      text:
`Cogwheel couldn't find your Celeste installation,  
or it failed loading the game's files.

Please select your Celeste folder in the next window.`,
      dismissable: false,
    });

    let dirs = await new Promise(resolve => {
      this._celesteSelectCB = resolve;
      this.cogwheel.electron.ipcRenderer.send("celesteSelect");
    });
    let dir = dirs ? dirs[0] : null;
    if (!dir || !this.cogwheel.fs.existsSync(dir)) {
      await this.cogwheel.alert({
        text:
`No Celeste installation selected.  
Cogwheel will now quit.`,
        dismissable: false,
      });

      this.cogwheel.electron.remote.getCurrentWindow().close();
      throw new Error("QUIT THE DAMN APP!");
    }
    this.cogwheel.nconf.set("celeste:dir", dir);
    this.cogwheel.nconf.save("config.json");
  }


  async openMap() {
    let paths = await this.openFile({
      title: "Select a map",
      defaultPath: this.cogwheel.path.join(this.cogwheel.nconf.get("celeste:dir"), "Mods"),
      filters: [{
        name: "Celeste Map Binary",
        extensions: ["bin"]
      }]
    });
    let path = paths && paths[0];
    if (!path)
      return;
    
    this.cogwheel.session.map = path;
    await this.cogwheel.session.init();
    this.cogwheel.render();
  }


  async saveMap() {
    let path = await this.saveFile({
      title: "Save your map",
      // defaultPath: this.path.join(this.nconf.get("celeste:dir"), "Mods"),
      filters: [{
        name: "Celeste Map Binary",
        extensions: ["bin"]
      }]
    });
    if (!path)
      return;
    
    this.cogwheel.session.map = path;
    await this.cogwheel.saveMap();
  }


  /**
   * @param {{ title?: string; defaultPath?: string; folder?: boolean; multi?: boolean; filters?: { name: string; extensions: string[]; }[] }} options
   * @returns {Promise<string[]>}
   */
  openFile(options) {
    return new Promise(resolve => {
      this._showFileOpenDialogCB = resolve;
      this.cogwheel.electron.ipcRenderer.send("showFileOpenDialog", Object.assign({ title: "", defaultPath: "", folder: false, multi: false, filters: null }, options));
    });
  }


  /**
   * @param {{ title?: string; defaultPath?: string; folder?: boolean; filters?: { name: string; extensions: string[]; }[] }} options
   * @returns {Promise<string>}
   */
  saveFile(options) {
    return new Promise(resolve => {
      this._showFileSaveDialogCB = resolve;
      this.cogwheel.electron.ipcRenderer.send("showFileSaveDialog", Object.assign({ title: "", defaultPath: "", folder: false, filters: null }, options));
    });
  }


  settings() {
    const title = (label) => el => rd$(el)`<b class="title">${label}</b>`;
    const row = (label, body) => el => rd$(el)`<span class="row"><span class="body">${body}</span><span class="label">${label}</span></span>`;
    const group = (...items) => el => {
      el = rd$(el)`<ul class="settings-group"></ul>`;

      let list = new RDOMListHelper(el);
      for (let i in items) {
        list.add(i, items[i]);
      }
      list.end();

      return el;
    }

    let el = this.elSettings = mdcrd.dialog({
      title: "Settings",
      body: el => rd$(el)`
        <div>
          ${group(
            title("Main"),
            row("Celeste Directory", mdcrd.textField("", "", (value) => this.cogwheel.alert({ text: value }))),
            row("Label", "<input here>"),
            row("Label", "<input here>")
          )}

          ${group(
            title("Other"),
            row("Label", "<input here>"),
            row("Label", "<input here>"),
            row("Label", "<input here>")
          )}
        </div>`,
      defaultButton: "yes",
      buttons: ["OK"],
    })(this.elSettings);

    document.body.appendChild(el);

    /** @type {import("@material/dialog").MDCDialog} */
    let dialog = el["MDCDialog"];

    // @ts-ignore Outdated .d.ts
    dialog.open();

    let promise = new Promise(resolve => el.addEventListener("MDCDialog:closed", e => resolve(e["detail"].action)));
    dialog["then"] = promise.then.bind(promise);
    dialog["catch"] = promise.catch.bind(promise);

    return dialog;
  }
  

}
