//@ts-check
import { rd, rdom, rd$, escape$, RDOMListHelper } from "../utils/rdom.js";
import mdcrd from "../utils/mdcrd.js";
import { CogMap } from "../content/map.js";
import { CogwheelMapTool } from "./extras.js";
import { CogAtlas } from "../content/atlas.js";

/**
 * @typedef {import("material-components-web")} mdc
 */
/** @type {import("material-components-web")} */
const mdc = window["mdc"]; // mdc

export class CogwheelDOM {
  /**
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
    this.cogwheel = cogwheel;
    this.container = document.getElementsByTagName("app-container")[0];
  }

  async start() {
    document.addEventListener("keydown", /** @param {KeyboardEvent} e */ e => {
      if (e.ctrlKey && (e.key === "z" || e.key === "y") && e.target instanceof HTMLElement && (
        e.target.tagName !== "INPUT"
      ))
        e.preventDefault();
    }, false);
  }

  /**
   * @param {HTMLCanvasElement} el
   * @param {{ name: string; title: string; fun: () => void; }[]} menu
   */
  setContextMenu(el, menu) {
    // Prevent a right click drag from registering as a context menu trigger.
    /** @type {number} */
    let eRightDownX;
    /** @type {number} */
    let eRightDownY;
    el.addEventListener(
      "mousedown",
      /** @param {MouseEvent} e */
      e => {
        if (e.button !== 2)
          return;
        eRightDownX = e.x;
        eRightDownY = e.y;
      },
      false
    );
    el.addEventListener(
      "contextmenu",
      /** @param {MouseEvent} e */
      e => {
        if (
          e.button !== 2 ||
          Math.abs(eRightDownX - e.x) <= 3 ||
          Math.abs(eRightDownY - e.y) <= 3
        )
          return;
        e.preventDefault();
        e.stopImmediatePropagation();
      },
      false
    );

    let jqel = $(el);
    // @ts-ignore
    jqel.contextMenu(menu, { triggerOn: "contextmenu" });
    this.cogwheel.pixi.app.view.addEventListener("mousedown", () => {
      // @ts-ignore
      jqel.contextMenu("close")
    }, false);
  }

  /**
   * @param {HTMLElement} el
   * @param {string} text
   * @param {"up" | "down"} dir
   */
  setTooltip(el, text, dir = "up") {
    let ctx = {
      /** @type {HTMLElement} */
      el: null,
      text: text,
      dir: dir,
      /** @type {() => void} */
      show: null,
      /** @type {() => void} */
      hide: null
    };

    if ("cogTooltip" in el) {
      // @ts-ignore
      el["cogTooltip"].text = text;
      // @ts-ignore
      el["cogTooltip"].dir = dir;
      // @ts-ignore
      return el["cogTooltip"].tooltipEl;
    }

    let visible = false;

    /**
     * @param {boolean} _visible
     */
    const renderTooltip = (_visible) => {
      visible = _visible;
      let tooltipEl = ctx.tooltipEl = rd$(ctx.tooltipEl)`<div class="tooltip" data-tooltip-dir=${ctx.dir} ${rd.toggleClass("visible")}=${visible}>${ctx.text}</div>`;
      refreshTooltip();
      return tooltipEl;
    };

    const refreshTooltip = () => {
      let tooltipEl = ctx.tooltipEl;
      let rect = el.getBoundingClientRect();

      tooltipEl.style.left = undefined;
      tooltipEl.style.bottom = undefined;

      switch (dir) {
        case "up":
        default:
          tooltipEl.style.left = (rect.left + rect.width / 2) + "px";
          tooltipEl.style.bottom = ((window.innerHeight - rect.top) + 2) + "px";
          break;

        case "down":
          tooltipEl.style.left = (rect.left + rect.width / 2) + "px";
          tooltipEl.style.top = (rect.bottom + 2) + "px";
          break;
      }

      if (visible)
        requestAnimationFrame(refreshTooltip);
    };

    el.addEventListener("mouseover", ctx.show = () => {
      renderTooltip(true);
    }, false);
    el.addEventListener("mouseout", ctx.hide = () => {
      renderTooltip(false);
    }, false);

    document.body.appendChild(renderTooltip(false));
    el["cogTooltip"] = ctx;
    return ctx.tooltipEl;
  }

  render() {
    let timeStart = performance.now();

    /**
     * @param {boolean} right
     * @param {() => HTMLElement} getTarget
     */
    const resizeButton = (right, getTarget) => el => rd$(el)`
    <div class="resize-button" ${rd.toggleClass("right")}=${right}></div>
    ${/** @param {HTMLElement} el */ el => {
      let retracted = false;
      el.addEventListener("click", () => {
        let target = getTarget();
        if (retracted) {
          el.classList.remove("retracted");
          target.classList.remove("retracted");
        } else {
          el.classList.add("retracted");
          target.classList.add("retracted");
        }
        retracted = !retracted;
      });
    }}`;

    this.el = rd$(this.el)`
    <app>
      ${mdcrd.topAppBar([
        el => rd$(el)`<span class="mdc-top-app-bar__title"><img id="logo" alt="logo" src="./logo-small.png"></span>`,

        mdcrd.menu.auto("File",
          // @ts-ignore
          ["create", "New", () => this.cogwheel.new()], // TODO
          ["folder_open", "Open", () => this.cogwheel.dialog.openMap()],

          ["save", "Save", () => this.cogwheel.saveMap()],

          ["folder", "Save As...", () => this.cogwheel.dialog.saveMap()],

          mdcrd.list.divider,
          // @ts-ignore
          ["settings", "Settings", () => this.cogwheel.dialog.settings()],
        ),

        mdcrd.menu.auto("Edit",
          ["", "Undo", () => this.cogwheel.history.undo()],

          ["", "Redo", () => this.cogwheel.history.redo()],
        ),

        mdcrd.menu.auto("Map",
          // @ts-ignore
          ["", "Stylegrounds", () => this.cogwheel.stylegrounds()], // TODO

          // @ts-ignore
          ["", "Metadata", () => this.cogwheel.metadata()], // TODO

          // @ts-ignore
          ["", "Save Map Image", () => this.cogwheel.saveMapImage()], // TODO
        ),

        mdcrd.menu.auto("Room",
          // @ts-ignore
          ["", "Add", () => this.cogwheel.roomAdd()], // TODO

          // @ts-ignore
          ["", "Configure", () => this.cogwheel.roomConfigure()], // TODO
        ),

        mdcrd.menu.auto("Help",
          this.renderUpdateButton = (el) => {
            let status = this.cogwheel.updaterStatus;
            el = el || this.elUpdateButton;
            el = this.elUpdateButton = mdcrd.menu.item(
              "",
              status === "downloaded" ? "Update (Restart)" : "Update",
              () => this.cogwheel.updaterStep(true),
              status === "idle" || status === "available" || status === "downloaded"
            )(el);

            return el;
          },
          ["", "About", () => this.cogwheel.dialog.about()],
        ),

        mdcrd.menu.auto("Debug",
          ["", "Electron Menubar", () => {
            let window = this.cogwheel.electron.remote.getCurrentWindow();
            window.setMenuBarVisibility(!window.isMenuBarVisible());
          }],
          ["", "Open DevTools", () => this.cogwheel.electron.remote.getCurrentWindow().webContents.openDevTools()],
          ["", "Reload Cogwheel", () => this.cogwheel.electron.remote.getCurrentWindow().webContents.reload()],
          ["", "Rescan Mods", () => {
            this.cogwheel.loadMods();
          }]
        ),

      ],
      [

      ])}

      <main>
        <div id="pixi-frame">
          ${this.cogwheel.pixi.app.view}
          <div id="stats">${null /*this.cogwheel.pixi.stats.dom*/}</div>
        </div>

        <div class="pane" id="pane-left">
          <div class="pane-inner">
            ${this.renderLevelList = (el, scroll) => {
              el = el || this.elLevelList;
              /** @type {HTMLElement} */
              let elFocused = null;

              let list = [];

              let map = this.cogwheel.get(CogMap, "Map");
              if (map) {
                for (let level of map) {
                  list.push(
                  /**
                   * @param {HTMLElement} el
                   */
                  el => {
                    el = mdcrd.list.item(level.name, () => this.cogwheel.map.focusLevel(level, true, false))(el);
                    if (level.name === this.cogwheel.session.level) {
                      el.setAttribute("data-focused", "");
                      elFocused = el;
                    } else {
                      el.removeAttribute("data-focused");
                    }
                    el.setAttribute("data-editorcolorindex", level.editorColorIndex.toString());
                    return el;
                  });
                }
              }

              el = this.elLevelList = mdcrd.list.list(...list)(el);

              if (scroll && elFocused) {
                setTimeout(() => el.parentElement.scroll(0, elFocused.offsetTop + elFocused.clientHeight / 2 - el.parentElement.clientHeight / 2), 0);
              }

              return el;
            }}
          </div>
          ${resizeButton(false, () => this.elLevelList.parentElement.parentElement)}
        </div>
        
        <div id="pane-center">
          ${this.renderPickerMini = el => {
            if (this.cogwheel.session.pickerIsList)
              return this.elPickerMini = null;

            this.elPickerMini = el = rd$(el || this.elPickerMini)`
              <div id="picker"><div id="picker-inner"></div></div>`;

            let currentTool = this.cogwheel.map.currentTool;
            let entries = (currentTool && currentTool.entries) || [];
            let layers = (currentTool && currentTool.layers) || [];

            let list = new RDOMListHelper(el.querySelector("#picker-inner"));
              
            /**
             * @param {HTMLElement} el
             */
            const scrollsnap = el => rd$(el)`<div class="scrollsnap"></div>`;
            /**
             * @param {string | number} id
             * @param {string} tooltip
             * @param {(el: HTMLElement) => HTMLElement} gen
             */
            const add = (id, tooltip, gen) => {
                list.add(id + "-snap", scrollsnap);
                list.add(id, /** @param {HTMLElement} el */ el => {
                  el = gen(el);
                  this.setTooltip(el, tooltip);
                  return el;
              });
            };

            /**
             * @param {string} id
             * @param {MouseEvent} e
             */
            const openMenu = (id, e) => {
              /** @type {mdc["menu"]["MDCMenu"]["prototype"]} */
              let menu = this["menu" + id];
              /** @type {HTMLElement} */
              let menuEl = this["menu" + id + "El"];
              if (!menu || !menuEl || !(e.target instanceof HTMLElement))
                return;

              if (!menuEl.parentElement)
                document.body.append(menuEl);

              let rect = e.target.getBoundingClientRect();
              // @ts-ignore Outdated .d.ts
              menu.setAbsolutePosition(rect.x, rect.y + menuEl.clientHeight);
              if (!menu.open)
                mdcrd.menu.open(menuEl);
            };

            /**
             * @param {HTMLElement} el
             * @param {string} id
             * @param {any[]} list
             */
            const genMenu = (el, id, list) => {
              let elReal = el ? el["MDCMenuEl"] : null;
              elReal = mdcrd.menu.list(...list)(elReal);
              let menu = this["menu" + id] = elReal["MDCMenu"];
              this["menu" + id + "El"] = elReal;

              if (!el) {
                el = rd$(null)`<div></div>`;
                el["MDCMenuEl"] = elReal;
                window.addEventListener("resize", () => menu.open = false);
              }

              return el;
            };

            add("switch", "Tools", el => rd$(el)`
              <div class="entry" onclick=${e => openMenu("ToolList", e)}>
                <i class="icon material-icons">${(currentTool && currentTool.icon) || "brush"}</i>
                ${el => {
                  let list = [];

                  let tools = Array.from(this.cogwheel.map.tools.values());
                  tools.sort(
                    (a, b) => {
                      let aHasIndex = "index" in a;
                      let bHasIndex = "index" in b;
                      if (!aHasIndex) {
                        if (!bHasIndex) {
                          return a.id.localeCompare(b.id, undefined);
                        }
                        return -1;
                      } else if (!bHasIndex) {
                        return 1;
                      }
                      
                      return a.index - b.index;
                    }
                  );
    
                  for (let tool of tools) {
                    if (!tool.name)
                      continue;
                    list.push(
                    /**
                     * @param {HTMLElement} el
                     */
                    el => {
                      el = mdcrd.menu.item(tool.icon, tool.name, () => {
                        this.cogwheel.map.currentTool = tool;
                      })(el);
                      if (tool.id === this.cogwheel.session.tool) {
                        el.setAttribute("data-focused", "");
                      } else {
                        el.removeAttribute("data-focused");
                      }
                      return el;
                    });
                  }

                  return genMenu(el, "ToolList", list);
                }}
              </div>`);

            if (layers.length > 0) {
              add("layers", "Layers", el => rd$(el)`
                <div class="entry" onclick=${e => openMenu("LayerList", e)}>
                  <i class="icon material-icons">layers</i>
                  <div class="badge">${currentTool.layers.find(l => l.id === this.cogwheel.session.layer).short}</div>
                  ${el => {
                    let list = [];
      
                    for (let layer of layers) {
                      list.push(mdcrd.menu.item(null, layer.name, () => {
                        this.cogwheel.session.layer = layer.id;
                        this.renderPicker();
                      }));
                    }

                    return genMenu(el, "LayerList", list);
                  }}
                </div>`);
            }

            if (entries.length > 0) {
              // add("expand", el => rd$(el)`<div class="entry"><i class="icon material-icons">apps</i></div>`);
              
              add("search", "Search", el => rd$(el)`<div class="entry"><i class="icon material-icons">search</i></div>`);
            }

            const nullIconURL = this.cogwheel.get(CogAtlas, "Content").get("null").dataURL;

            for (let i in entries) {
              let item = entries[i];
              add("entry-" + i, item.name, el => item.renderEntry ? item.renderEntry(el) : rd$(el)`
                <div class="entry" ${rd.toggleClass("active")}=${item.active ? item.active() : this.cogwheel.session.place === item.id} onclick=${() => {
                  let skipRender = false;
                  if (item.select) {
                    skipRender = item.select();
                  } else {
                    this.cogwheel.session.place = item.id;
                  }

                  if (skipRender !== true)
                    this.renderPicker();
                }}>
                  <img class="icon" src=${!item.icon ? nullIconURL : (item.icon.dataURL || item.icon)}>
                </div>`);
            }

            list.end();

            return el;
          }}
        </div>

        <div class="pane" id="pane-right">
          <div class="pane-inner clusterize-scroll" id="pane-right-inner">
            ${this.renderPickerList = el => {
              if (!this.cogwheel.session.pickerIsList)
                return this.elPickerList = null;

              el = el || this.elPickerList;

              let currentTool = this.cogwheel.map.currentTool;
              let entries = (currentTool && currentTool.entries) || [];
              let layers = (currentTool && currentTool.layers) || [];

              this.elPickerList = el = rd$(el || this.elPickerList)`
                <div id="pickerlist">
                  <div id="pickerlist-top">
                    ${el => {
                      el = rd$(el)`<div id="pickerlist-tools"></div>`;

                      let list = new RDOMListHelper(el);

                      let tools = Array.from(this.cogwheel.map.tools.values());
                      tools.sort(
                        (a, b) => {
                          let aHasIndex = "index" in a;
                          let bHasIndex = "index" in b;
                          if (!aHasIndex) {
                            if (!bHasIndex) {
                              return a.id.localeCompare(b.id, undefined);
                            }
                            return -1;
                          } else if (!bHasIndex) {
                            return 1;
                          }
                          
                          return a.index - b.index;
                        }
                      );
        
                      for (let tool of tools) {
                        if (!tool.name)
                          continue;
                        list.add(tool.id,
                        /**
                         * @param {HTMLElement} el
                         */
                        el => {
                          el = mdcrd.button(mdcrd.icon((tool && tool.icon) || "brush"), () => {
                            this.cogwheel.map.currentTool = tool;
                          })(el);
                          this.setTooltip(el, tool.name, "down");
                          el.style.setProperty("--mdc-theme-primary", "black");
                          if (tool.id === this.cogwheel.session.tool) {
                            el.setAttribute("data-focused", "");
                          } else {
                            el.removeAttribute("data-focused");
                          }
                          return el;
                        });
                      }

                      return el;
                    }}

                    ${el => {
                      if (layers.length <= 0)
                        return null;

                      let list = [];

                      for (let layer of layers) {
                        list.push(
                        /**
                         * @param {HTMLElement} el
                         */
                        el => {
                          el = mdcrd.list.item(layer.name, () => {
                            this.cogwheel.session.layer = layer.id;
                            this.renderPicker();
                          })(el);
                          if (layer.id === this.cogwheel.session.layer) {
                            el.setAttribute("data-focused", "");
                          } else {
                            el.removeAttribute("data-focused");
                          }
                          return el;
                        });
                      }

                      el = mdcrd.list.list(...list)(el);
                      el.id = "pickerlist-layers";
                      return el;
                    }}

                    ${el => {
                      this.elPickerListSearch = el = mdcrd.textField("Search")(el);
                      el.id = "pickerlist-search";

                      requestAnimationFrame(() => {
                        let jets = el["jets"];
                        if (jets) {
                          jets.update();
                          return;
                        }

                        // @ts-ignore
                        jets = el["jets"] = new Jets({
                          searchTag: "#pickerlist-search input",
                          contentTag: "#pickerlist-entries",
                        });
                        
                        jets.search = ((orig) => (query, ...args) => {
                          let entries = this.elPickerListEntries;
                          let clusterize = entries["clusterize"];
                          let clusterizeWasEnabled = clusterize.enabled;
                          clusterize.disable();
                          if (clusterizeWasEnabled)
                            jets.update();

                          orig(query, ...args);

                          let prevWidth = parseInt(entries.style.width);
                          entries.style.width = "";
                          entries.style.width = Math.max(prevWidth, entries.getBoundingClientRect().width) + "px";

                          if (!query && !jets.search_tag.value)
                            clusterize.enable();
                        })(jets.search.bind(jets));
                      });

                      return entries.length <= 0 ? null : el;
                    }}
                  </div>

                  ${el => {
                    let clusterizeWasEnabled = true;
                    if (el) {
                      let clusterize = el["clusterize"];
                      if (clusterize) {
                        clusterizeWasEnabled = clusterize.enabled;
                        clusterize.disable();
                      }
                    }

                    let list = [];

                    let currentPlace = this.cogwheel.session.place;

                    for (let i in entries) {
                      let item = entries[i];
                      list.push(
                      /**
                       * @param {HTMLElement} el
                       */
                      el => {
                        el = mdcrd.list.item(el => rd$(el)`<span><img class="icon" src=${!item.icon ? "" : (item.icon.dataURL || item.icon)}>${item.name}</span>`, () => {
                          let skipRender = false;
                          if (item.select) {
                            skipRender = item.select();
                          } else {
                            this.cogwheel.session.place = item.id;
                          }
        
                          if (skipRender !== true)
                            this.renderPicker();
                        })(el);
      
                        if (item.active ? item.active() : currentPlace === item.id) {
                          el.setAttribute("data-focused", "");
                        } else {
                          el.removeAttribute("data-focused");
                        }
                        return el;
                      });
                    }

                    this.elPickerListEntries = el = mdcrd.list.list(...list)(el);
                    el.id = "pickerlist-entries";
                    el.classList.add("clusterize-content");

                    const clusterizeify = () => {
                      let jets = this.elPickerListSearch["jets"];
                      let research = jets && jets.search_tag.value;
                      if (research)
                        jets.search("");

                      let clusterize = el["clusterize"];
                      if (clusterize) {
                        if (clusterizeWasEnabled)
                          clusterize.enable();
                      } else {
                        // @ts-ignore
                        el["clusterize"] = new Clusterize({
                          scrollElem: this.elPickerList.parentElement,
                          contentElem: el
                        });
                      }

                      el.style.width = "";
                      el.style.width = el.getBoundingClientRect().width + "px";

                      if (research)
                        jets.search();
                    }

                    if (el.parentElement)
                      clusterizeify(); // Added to DOM. Run clusterizeify now.
                    else
                      requestAnimationFrame(clusterizeify); // Run clusterizeify after el is added to DOM.

                    return el;
                  }}
                </div>
              `;

              return el;
            }}
          </div>
          ${resizeButton(true, () => this.elPickerList.parentElement.parentElement)}
        </div>

      </main>

    </app>
    ${el => this.container.appendChild(el)}`;


    this.renderPicker = () => {
      let a = this.renderPickerMini();
      let b = this.renderPickerList();
      return a || b;
    }

    this.cogwheel.pixi.resize();
    let timeEnd = performance.now();
    console.log("[perf]", "CogwheelDOM.render", timeEnd - timeStart);
  }

}
