//@ts-check
import CONSTS from "../consts.js";
import { CogMap, CogMapEntity } from "../content/map.js";
import { CogAutotiler } from "../content/autotiler.js";
import { CogAtlas } from "../content/atlas.js";
import { CogBinEl } from "../content/binel.js";
import { CogwheelMapEntityType, CogwheelMapTool } from "./extras.js";

/**
 * @typedef {{ entity?: CogMap.Entity; node?: CogMap.EntityNode }} CogwheelMapSelectedEntry
 * @typedef {CogwheelMapSelectedEntry} CogwheelMap.SelectedEntry
 */

export class CogwheelMap {
  /**
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
    this.cogwheel = cogwheel;
  }

  resetPlugins() {
    /** @type {Map<string, CogwheelMapEntityType>} */
    this.entityTypes = new Map();
    this.entityTypes.set("__base", CogwheelMapEntityType.prototype);

    /** @type {Map<string, CogwheelMapTool>} */
    this.tools = new Map();
    this.tools.set("__base", CogwheelMapTool.prototype);
  }

  resetMap() {
    /** @type {WeakMap<CogMap.Entity, PIXI.DisplayObject>} */
    this.entityContainers = new WeakMap();

    this.autotilerBG = this.cogwheel.get(CogAutotiler, "BGTiles");
    this.autotilerFG = this.cogwheel.get(CogAutotiler, "FGTiles");
  }

  /** @type {CogwheelMapTool} */
  get currentTool() {
    return this._currentTool;
  }
  set currentTool(value) {
    if (this._currentTool && this._currentTool.end)
      this._currentTool.end();

    this._currentTool = value;
    
    if (value) {
      if (value.start)
        value.start();
      
      let layers = value.layers;
      if (layers.findIndex(l => l.id === this.cogwheel.session.layer) === -1)
        this.cogwheel.session.layer = layers[0].id;
    }
    
    this.cogwheel.session.tool = value ? value.id : "";
    this.cogwheel.dom.renderPicker();
    this.cogwheel.session.save();
  }

  async start() {
    this.scene = this.cogwheel.pixi.viewport;

    /** @type {Map<number, PIXI.display.Layer>} */
    this.depthLayers = new Map();
    this.depthIndices = {
      "underlay": -100000,
      "ghosts":   -10000,
      "bg":       0,
      "bgdecals": 1,
      "entities": 2,
      "solids":   3,
      "fgdecals": 4,
      "overlay":  100000,
    };

    this.textStyle = new PIXI.TextStyle({
      fontFamily: "Arial",
      fontSize: 32,
      fill: "#ffffff",
      stroke: '#000000',
      strokeThickness: 2
    });

    // Listen for pixi events.
    this.cogwheel.pixi.app.ticker.add((deltaTime) => this.update(deltaTime));

    this.cursorDragging = false;
    for (let type of ["up", "down"]) {
      this.cogwheel.pixi.interaction.on("mouse" + type, (epixi) => {
        /** @type {MouseEvent} */
        let e = epixi["data"]["trueEvent"] || epixi["data"]["originalEvent"];
        if (e.button !== 0)
          return;
        
        let cb = this.currentTool[type];
        if (type === "up") {
          this.cursorDragging = false;
        } else if (type === "down") {
          this.cursorDragging = true;
        }

        if (cb && this.currentLevelContainer && !this.cogwheel.reloading)
          cb.call(this.currentTool, new AVec(this.scene.toWorld(this.cogwheel.pixi.interaction.mouse.global)));
      });
    }

    this.selectedOverlay = new PIXI.Graphics();
    this.selectedOverlay.name = "selectedOverlay";
    this.selectedOverlay.parentGroup = this.getDepthGroup("overlay");
    /** @type {CogwheelMap.SelectedEntry[]} */
    this.selectedEntries = [];
  }

  /**
   * @param {string | number} depth
   */
  getDepthGroup(depth) {
    /** @type {number} */
    const index = typeof(depth) === "string" ? this.depthIndices[depth] : depth;

    let layer = this.depthLayers.get(index);
    if (layer)
      return layer.group;

    layer = new PIXI.display.Layer(new PIXI.display.Group(index, false));
    layer.name = depth.toString();

    this.cogwheel.pixi.app.stage.addChild(layer);
    this.depthLayers.set(index, layer);
    return layer.group;
  }

  /**
   * @param  {...number} except
   */
  getNewID(...except) {
    const map = this.cogwheel.get(CogMap, "Map");
    if (!map)
      return -1;

    let id = 0;
    for (let level of map)
      for (let entity of level.entities)
        id = Math.max(id, entity.id);
    return id + 1;
  }

  /**
   * @param {PIXI.DisplayObject} [sprite]
   */
  invalidate(sprite) {
    for (let container = sprite; container = container.parent;) {
      if (!container["ghostable"])
        continue;
      container["ghostDirty"] = true;
      break;
    }
  }

  get defaultTileEntries() {
    const layer = this.cogwheel.session.layer;
    const autotiler = layer === "bg" ? this.autotilerBG : this.autotilerFG;
    
    /** @type {typeof CogwheelMapTool.prototype.entries} */
    const list = [];

    const maskSingle = [
      0, 0, 0,
      0, 1, 0,
      0, 0, 0
    ];

    for (let entry of autotiler.lookup.entries()) {
      let id = entry[0];
      const type = entry[1];
      
      let masked = type.masked.find(data => {
        for (let i in data.mask) {
          if (data.mask[i] !== 2 && maskSingle[i] !== data.mask[i]) {
            return false;
          }
        }
        return true;
      });

      if (id === "z")
        id = "0";

      let icon = ((masked && masked.tiles) || type.center).textures[0];
      let name = icon.atlasPath;
      name = this.cogwheel.utils.humanify(name.slice(name.indexOf("/") + 1, name.indexOf("@")));

      if (name.startsWith("Bg "))
        name = name.slice(3);

      if (id === "0")
        name = "Air";
      
      list.push({
        id,
        name,
        icon
      })
    }
    return list;
  }

  get defaultTileLayers() {
    return [
      { name: "Foreground", short: "FG", id: "solids" },
      { name: "Background", short: "BG", id: "bg" },
    ];
  }

  get defaultEntityEntries() {
    const layer = this.cogwheel.session.layer;

    /** @type {typeof CogwheelMapTool.prototype.entries} */
    let all = [];

    if (layer === "entities") {
      for (let { 1: type } of this.entityTypes) {
        for (let i in (type.entries || [])) {
          let entry = type.entries[i];
          let id = type.id + ":" + i;
          all.push({
            id,
            name: entry.name || type.name,
            icon: null
          });
        }
      }

    } else {
      const atlas = this.cogwheel.get(CogAtlas, "Gameplay");
      for (let key of atlas.textures.keys()) {
        if (!key.startsWith("decals/"))
          continue;
        
        key = key.slice(7);
        all.push({
          id: "decal:" + key,
          name: key,
          icon: null
        });
      }
    }

    all.sort((a, b) => a.name.localeCompare(b.name, undefined));
    return all;
  }

  get defaultEntityLayers() {
    return [
      { name: "Entities", short: "E", id: "entities" },
      { name: "FG Decals", short: "FG", id: "fgdecals" },
      { name: "BG Decals", short: "BG", id: "bgdecals" },
    ];
  }

  /**
   * @param {number} deltaTime
   */
  update(deltaTime) {
    const map = this.cogwheel.get(CogMap, "Map");
    if (!map)
      return;

    const { renderer, viewport: scene } = this.cogwheel.pixi;
    // @ts-ignore Broken .d.ts
    const { x: vx, y: vy, width: vw, height: vh } = scene.getVisibleBounds();
    const currentLevelName = this.cogwheel.session.level;

    let renderBudget = (this.cogwheel.ready && this.cogwheel.reloading === 0) ? 20 : 0;

    let ghosted = false;

    for (let level of map) {
      let lx = level.x - CONSTS.VIEWPORT_LOAD_BORDER;
      let ly = level.y - CONSTS.VIEWPORT_LOAD_BORDER;
      let lw = level.width + 2 * CONSTS.VIEWPORT_LOAD_BORDER;
      let lh = level.height + 2 * CONSTS.VIEWPORT_LOAD_BORDER;
      if (
        lx + lw < vx || vx + vw < lx ||
        ly + lh < vy || vy + vh < ly
      ) {
        // Level outside visible bounds.
        continue;
      }

      /** @type {PIXI.Container} */
      // @ts-ignore
      let container = scene.getChildByName("level:" + level.name);

      // Render as many levels as we can. Add dummies and ghosts for the rest, and remove / hide them afterwards.

      /** @type {PIXI.Sprite} */
      // @ts-ignore
      let dummy = scene.getChildByName("dummy:" + level.name);
      if (!container) {
        if (renderBudget > 0) {
          let then = performance.now();
          container = this.renderLevel(level);
          renderBudget -= performance.now() - then;
          container.visible = !ghosted;
          if (dummy)
            scene.removeChild(dummy);

        } else if (!dummy) {
          dummy = new PIXI.Sprite(PIXI.Texture.WHITE);
          dummy.name = "dummy:" + level.name;
          dummy.tint = CONSTS.COLORS_FGTILES[level.editorColorIndex];
          dummy.position.set(level.x, level.y);
          dummy.scale.set(level.width / 16, level.height / 16);
          dummy.interactive = true;
          dummy.on("mousedown", e => {
            if (e["data"].originalEvent.button < 2) {
              this.focusLevel(level, false, true);
              scene.removeChild(dummy);
            }
          });
          dummy.visible = !ghosted;
          scene.addChild(dummy);
        }

      } else if (dummy) {
        scene.removeChild(dummy);

      }

      if (!container)
        continue;

      const active = currentLevelName === level.name;

      /** @type {PIXI.Sprite} */
      let ghost = container["ghost"];
      if (!active) {
        if (!ghost)
          // @ts-ignore
          ghost = this.scene.getChildByName("ghost:" + level.name);

        if (!ghost || container["ghostDirty"]) {
          container["ghostDirty"] = false;
          if (!ghosted) {
            ghosted = true;
            for (let container of this.scene.children)
              if (container.name.startsWith("level:") || container.name.startsWith("dummy:"))
                container.visible = false;
          }

          const { x, y } = container.position;
          container.position.set(0, 0);
          container.visible = true;

          /** @type {PIXI.RenderTexture} */
          // @ts-ignore
          let rt = ghost ? ghost.texture : PIXI.RenderTexture.create(level.width, level.height);

          renderer.batch.flush();
      
          renderer.renderTexture.bind(rt);
          
          renderer.clear();

          this.refreshLevelActive(container);
          renderer.render(container, rt, true);

          this.cogwheel.pixi.stage.updateStage();
          let layers = Array.from(this.depthLayers.entries()).sort((a, b) => a[0] - b[0]).map(a => a[1]);
          for (let layer of layers) {
            if (layer.name === "overlay")
              continue;
            renderer.render(layer, rt, false);
          }
          
          let first = !ghost;
          ghost = ghost || new PIXI.Sprite(rt);
          ghost.name = "ghost:" + level.name;
          ghost.visible = true;
          ghost.alpha = 0.7;

          if (first) {
            ghost.interactive = true;
            ghost.on("mousedown", e => {
              if (e["data"].originalEvent.button < 2) {
                this.focusLevel(level, false, true);
                ghost.visible = false;
              }
            });
            this.scene.addChildAt(ghost, this.scene.getChildIndex(container));
          }
          
          renderer.batch.flush();

          renderer.renderTexture.bind(null);

          ghost.position.set(x, y);
          container.position.set(x, y);
          container.visible = false;
        }
  
        container["ghost"] = ghost;

      } else if (ghost) {
        ghost.visible = false;
      }

    }

    for (let container of this.scene.children) {
      if (container.name.startsWith("level:")) {
        let active = currentLevelName === container["level"].name;
        container.visible = active;
        if (active)
          container["ghostDirty"] = true;

      } else if (container.name.startsWith("dummy:")) {
        container.visible = true;
      }
    }

    let selectedOverlay = this.selectedOverlay;
    scene.removeChild(selectedOverlay);
    scene.addChild(selectedOverlay);

    this.cursorPosPrev = this.cursorPos;
    this.cursorPos = new AVec(this.scene.toWorld(this.cogwheel.pixi.interaction.mouse.global));
    if (!this.cursorPosPrev)
      this.cursorPosPrev = this.cursorPos;
    this.cursorDelta = this.cursorPos.vsub(this.cursorPosPrev);

    if (this.currentTool && this.currentLevelContainer && !this.cogwheel.reloading) {
      if (this.currentTool.update)
        this.currentTool.update(deltaTime);
      if (this.cursorDragging && this.currentTool.drag)
        this.currentTool.drag(this.cursorPos, this.cursorDelta);
      if (this.currentTool.move)
        this.currentTool.move(this.cursorPos, this.cursorDelta);
    }

    selectedOverlay.clear();
    selectedOverlay.lineStyle(1, 0xdd0aee, 0.9);
    selectedOverlay.beginFill(0xdd0aee, 0.3);

    let selectedEntries = this.selectedEntries;
    for (let { entity, node } of selectedEntries) {
      if (entity) {
        let container = this.entityContainers.get(entity);
        if (!container)
          continue;
        container.parentGroup = selectedOverlay.parentGroup;
        let type = this.getEntityType(entity);
        let { 0: bx, 1: by, 2: bw, 3: bh } = type.getBounds(entity);
        selectedOverlay.drawRect(entity._level.x + bx + entity.x, entity._level.y + by + entity.y, bw, bh);

      } else if (node) {
        entity = node._entity;
        let container = this.entityContainers.get(entity);
        if (!container || node._entity._nodes.indexOf(node) === -1)
          continue;
        container.parentGroup = selectedOverlay.parentGroup;
        let type = this.getEntityType(entity);
        let { 0: bx, 1: by, 2: bw, 3: bh } = type.getNodeBounds(node);
        selectedOverlay.drawRect(entity._level.x + bx + node.x, entity._level.y + by + node.y, bw, bh);
      }
    }

    this.cogwheel.pixi.renderer.once("postrender", () => {
      for (let { entity, node } of selectedEntries) {
        if (node)
          entity = node._entity;
        if (entity) {
          let container = this.entityContainers.get(entity);
          if (!container)
            continue;
          container.parentGroup = null;
          container._activeParentLayer = null;
        }
      }
    });
  }

  /**
   * @param {PIXI.Container} container
   */
  refreshLevelActive(container) {
    /** @type {CogMap.Level} */
    let level = container["level"];

    let box = container.getChildByName("box");
    let border = container.getChildByName("border");

    let active = this.cogwheel.session.level === level.name;
    box.interactive = !active;
    box.alpha = !active ? 0.3 : 1;
    border.alpha = !active ? 0.2 : 0.05;

    // container.hitArea = box.hitArea = new PIXI.Rectangle(0, 0, level.width, level.height);

    if (container.mask && container.mask.parent === container) {
      container.removeChild(container.mask);
      container.mask = null;
    }

    // Make the current active level render on top of all other levels.
    let parent = container.parent;
    if (active && parent) {
      this.currentLevel = level;
      this.currentLevelContainer = container;
      parent.removeChild(container);
      parent.addChild(container);
    }
  }

  /**
   * @param {CogMap.Entity} entity
   */
  getEntityType(entity) {
    return this.entityTypes.get(entity._bin.name) || this.entityTypes.get("dummy");
  }

  /**
   * @param {CogMap.Level} level
   * @param {number} x
   * @param {number} y
   */
  isSolid(level, x, y) {
    if (!level)
      level = this.currentLevel;
    if (level.solids.get(x / 8, y / 8) !== "0")
      return true;
    
    let point = [x, y, 0, 0];
    for (let entity of level.entities) {
      let type = this.getEntityType(entity);
      if (!type.isSolid)
        continue;

      // TODO: Solid entities!
      let bounds = type.getBounds(entity).vadd(entity);
      if (!bounds)
        continue;

      if (this.cogwheel.utils.isOverlap(point, bounds))
        return true;
    }

    return false;
  }

  /**
   * @param {number[]} bounds
   * @param {CogMap.Entity} entity
   * @param {CogwheelMapEntityType} [type]
   */
  getOverlaps(bounds, entity, type) {
    if (!type)
      type = this.getEntityType(entity);

    let matches = [];
      
    if (this.cogwheel.utils.isOverlap(bounds, type.getBounds(entity).vadd(entity))) {
      matches.push({ entity });
    }

    for (let node of entity._nodes) {
      if (this.cogwheel.utils.isOverlap(bounds, type.getNodeBounds(node).vadd(node))) {
        matches.push({ node });
      }
    }

    return matches;
  }

  /**
   * @param {number[]} bounds
   * @param {CogMap.Entity | CogMap.EntityNode} entityOrNode
   * @param {CogwheelMapEntityType} [type]
   */
  isOverlap(bounds, entityOrNode, type) {
    let node = entityOrNode instanceof CogMap.EntityNode ? entityOrNode : null;
    let entity = entityOrNode instanceof CogMap.Entity ? entityOrNode : null;

    if (!type)
      type = this.getEntityType(entity || node._entity);

    return (
      (entity && this.cogwheel.utils.isOverlap(bounds, type.getBounds(entity).vadd(entity))) ||
      (node && this.cogwheel.utils.isOverlap(bounds, type.getNodeBounds(node).vadd(node)))
    );
  }

  /**
   * @param {CogMap.Level} level
   * @param {string} layer
   * @param {number[]} selectBounds
   */
  selectEntities(level, layer, selectBounds) {
    let levelContainer = !level ? this.currentLevelContainer : this.scene.getChildByName("level:" + level.name);
    // @ts-ignore
    let layerContainer = levelContainer.getChildByName(layer);
    level = this.currentLevel;

    this.selectedEntries = [];

    for (let container of layerContainer.children) {
      /** @type {CogMap.Entity} */
      let entity = container["entity"];
      /** @type {CogwheelMapEntityType} */
      let type = container["entityType"];

      this.selectedEntries.push(...this.getOverlaps(selectBounds, entity, type));
    }

    return this.selectedEntries;
  }

  /**
   * @param {CogAutotiler.TerrainType} type
   * @param {string} value
   * @param {string} target
   */
  getMaskValue(type, value, target) {
    return target !== "0" && !type.ignores.has(target) && !(type.ignores.has("*") && value !== target);
  };

  /**
   * @param {CogAutotiler} autotiler
   * @param {CogMap.LevelTiles} tiles
   * @param {string} value
   * @param {number} x
   * @param {number} y
   */
  getMaskQuads(autotiler, tiles, value, x, y) {
    const type = autotiler.lookup.get(value);

    if (!type)
      return null;
      
    const maskValue0 = this.getMaskValue(type, value, tiles.get(x - 1 + 0, y - 1 + 0));
    const maskValue1 = this.getMaskValue(type, value, tiles.get(x - 1 + 1, y - 1 + 0));
    const maskValue2 = this.getMaskValue(type, value, tiles.get(x - 1 + 2, y - 1 + 0));
    const maskValue3 = this.getMaskValue(type, value, tiles.get(x - 1 + 0, y - 1 + 1));
    const maskValue4 = this.getMaskValue(type, value, tiles.get(x - 1 + 1, y - 1 + 1));
    const maskValue5 = this.getMaskValue(type, value, tiles.get(x - 1 + 2, y - 1 + 1));
    const maskValue6 = this.getMaskValue(type, value, tiles.get(x - 1 + 0, y - 1 + 2));
    const maskValue7 = this.getMaskValue(type, value, tiles.get(x - 1 + 1, y - 1 + 2));
    const maskValue8 = this.getMaskValue(type, value, tiles.get(x - 1 + 2, y - 1 + 2));

    for (const data of type.masked) {
      // Unrolled optimized loop.
      const mask = data.mask;
      const mask0 = mask[0];
      const mask1 = mask[1];
      const mask2 = mask[2];
      const mask3 = mask[3];
      const mask4 = mask[4];
      const mask5 = mask[5];
      const mask6 = mask[6];
      const mask7 = mask[7];
      const mask8 = mask[8];
      if (
        !(mask0 !== 2 && maskValue0 !== (mask0 > 0)) &&
        !(mask1 !== 2 && maskValue1 !== (mask1 > 0)) &&
        !(mask2 !== 2 && maskValue2 !== (mask2 > 0)) &&
        !(mask3 !== 2 && maskValue3 !== (mask3 > 0)) &&
        !(mask4 !== 2 && maskValue4 !== (mask4 > 0)) &&
        !(mask5 !== 2 && maskValue5 !== (mask5 > 0)) &&
        !(mask6 !== 2 && maskValue6 !== (mask6 > 0)) &&
        !(mask7 !== 2 && maskValue7 !== (mask7 > 0)) &&
        !(mask8 !== 2 && maskValue8 !== (mask8 > 0))
      )
        return data.tiles;
    }

    return (
      tiles.get(x - 2, y) === "0" || tiles.get(x + 2, y) === "0" ||
      tiles.get(x, y - 2) === "0" || tiles.get(x, y + 2) === "0"
    ) ? type.padded : type.center;
  }

  /**
   * @param {CogMap.Level} level
   * @param {CogMap.LevelTiles} tiles
   * @param {number[]} [bounds]
   */
  renderTilemap(level, tiles, bounds) {
    level = level || this.currentLevel;
    const autotiler = tiles._type === "bg" ? this.autotilerBG : this.autotilerFG;

    /** @type {PIXI.Sprite} */
    let rtSprite;

    /** @type {PIXI.Container} */
    // @ts-ignore
    let levelContainer = this.scene.getChildByName("level:" + level.name);
    if (!levelContainer)
      return;
    
    // @ts-ignore
    rtSprite = levelContainer.getChildByName(tiles._type);

    if (!rtSprite) {
      rtSprite = new PIXI.Sprite(null);
      rtSprite.name = tiles._type;
    }

    rtSprite.interactive = false;
    rtSprite.interactiveChildren = false;

    /** @type {PIXI.RenderTexture} */
    // @ts-ignore
    let rt = rtSprite.texture;
    if (!rt || !(rt instanceof PIXI.RenderTexture)) {
      rt = PIXI.RenderTexture.create({ width: level.width, height: level.height, scaleMode: PIXI.SCALE_MODES.NEAREST, resolution: 1 });
      rtSprite.texture = rt;
    }

    this.cogwheel.utils.updateChild(levelContainer, rtSprite);

    const maxWidth = level.width / 8;
    const maxHeight = level.height / 8;

    const boundsX = bounds ? bounds[0] : 0;
    const boundsY = bounds ? bounds[1] : 0;
    const boundsWidth = bounds ? bounds[2] : maxWidth;
    const boundsRight = bounds ? boundsX + bounds[2] : maxWidth;
    const boundsHeight = bounds ? bounds[3] : maxHeight;
    const boundsBottom = bounds ? boundsY + bounds[3] : maxHeight;

    let renderer = this.cogwheel.pixi.renderer;

    renderer.batch.flush();

    renderer.renderTexture.bind(rt);

    if (bounds) {
      // The following code somehow doesn't properly pop the mask.
      /*
      let mask = new PIXI.Graphics();
      mask.beginFill(0xffffff);
      mask.drawRect(
        boundsX * 8,
        boundsY * 8,
        boundsWidth * 8,
        boundsHeight * 8
      );
      renderer.stencilManager.pushStencil(mask);
      renderer.clear(0);
      renderer.stencilManager.popStencil();
      */
      
      let gl = renderer.gl;
      gl.enable(gl.SCISSOR_TEST); 
      gl.scissor(boundsX * 8, boundsY * 8, boundsWidth * 8, boundsHeight * 8);
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);
      gl.disable(gl.SCISSOR_TEST);

    } else {
      // This breaks rendering the BG tilemap when scrolling.
      // renderer.clear(0);

      let gl = renderer.gl;
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);
    }

    let sprite = new PIXI.Sprite();
      
    // let prng = this.cogwheel.utils.prng((level.x << 16) | level.y + level.width + level.height)
    for (let y = boundsY; y < boundsBottom; y++) {
      if (y < 0 || maxHeight <= y)
        continue;
      for (let x = boundsX; x < boundsRight; x++) {
        if (x < 0 || maxWidth <= x)
          continue;

        /*
        let prngA = prng();
        let prngB = prng();
        */

        let value = tiles.get(x, y);
        if (value === "0")
          continue;
        let quads = this.getMaskQuads(autotiler, tiles, value, x, y);

        if (!quads || quads.textures.length === 0) {
          sprite.texture = PIXI.Texture.WHITE;
          sprite.tint = 0xFF0000;
          sprite.scale.set(8 / 16, 8 / 16);

        } else {
          // let i = Math.floor(prngA * quads.textures.length) + Math.floor(prngB * quads.textures.length);
          let i = this.cogwheel.utils.prngAt(level.x + x, level.y + y, quads.textures.length);
          sprite.texture = this.cogwheel.pixi.get(quads.textures[i]).texture;
          sprite.tint = 0xFFFFFF;
          sprite.scale.set(1, 1);
        }

        sprite.position.set(x * 8, y * 8);
        renderer.clearBeforeRender = false;
        renderer.render(sprite, rt, false);
      }
    }

    renderer.batch.flush();

    renderer.renderTexture.bind(null);
    renderer.clearBeforeRender = true;

    rtSprite.parentGroup = this.getDepthGroup(tiles._type);
    this.invalidate(rtSprite);
    return rtSprite;
  }

  /**
   * @param {CogMap.Level} level
   * @param {string} group
   */
  renderEntities(level, group) {
    level = level || this.currentLevel;

    const entitiesContainer = new PIXI.Container();
    entitiesContainer.name = group;

    entitiesContainer.interactive = false;
    entitiesContainer.interactiveChildren = false;
    
    // @ts-ignore
    this.cogwheel.utils.updateChild(this.scene.getChildByName("level:" + level.name), entitiesContainer);

    /** @type {CogMap.Entity[]} */
    const entities = level[group];
    for (let entity of entities) {
      let uid = this.cogwheel.utils.getUID(entity).toString();
      const container = this._renderEntity(entity);
      this.entityContainers.set(entity, container);
      this.cogwheel.utils.updateChild(entitiesContainer, container, uid);
    }

    entitiesContainer.parentGroup = this.getDepthGroup(group);
    this.invalidate(entitiesContainer);
    return entitiesContainer;
  }

  /**
   * @param {CogMap.Entity} entity
   */
  renderEntity(entity) {
    /** @type {PIXI.Container} */
    // @ts-ignore
    const levelContainer = entity._level && this.scene.getChildByName("level:" + entity._level.name);
    /** @type {PIXI.Container} */
    // @ts-ignore
    const entitiesContainer = levelContainer && levelContainer.getChildByName(entity._group);

    /** @type {PIXI.DisplayObject} */
    let container = null;

    let uid = this.cogwheel.utils.getUID(entity).toString();
    
    if (!entitiesContainer || entity._level[entity._group].indexOf(entity) !== -1) {
      container = this._renderEntity(entity);
    }

    if (entitiesContainer) {
      this.entityContainers.set(entity, container);
      this.cogwheel.utils.updateChild(entitiesContainer, container, uid);
      this.invalidate(entitiesContainer);
    } else {
      container.name = uid;
    }

    return container;
  }

  /**
   * @param {CogMap.Entity} entity
   */
  _renderEntity(entity) {
    const entityType = this.getEntityType(entity);
    const container = entityType.render(entity);

    this.cogwheel.utils.resolveSprites(container);

    container.position.set(entity.x, entity.y);

    container["entityType"] = entityType;
    container["entity"] = entity;

    return container;
  }

  /**
   * @param {CogMap.Level} [level]
   */
  renderLevel(level) {
    level = level || this.currentLevel;

    const timeStart = performance.now();

    /**
     * @param {number} color
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     */
    const renderBorder = (color, x, y, w, h) => {
      const box = new PIXI.Sprite(PIXI.Texture.WHITE);
      box.tint = color;
      box.position.set(x, y);
      box.scale.set(w / 16, h / 16);
      return box;
    }

    const container = new PIXI.Container();
    container.position.set(level.x, level.y);
    container.name = "level:" + level.name;
    container["level"] = level;

    this.cogwheel.utils.updateChild(this.scene, container);

    const box = new PIXI.Sprite(PIXI.Texture.WHITE);
    box.name = "box";
    box.tint = CONSTS.COLORS_LEVELBG[level.editorColorIndex];
    box.scale.set(level.width / 16, level.height / 16);
    box.on("mousedown", e => {
      if (e["data"].originalEvent.button < 2)
        this.focusLevel(level, false, true);
    });
    // box.parentGroup = this.getDepthGroup("underlay");
    container.addChild(box);

    this.renderTilemap(level, level.bg);
    this.renderEntities(level, "bgdecals");
    this.renderEntities(level, "entities");
    this.renderTilemap(level, level.solids);
    this.renderEntities(level, "fgdecals");

    const border = new PIXI.Container();
    border.name = "border";
    border.parentGroup = this.getDepthGroup("overlay");
    container.addChild(border);

    const borderColor = CONSTS.COLORS_FGTILES[level.editorColorIndex];
    border.addChild(renderBorder(borderColor, 0, 0, level.width, 2));
    border.addChild(renderBorder(borderColor, 0, level.height - 2, level.width, 2));
    border.addChild(renderBorder(borderColor, 0, 2, 2, level.height - 4));
    border.addChild(renderBorder(borderColor, level.width - 2, 2, 2, level.height - 4));

    const timeEnd = performance.now();
    console.log("[perf]", `CogwheelMap.renderLevel(${level.name})`, timeEnd - timeStart);
    this.refreshLevelActive(container);

    container["ghostable"] = true;
    return container;
  }

  /**
   * @param {CogBinEl} bin
   */
  renderFiller(bin) {
    const map = this.cogwheel.get(CogMap, "Map");

    let x = bin.getAttr("x");
    let y = bin.getAttr("y");
    let w = bin.getAttr("w");
    let h = bin.getAttr("h");

    const box = new PIXI.Sprite(PIXI.Texture.WHITE);
    box.name = "filler";
    box["filler"] = bin;
    box.tint = CONSTS.COLOR_FILLER;
    box.position.set(x * 8, y * 8);
    box.scale.set(w * 8 / 16, h * 8 / 16);

    return box;
  }

  renderAll(levels = false) {
    const timeStart = performance.now();

    this.scene.removeChildren();

    const map = this.cogwheel.get(CogMap, "Map");
    const mapBounds = map.bounds;

    for (let filler of map._bin.get("Filler").children) {
      let container = this.renderFiller(filler);
      this.scene.addChild(container);
    }

    if (levels) {
      for (let level of map) {
        this.renderLevel(level);
      }
    }

    this.scene.worldWidth = mapBounds[2];
    this.scene.worldHeight = mapBounds[3];

    const timeEnd = performance.now();
    console.log("[perf]", `CogwheelMap.renderAll(${levels})`, timeEnd - timeStart);
  }

  /**
   * @param {CogMap.Level} level
   * @param {boolean} [snapCamera]
   * @param {boolean} [scrollList]
   */
  focusLevel(level, snapCamera = false, scrollList = true) {
    const map = this.cogwheel.get(CogMap, "Map");

    if (!level) {
      for (let other of map) {
        level = other;
        break;
      }
    }

    this.cogwheel.session.level = level.name;
    this.cogwheel.session.save();

    for (let container of this.scene.children)
      if (container.name.startsWith("level:") && container instanceof PIXI.Container)
        this.refreshLevelActive(container);

    this.cogwheel.dom.renderLevelList(null, scrollList);

    if (!snapCamera)
      return;

    this.scene.snap(level.x + level.width * 0.5, level.y + level.height * 0.5, {
      time: 200,
      interrupt: true,
      removeOnComplete: true,
      removeOnInterrupt: true
    });

    // 1 / 2^N
    let widthDivScale = this.scene.screenWidth / (level.width + CONSTS.VIEWPORT_ZOOM_BORDER * 2);
    // @ts-ignore Broken .d.ts
    let heightDivScale = this.scene.screenHeight / (level.height + CONSTS.VIEWPORT_ZOOM_BORDER * 2);
    let maxDivScale = this.cogwheel.utils.nextPowerOfTwo(Math.ceil(Math.min(widthDivScale, heightDivScale)));

    // 2^N
    let widthMulScale = (level.width + CONSTS.VIEWPORT_ZOOM_BORDER * 2) / this.scene.screenWidth;
    // @ts-ignore Broken .d.ts
    let heightMulScale = (level.height + CONSTS.VIEWPORT_ZOOM_BORDER * 2) / this.scene.screenHeight;
    let maxMulScale = this.cogwheel.utils.nextPowerOfTwo(Math.ceil(Math.max(widthMulScale, heightMulScale)));

    if (maxMulScale !== 1)
      maxDivScale = 1;

    /** @type {any} */
    let zoom = this.scene.plugins.get("wheel");
    zoom.smoothingCount = 0;
    zoom.smoothingCenter = new PIXI.Point(level.x + level.width * 0.5, level.y + level.height * 0.5);
    zoom.smoothingTarget = maxDivScale / maxMulScale;
    zoom.smoothingTargetPrev = zoom.smoothingTargetPrev || zoom.parent.scale.x;
  }

}

CogwheelMap.Tool = CogwheelMapTool;
CogwheelMap.EntityType = CogwheelMapEntityType;
