//@ts-check
import { CogMap } from "../content/map.js";
import { CogSubtexture } from "../content/texture.js";

/**
 * @typedef {{ name: string; data: { [key: string]: any }; nodes?: [number, number][] }} CogwheelMapEntityTypeEntry
 */

/**
 * @abstract
 */
export class CogwheelMapEntityType {
  /** @type {string} */
  id;
  /** @type {string} */
  name;

  /** @type {boolean} */
  isSolid = false;

  /** @type {{ [id: string]: (string | { type: string; default: any }); }} */
  props;

  // @ts-ignore
  /** @type {{ [id: string | number]: CogwheelMapEntityTypeEntry }} */
  entries;

  /** @type {AVec} */
  defaultNodeSize;

  /** @type {number} */
  nodesMin;
  /** @type {number} */
  nodesMax;

  /**
   * @abstract
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
  }

  async load() {
  }

  /**
   * @param {string | number} id
   * @returns {CogwheelMapEntityTypeEntry}
   */
  getEntry(id) {
    return this.entries[id];
  }

  /**
   * @param {CogMap.Entity} entity
   * @returns {AVec}
   */
  getBounds(entity) {
    /** @type {import("../cogwheel.js").Cogwheel} */
    let cogwheel = window["cogwheel"];

    let container = this.render(entity);
    cogwheel.utils.resolveSprites(container);
    let bounds = container.getBounds();

    let p1 = new PIXI.Point(bounds.left, bounds.top);
    let p2 = new PIXI.Point(bounds.right, bounds.bottom);
    
    if (container.parent) {
      p1 = container.parent.toLocal(p1);
      p1.x -= entity.x;
      p1.y -= entity.y;
      p2 = container.parent.toLocal(p2);
      p2.x -= entity.x;
      p2.y -= entity.y;
    }

    let { x: left, y: top } = p1;
    let { x: right, y: bottom } = p2;
    return new AVec(left, top, right - left, bottom - top);
  }

  /**
   * @param {CogMap.EntityNode} node
   * @returns {AVec}
   */
  getNodeBounds(node) {
    let size = new AVec(this.defaultNodeSize || [ 4, 4 ]);
    return new AVec(0, 0, ...size).vsub(size.vmul(0.5));
  }

  /**
   * @param {CogMap.Entity} entity
   * @returns {PIXI.DisplayObject}
   */
  render(entity) {
    return null;
  }

}

/**
 * @abstract
 */
export class CogwheelMapTool {
  /** @type {string} */
  id;
  /** @type {string} */
  name;

  /** @type {number} */
  index;

  /**
   * @abstract
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
  }

  /** @returns {string} */
  get icon() {
    return null;
  }

  /** @returns {{ id: string, name: string, icon: CogSubtexture, select?: () => any, active?: () => boolean, renderEntry?: (el: HTMLElement) => HTMLElement }[]} */
  get entries() {
    return null;
  }

  /** @returns {{ name: string, short: string, id: string }[]} */
  get layers() {
    return null;
  }

  start() {
  }

  end() {
  }

  /** @param {number} deltaTime */
  update(deltaTime) {

  }

  /**
   * @param {AVec} at
   * @param {AVec} delta
   */
  move(at, delta) {

  }

  /** @param {AVec} at */
  down(at) {

  }

  /** @param {AVec} at */
  up(at) {

  }

  /**
   * @param {AVec} at
   * @param {AVec} delta
   */
  drag(at, delta) {

  }

}
