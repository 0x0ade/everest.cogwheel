//@ts-check
import CONSTS from "../consts.js";
import { CogMap } from "../content/map.js";
import { CogAutotiler } from "../content/autotiler.js";
import { CogAtlas } from "../content/atlas.js";
import { CogBinEl } from "../content/binel.js";
import { CogwheelMapEntityType, CogwheelMapTool } from "./extras.js";

/**
 * @typedef {() => void} UndoAction
 * @typedef {() => void} RedoAction
 * @typedef {{ undo: UndoAction, redo: RedoAction }} HistoryEntry
 */

export class CogwheelHistory {
  /**
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
    this.cogwheel = cogwheel;

    this.reset();
  }

  reset() {
    /** @type {HistoryEntry[]} */
    this.stackUndo = [];
    /** @type {HistoryEntry[]} */
    this.stackUndone = [];
  }

  async start() {
  }

  /**
   * @param {UndoAction} undo
   * @param {RedoAction} redo
   * @param {boolean} [firstRun]
   */
  push(undo, redo, firstRun = true) {
    this.stackUndo.push({ undo, redo });
    if (this.stackUndone.length)
      this.stackUndone = [];
    if (firstRun)
      redo();
  }

  undo() {
    let entry = this.stackUndo.pop();
    if (!entry) {
      this.cogwheel.snackbar({ text: "Nothing to undo." });
      return;
    }
    entry.undo();
    this.stackUndone.push(entry);
  }

  redo() {
    let entry = this.stackUndone.pop();
    if (!entry) {
      this.cogwheel.snackbar({ text: "Nothing to redo." });
      return;
    }
    entry.redo();
    this.stackUndo.push(entry);
  }

}
