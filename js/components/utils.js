//@ts-check
import { CogAtlas } from "../content/atlas.js";
import { CogSubtexture } from "../content/texture.js";

const REGEX_UPPERATOZ = /[A-Z]/g;

export class CogwheelUtils {
  /**
   * @param {import("../cogwheel.js").Cogwheel} cogwheel
   */
  constructor(cogwheel) {
    this.cogwheel = cogwheel;

    /** @type {WeakMap<any, number>} */
    this._uidMap = new WeakMap();
    this._uidLast = 0;
  }

  /**
   * @param {any} ref
   */
  getUID(ref) {
    if (this._uidMap.has(ref))
      return this._uidMap.get(ref);

    let value = this._uidLast++;
    this._uidMap.set(ref, value);
    return value;
  }

  /**
   * @param {() => void} func
   */
  time(func) {
    let then = performance.now();
    func();
    let now = performance.now();
    return now - then;
  }

  /**
   * @param {number[]} a
   * @param {number[]} b
   */
  isOverlap(a, b) {
    if (!a || !b)
      return false;

    let { 0: ax, 1: ay, 2: aw, 3: ah } = a;
    let { 0: bx, 1: by, 2: bw, 3: bh } = b;

    if (a.length <= 2) {
      aw = 0;
      ah = 0;
    }

    if (b.length <= 2) {
      bx -= 1;
      by -= 1;
      bw = 2;
      bh = 2;
    }

    return (
      bx <= ax + aw && ax <= bx + bw &&
      by <= ay + ah && ay <= by + bh
    );
  }

  /**
   * @param {number} v
   */
  nextPowerOfTwo(v) {
    if (v === 0)
      return 1;
    if (v === 1)
      return 1;
    v--;
    let p = 2;
    while (v >>= 1)
      p <<= 1;
    return p;
  }

  /**
   * @param {number} start
   * @param {number} end
   * @param {number} f
   */
  lerp(start, end, f) {
    return (1 - f) * start + f * end;
  }

  /**
   * @param {number} seed
   * @param {number} [seedY]
   * @param {number} [seedZ]
   */
  prng(seed, seedY, seedZ) {
    if (seedY)
      seed = this.prngSeedAt(seed, seedY, seedZ);

    function prngNext() {
      seed = Math.imul(48271, seed) | 0 % 2147483647;
      return (seed & 2147483647) / 2147483648;  
    }

    /**
     * @template T
     * @param {T[]} array
     */
    prngNext.pick = (array) => array[Math.floor((prngNext() * 719 * array.length + prngNext() * 119 * array.length) % array.length)];
    
    return prngNext;
  }

  /**
   * @param {number} x
   * @param {number} y
   * @param {number} [mod]
   */
  prngAt(x, y, mod) {
    mod = mod || 0;

    let a = ((x * 71317 + mod) << 16) | (y * 51713 + mod);
    a = Math.imul(48271, a) | 0 % 2147483647;
    if (!mod)
      return a;
    
    let b = Math.imul(48271, a) | 0 % 2147483647;
    let c = Math.imul(48271, b) | 0 % 2147483647;
    a = (a & 2147483647) / 2147483648;
    b = (b & 2147483647) / 2147483648;
    c = (c & 2147483647) / 2147483648;
    a = Math.floor(a * mod);  
    b = Math.floor(b * mod);  
    c = Math.floor(c * mod);
    let i = a + b + c;
    i = ((i % mod) + 2 * mod) % mod;
    return i;
  }

  /**
   * @param {number} x
   * @param {number} y
   * @param {number} [z]
   */
  prngSeedAt(x, y, z) {
    z = z || 0;

    let a = ((x * 71317 + z) << 16) | (y * 51713 + z);
    a = Math.imul(48271, a) | 0 % 2147483647;
    if (!z)
      return a;
    
    let b = Math.imul(48271, a) | 0 % 2147483647;
    let c = Math.imul(48271, b) | 0 % 2147483647;
    a = (a & 2147483647) / 2147483648;
    b = (b & 2147483647) / 2147483648;
    c = (c & 2147483647) / 2147483648;
    return  a + b + c;
  }

  /**
   * @param {AVec} from
   * @param {AVec} to
   * @param {AVec} control
   */
  curve(from, to, control) {
    /**
     * @param {number} percent
     */
    function curveGet(percent) {
      return $$$({from, to, control, percent, i: 1 - percent})`
        from * i * i +
        control * 2 * i * percent +
        to * percent * percent`;
    }
    return curveGet;
  }

  /**
   * @param {number | string} key
   */
  hexcolor(key) {
    if (typeof key === "number")
      return key;
    if (key[0] === "#")
      key = key.slice(1);
    return parseInt(key, 16);
  }

  /**
   * @param {string} path
   * @param {boolean} [typeAsSuffix]
   */
  humanify(path, typeAsSuffix = true) {
    let typeIndex = path.lastIndexOf("/");

    let name = "";
    let cPrev = " ";
    for (let i = typeAsSuffix ? typeIndex + 1 : 0; i < path.length; i++) {
      let c = path[i];

      if (c === "_" || c === "/")
        c = " ";
      if (cPrev === " ")
        c = c.toUpperCase();
      if (REGEX_UPPERATOZ.test(c))
        name += " ";

      name += c;
      cPrev = name[name.length - 1];
    }

    if (typeAsSuffix && typeIndex !== -1)
      name += " (" + this.humanify(path.slice(0, typeIndex), false) + ")";
    return name.trim();
  }

  /**
   * @param {string | CogSubtexture} textureOrPath
   * @param {(sprite: PIXI.Sprite) => void} [cb]
   */
  sprite(textureOrPath, cb) {
    const atlas = this.cogwheel.get(CogAtlas, "Gameplay");

    /** @type {CogSubtexture} */
    let textureCog = null;
    if (textureOrPath instanceof CogSubtexture) {
      textureCog = textureOrPath;
    } else if (textureOrPath) {
      let texturePath = textureOrPath;
      let texturePathExtIndex = texturePath.lastIndexOf(".");
      if (texturePathExtIndex !== -1) {
        texturePath = texturePath.split(texturePath.slice(texturePathExtIndex)).join("");
      }
      textureCog = atlas.get(texturePath);
    }
    
    let sprite = new PIXI.Sprite();
    sprite["cogLoading"] = true;

    // Sprites can be loaded on the fly.
    sprite["cogTexture"] = textureCog;
    if (textureCog) {
      const resolved = texturePixi => {
        if (!sprite["cogLoading"])
          return;

        sprite["cogLoading"] = false;
        sprite.tint = 0xffffff;
        sprite.scale.set(1, 1);

        let post = sprite["cogPostLoad"];
        if (post)
          post(sprite);
          
        sprite.texture = texturePixi.texture;
        this.cogwheel.map.invalidate(sprite);
        
        if (cb)
          cb(sprite);
      }

      let texturePixi = this.cogwheel.pixi.get(textureCog);
      if (texturePixi)
        resolved(texturePixi);
      else
        this.cogwheel.pixi.load(textureCog).then(resolved);
    } else {
      console.error("[spriteLoading]", "Cannot find texture", textureOrPath);
    }

    return sprite;
  }
  
  /**
   * @param {PIXI.DisplayObject} sprite
   */
  resolveSprites(sprite) {
    if (sprite instanceof PIXI.Container)
      for (let child of sprite.children)
        this.resolveSprites(child);

    /** @type {CogSubtexture} */
    let textureCog = sprite["cogTexture"];
    const width = textureCog ? textureCog.width : 1;
    const height = textureCog ? textureCog.height : 1;

    if (!(sprite instanceof PIXI.Sprite))
      return;

    if (!sprite["cogLoading"]) {
      sprite.anchor.set(
        Math.floor(sprite.anchor.x * width) / width,
        Math.floor(sprite.anchor.y * height) / height
      );
      return;
    }
    
    let tint = sprite.tint;
    let scale = sprite.scale.clone();
    sprite["cogPostLoad"] = () => {
      sprite.tint = tint;
      sprite.scale.set(scale.x, scale.y);
      sprite.anchor.set(
        Math.floor(sprite.anchor.x * width) / width,
        Math.floor(sprite.anchor.y * height) / height
      );
    };

    if (!textureCog) {
      // Missing texture.
      sprite.texture = PIXI.Texture.WHITE;
      sprite.tint = 0xff0000;
      sprite.anchor.set(0.5, 0.5);
      sprite.scale.set(4 / 16, 4 / 16);
      return;
    }

    sprite.texture = PIXI.Texture.WHITE;
    sprite.tint = 0xffff00;
    sprite.scale.set(scale.x * width / 16, scale.y * height / 16);
  }

  /**
   * @param {PIXI.Container} parent
   * @param {PIXI.DisplayObject} child
   * @param {string} [name]
   */
  updateChild(parent, child, name) {
    if (!parent)
      throw new Error("Cannot update the child of a null parent!");

    if (child) {
      if (name)
        child.name = name;
      else
        name = child.name;
    }

    let childPrev = parent.getChildByName(name);
    if (childPrev) {
      if (childPrev === child)
        return;

      let index = parent.getChildIndex(childPrev);
      parent.removeChild(childPrev);
      if (child)
        parent.addChildAt(child, index);
      
    } else if (child) {
      parent.addChild(child);
    }
  }

}
