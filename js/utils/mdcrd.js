//@ts-check
import { rd, rdom, rd$, RDOMListHelper } from "./rdom.js";

/** @type {import("material-components-web")} */
const mdc = window["mdc"]; // mdc
// @ts-ignore
const markdown = new showdown.Converter(); // showdown

/**
 * @typedef {(el: HTMLElement) => HTMLElement} HTMLElementGen
 * @typedef {string | HTMLElementGen} Label
 * @typedef {(string | HTMLElementGen | any[])[]} Items
 */

var mdcrd = {

  _: {
    menus: [],
  },

  /**
   * @param {string} text
   */
  markdown: (text) =>
  /**
   * @param {HTMLElement} el
   */
  el => rd$(el)`<span ${rd.html("text")}=${markdown.makeHtml(text)}></span>`,

  /**
   * @param {string} name
   */
  icon: (name) =>
  /**
   * @param {HTMLElement} el
   */
  el => rd$(el)`<i class="material-icons">${name}</i>`,

  /**
   * @param {string} text
   * @param {string} action
   * @param {(e: Event) => void} callback
   */
  snackbar: (text, action, callback) =>
  /**
   * @param {HTMLElement} el
   */
  el => rd$(el)`
  <div class="mdc-snackbar mdc-snackbar--leading">
    <div class="mdc-snackbar__surface">
      <div class="mdc-snackbar__label" role="status" aria-live="polite">
        ${text}
      </div>
      <div class="mdc-snackbar__actions" rdom-get="list">
        <button type="button" class="mdc-button mdc-snackbar__action" onclick=${callback}>${action}</button>
        <button class="mdc-icon-button mdc-snackbar__dismiss material-icons" title="Dismiss">close</button>
      </div>
    </div>
  </div>
  ${el => el.MDCSnackbar = new mdc.snackbar.MDCSnackbar(el)}`,

  /**
   * @param {string} label
   * @param {string} [placeholder]
   * @param {(value: string) => void} [cb]
   */
  textField: (label, placeholder, cb) =>
  /**
   * @param {HTMLElement} el
   */
  el => {
    el = rd$(el)`
    <div class="mdc-text-field mdc-text-field--outlined">
      <input class="mdc-text-field__input" onchange=${cb}>
      <div class="mdc-notched-outline">
        <div class="mdc-notched-outline__leading"></div>
        <div class="mdc-notched-outline__notch">
          <label class="mdc-floating-label">${label}</label>
        </div>
        <div class="mdc-notched-outline__trailing"></div>
      </div>
    </div>
    ${el => el.MDCTextField = new mdc.textField.MDCTextField(el)}`;

    el["MDCTextField"].value = placeholder || "";

    return el;
  },

  /**
   * @param {Label} label
   * @param {(e: MouseEvent) => void} [cb]
   */
  button: (label, cb) =>
  /**
   * @param {HTMLElement} el
   */
  el => rd$(el)`
  <button
    class="mdc-button"
    onclick=${cb}
  >${label}</button>
  ${el => el.MDCRipple = new mdc.ripple.MDCRipple(el)}`,

  list: {
    /**
     * @param {HTMLElement} el
     */
    divider: el =>
    rd$(el)`<li class="mdc-list-divider" role="separator"></li>`,

    /**
     * @param {Label} label
     * @param {(e: Event) => void} callback
     */
    item: (label, callback, enabled = true) =>
    /**
     * @param {HTMLElement} el
     */
    el => rd$(el)`
    <li class="mdc-list-item"
      onclick=${callback} tabindex=${enabled ? 0 : -1} aria-disabled=${!enabled}
      ${rd.toggleClass("disabled", "mdc-list-item--disabled")}=${!enabled}
    >
      <span class="mdc-list-item__text">${label}</span>
    </li>
    ${el => el.MDCRipple = new mdc.ripple.MDCRipple(el)}`,

    /**
     * @param {Items} items
     */
    list: (...items) =>
    /**
     * @param {HTMLElement} el
     */
    el => {
      el = rd$(el)`
      <ul class="mdc-list"></ul>
      ${el => {
        // @ts-ignore
        el.MDCList = new mdc.list.MDCList(el);
      }}`;

      let list = new RDOMListHelper(el);
      for (let i in items) {
        let item = items[i];
        if (!(item instanceof Function))
          // @ts-ignore
          list.add(i, mdcrd.list.item(...item));
        else
          list.add(i, item);
      }
      list.end();

      return el;
    },
  },

  menu: {
    /**
     * @param {Label} label
     * @param {Items} items
     */
    auto: (label, ...items) =>
    /**
     * @param {HTMLElement} el
     */
    el => rd$(el)`
    <div class="mdc-menu-surface--anchor">
      ${mdcrd.menu.button(label)}
      ${mdcrd.menu.list(...items)}
    </div>`,

    /**
     * @param {HTMLElement | Event} a
     * @param {Event} [b]
     */
    open: function(a, b) {
      let el =
        a instanceof HTMLElement ? a :
        this instanceof HTMLElement ? this :
        null;
      let e =
        a instanceof Event ? a :
        b instanceof Event ? b :
        null;
      
      let menuEl = el["MDCMenu"] ? el : el.parentElement.getElementsByClassName("mdc-menu")[
        Array.prototype.slice.call(
          el.parentElement.getElementsByClassName("mdc-menu-btn")
        ).indexOf(el)
      ];
      let menu = menuEl["MDCMenu"];

      // TODO: Fix tabindex
      /*
      if (e instanceof KeyboardEvent) {
        for (let child of menuEl.querySelectorAll("[tabindex-o]")) {
          if (!(child instanceof HTMLElement))
            continue;
          rd$(rdom.getCtx(child))`tabindex=${child.getAttribute("tabindex-o")}`;
          child.removeAttribute("tabindex-o");
        }
      } else {
        for (let child of menuEl.querySelectorAll("[tabindex]")) {
          if (!(child instanceof HTMLElement))
            continue;
          child.setAttribute("tabindex-o", child.getAttribute("tabindex-o") || child.getAttribute("tabindex"));
          rd$(rdom.getCtx(child))`tabindex=${""}`;
        }
      }
      */
      for (let child of menuEl.querySelectorAll("[tabindex]")) {
        if (!(child instanceof HTMLElement))
          continue;
        rd$(rdom.getCtx(child))`tabindex=${""}`;
      }

      for (let other of mdcrd._.menus)
        other.open = other === menu;
    },

    /**
     * @param {Label} label
     * @param {(e: MouseEvent | KeyboardEvent) => void} [open]
     */
    button: (label, open) =>
    /**
     * @param {HTMLElement} el
     */
    el => rd$(el)`
    <button
      class="mdc-button mdc-menu-btn"
      onmousedown=${function(/** @type {MouseEvent} */ e) {
        if (e.button > 2)
          return;
        if (open)
          return open.apply(this, [e]);
        return mdcrd.menu.open(this, e);
      }}
      onkeypress=${function(/** @type {KeyboardEvent} */ e) {
        if (e.key !== "Enter")
          return;
        if (open)
          return open.apply(this, [e]);
        return mdcrd.menu.open(this, e);
      }}
    >${label}</button>
    ${el => el.MDCRipple = new mdc.ripple.MDCRipple(el)}`,

    /**
     * @param {string} icon
     * @param {Label} label
     * @param {(e: MouseEvent) => void} callback
     */
    item: (icon, label, callback, enabled = true) =>
    /**
     * @param {HTMLElement} el
     */
    el => rd$(el)`
    <li class="mdc-list-item" role="menuitem"
      onclick=${callback} tabindex=${enabled ? 0 : -1} aria-disabled=${!enabled}
      ${rd.toggleClass("disabled", "mdc-list-item--disabled")}=${!enabled}
    >
      <span class="mdc-list-item__text">${mdcrd.icon(icon)}${label}</span>
    </li>
    ${el => el.MDCRipple = new mdc.ripple.MDCRipple(el)}`,

    /**
     * @param {Items} items
     */
    list: (...items) =>
    /**
     * @param {HTMLElement} el
     */
    el => {
      el = rd$(el)`
      <div class="mdc-menu mdc-menu-surface" tabindex="-1">
        <ul class="mdc-menu__items mdc-list" role="menu" rdom-get="list"></ul>
      </div>
      ${el => mdcrd._.menus.push(el.MDCMenu = new mdc.menu.MDCMenu(el))}`;

      el["MDCMenu"].quickOpen = false;

      let list = new RDOMListHelper(rdom.get(el, "list"));
      for (let i in items) {
        let item = items[i];
        if (!(item instanceof Function))
          // @ts-ignore
          list.add(i, mdcrd.menu.item(...item));
        else
          list.add(i, item);
      }
      list.end();

      return el;
    },
  },

  /**
   * @param {Items} left
   * @param {Items} right
   */
  topAppBar: (left, right) =>
  /**
   * @param {HTMLElement} el
   */
  el => {
    el = rd$(el)`
    <header class="mdc-top-app-bar mdc-top-app-bar--dense">
      <div class="mdc-top-app-bar__row">
        <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-start" rdom-get="left">
        </section>

        <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-end" rdom-get="right">
        </section>
      </div>
    </header>
    ${el => el.MDCTopAppBar = new mdc.topAppBar.MDCTopAppBar(el)}`;

    let list;
    list = new RDOMListHelper(rdom.get(el, "left"));
    for (let i in left)
      list.add(i, left[i]);
    list.end();
    list = new RDOMListHelper(rdom.get(el, "right"));
    for (let i in right)
      list.add(i, right[i]);
    list.end();

    return el;
  },

  /** @param {{ title: string; body: HTMLElementGen; defaultButton: string; buttons: string[] }} _ */
  dialog: ({
    title,
    body,
    defaultButton,
    buttons
  }) =>
  /**
   * @param {HTMLElement} el
   */
  el => {
    el = rd$(el)`
    <div class="mdc-dialog" role="alertdialog" aria-modal="true" aria-labelledby="my-dialog-title" aria-describedby="my-dialog-content">
      <div class="mdc-dialog__container">
        <div class="mdc-dialog__surface">
        <h2 class="mdc-dialog__title" id="my-dialog-title">${title}</h2>
        <div class="mdc-dialog__content" id="my-dialog-content">${body}</div>
        <footer class="mdc-dialog__actions" rdom-get="buttons">
        </footer>
        </div>
      </div>
      <div class="mdc-dialog__scrim"></div>
    </div>
    ${el => el.MDCDialog = new mdc.dialog.MDCDialog(el)}`;

    let list;
    list = new RDOMListHelper(rdom.get(el, "buttons"));
    for (let i in buttons)
      list.add(i, el => rd$(el)`<button type="button" class="mdc-button mdc-dialog__button" data-mdc-dialog-action=${i} ${rd.toggleClass(`default-${i}`, "mdc-dialog__button--default")}=${defaultButton === i}>${buttons[i]}</button>`);
    list.end();

    return el;
  }

};

export default window["mdcrd"] = mdcrd;
