//@ts-check

/**
 * @typedef {number | number[] | { x: number; y: number; }} AVecInputX
 * @typedef {number[]} AVecInputY
 */

const AVec = (() => {

	/**
	 * @param {number} length
	 * @param {number} fallback
	 * @param {AVecInputX} arrayOrX
	 * @param {AVecInputY} yAndRest
	 * @returns {number[]}
	 */
	function toArray(length, fallback, arrayOrX, yAndRest) {
		let array;

		if (arrayOrX instanceof Array) {
			array = arrayOrX;

		} else if (typeof arrayOrX === "object" && "x" in arrayOrX) {
			array = [arrayOrX.x, arrayOrX.y];
			
		} else {
			array = [arrayOrX, ...yAndRest];
		}

		if (length) {
			if (array.length > length)
				array = array.slice(0, length);
			while (array.length < length)
				array.push(fallback);
		}

		return array;
	}

	/**
	 * @extends {Array<number>}
	 */
	class AVec extends Array {
		/**
		 * @param {AVecInputX} arrayOrX
		 * @param {AVecInputY} yAndRest
		 */
		constructor(arrayOrX, ...yAndRest) {
			super();
			// @ts-ignore
			return toArray(0, 0, arrayOrX, yAndRest);
		}
		
		// Dummy functions for VS Code.
		*[Symbol.iterator]() {
			yield 0;
		}
		get x() {
			return this[0];
		}
		set x(value) {
			this[0] = value;
		}
		get y() {
			return this[1];
		}
		set y(value) {
			this[1] = value;
		}
		/**
		 * @param {any[]} items
		 * @returns {AVec}
		 */
		concat(...items) {
			// @ts-ignore
			return super.concat(...items);
		}
		/**
		 * @returns {AVec}
		 */
		reverse() {
			// @ts-ignore
			return super.reverse();
		}
		/**
		 * @param {number} [start]
		 * @param {number} [end]
		 * @returns {AVec}
		 */
		slice(start, end) {
			// @ts-ignore
			return super.slice(start, end);
		}
		/**
		 * @param {(a: number, b: number) => number} [compareFn]
		 * @returns {this}
		 */
		sort(compareFn) {
			return super.sort(compareFn);
		}
		/**
		 * @param {number} start
		 * @param {number} deleteCount
		 * @param {number[]} items
		 * @returns {AVec}
		 */
		splice(start, deleteCount, ...items) {
			// @ts-ignore
			return super.splice(start, deleteCount, ...items);
		}

		// Proper functions.
		/**
		 * @param {(value: number, index: number, array: number[]) => number} callbackfn
		 * @returns {AVec}
		 */
		vmap(callbackfn) {
			// @ts-ignore
			return super.map(callbackfn);
		}

		/**
		 * @param {(value: number, index: number, array: number[]) => value is number} callbackfn
		 * @returns {AVec}
		 */
		vfilter(callbackfn) {
			// @ts-ignore
			return super.filter(callbackfn);
		}

		/**
		 * @param {AVecInputX} arrayOrX
		 * @param {AVecInputY} yAndRest
		 */
		vadd(arrayOrX, ...yAndRest) {
			/** @type {AVec} */
			// @ts-ignore
			let a = [...this];
			if (typeof(arrayOrX) === "number" && yAndRest.length === 0) {
				for (let i in a)
					a[i] += arrayOrX;
			} else {
				let b = toArray(this.length, 0, arrayOrX, yAndRest);
				for (let i in a)
					a[i] += b[i];
			}
			return a;
		}

		/**
		 * @param {AVecInputX} arrayOrX
		 * @param {AVecInputY} yAndRest
		 */
		vsub(arrayOrX, ...yAndRest) {
			/** @type {AVec} */
			// @ts-ignore
			let a = [...this];
			if (typeof(arrayOrX) === "number" && yAndRest.length === 0) {
				for (let i in a)
					a[i] -= arrayOrX;
			} else {
				let b = toArray(this.length, 0, arrayOrX, yAndRest);
				for (let i in a)
					a[i] -= b[i];
			}
			return a;
		};

		/**
		 * @param {AVecInputX} arrayOrX
		 * @param {AVecInputY} yAndRest
		 */
		vdiv(arrayOrX, ...yAndRest) {
			/** @type {AVec} */
			// @ts-ignore
			let a = [...this];
			if (typeof(arrayOrX) === "number" && yAndRest.length === 0) {
				for (let i in a)
					a[i] /= arrayOrX;
			} else {
				let b = toArray(this.length, 1, arrayOrX, yAndRest);
				for (let i in a)
					a[i] /= b[i];
			}
			return a;
		};

		/**
		 * @param {AVecInputX} arrayOrX
		 * @param {AVecInputY} yAndRest
		 */
		vmul(arrayOrX, ...yAndRest) {
			/** @type {AVec} */
			// @ts-ignore
			let a = [...this];
			if (typeof(arrayOrX) === "number" && yAndRest.length === 0) {
				for (let i in a)
					a[i] *= arrayOrX;
			} else {
				let b = toArray(this.length, 1, arrayOrX, yAndRest);
				for (let i in a)
					a[i] *= b[i];
			}
			return a;
		};

		/**
		 * @param {AVecInputX} arrayOrX
		 * @param {AVecInputY} yAndRest
		 */
		vset(arrayOrX, ...yAndRest) {
			/** @type {AVec} */
			// @ts-ignore
			let a = [...this];
			if (typeof(arrayOrX) === "number" && yAndRest.length === 0) {
				for (let i in a)
					a[i] = arrayOrX;
			} else {
				let b = toArray(this.length, undefined, arrayOrX, yAndRest);
				for (let i in a)
					a[i] = b[i];
			}
			return a;
		}

		/**
		 * @param {number[] | { x: number; y: number; }} target
		 */
		vcopyTo(target) {
			/** @type {AVec} */
			// @ts-ignore
			let a = [...this];
			if (target instanceof Array) {
				for (let i in a)
					target[i] = a[i];
			} else {
				target.x = a[0];
				target.y = a[1];
			}
			return a;
		}
	}

	// Copy all new functions from AVec to Array.
	for (let id of Reflect.ownKeys(AVec.prototype)) {
		if (id in Array.prototype)
			continue;
		if (typeof id !== "string" || !id.startsWith("v"))
			continue;
		Object.defineProperty(Array.prototype, id, {
			enumerable: false,
			get: function() { return AVec.prototype[id].bind(this); }
		});
	}

	// Define custom properties for x and y. Kill me later.
	Object.defineProperty(Array.prototype, "x", {
		enumerable: false,
		get: function() { return this[0]; },
		set: function(value) { this[0] = value; }
	});
	Object.defineProperty(Array.prototype, "y", {
		enumerable: false,
		get: function() { return this[1]; },
		set: function(value) { this[1] = value; }
	});

	return AVec;
})();

const $$ = (() => {
	const REGEX_NEWLINE = /\n/g;
	/**
	 * @param {TemplateStringsArray} template
	 * @param {(number | AVec | string)[]} values
	 */
	function avec$(template, ...values) {
		/** @type {(string | string[])[]} */
		let pieces = [];
		
		let merged = template.reduce(function avec$reduce(prev, next, i) {
			let val = values[i - 1];
			if (val instanceof Array) {
				return prev + " [ " + val.join(", ") + " ] " + next;
			} else {
				return prev + " " + val + " " + next;
			}
		});
		
		let close = -1;
		let open = -1;
		let prev = -1;
		let next = -1;
		let dimsMax = 0;
		while (true) {
			open = merged.indexOf("[", open + 1);
			if (open === -1) {
				pieces.push(merged.slice(close + 1));
				break;
			}

			if (open !== 0)
				pieces.push(merged.slice(close + 1, open));

			close = merged.indexOf("]", open + 1);

			/** @type {string[]} */
			prev = open;
			next = merged.indexOf(",", open + 1);
			if (next === -1 || next >= close) {
				continue;
			}

			let values = [];
			let dims = 0;
			while (true) {
				dims++;

				if (next === -1 || next >= close)
					next = close;
				values.push(merged.slice(prev + 1, next).trim());
				if (next === close)
					break;
				prev = next;
				next = merged.indexOf(",", next + 1);
			}

			if (dims > dimsMax)
				dimsMax = dims;
			
			pieces.push(values);
		}

		/** @type {string[]} */
		let exprs = new Array(dimsMax);
		for (let d = 0; d < dimsMax; d++)
			exprs[d] = "";

		for (let piece of pieces) {
			if (typeof piece === "string") {
				for (let d in exprs)
					exprs[d] += piece;
			} else {
				for (let d in exprs)
					exprs[d] += piece[d];
			}
		}
		
		/** @type {AVec} */
		// @ts-ignore
		let r = exprs.map(expr => math.evaluate(expr.replace(REGEX_NEWLINE, " ")));
		return r;
	};

	return avec$;
})();

const $$$ = (() => {
	const REGEX_ESCAPE = /[.*+?^${}()|[\]\\]/;
	function escapeRegExp(s) {
		return s.replace(REGEX_ESCAPE, "\\$&");
	}

	/**
	 * @param {{ [x: string]: number | AVec }} map
	 * @returns {(template: TemplateStringsArray, ...values: (number | AVec | string)[]) => AVec}
	 */
	function dynavec$(map) {
		return (template, values) => {
			let merged = template.reduce(function avec$reduce(prev, next, i) {
				let val = values[i - 1];
				if (val instanceof Array) {
					return prev + " [ " + val.join(", ") + " ] " + next;
				} else {
					return prev + " " + val + " " + next;
				}
			});

			for (let key in map) {
				/** @type {any} */
				let val = map[key];
				if (val instanceof Array)
					val = " [ " + val.join(", ") + " ] ";
				let regex = new RegExp(escapeRegExp(key), "g");
				merged = merged.replace(regex, val.toString());
			}

			return $$`${merged}`;
		};
	}
	return dynavec$;
})();
