//@ts-check
export default {

  COLOR_BG: 0x111111,
  COLOR_LEVELBG: 0x333333,
  COLOR_FILLER: 0x444444,
  
  COLORS_FGTILES: [
    0xffffff,
    0xf6735e,
    0x85f65e,
    0x37d7e3,
    0x376be3,
    0xc337e3,
    0xe33773
  ],
  COLORS_LEVELBG: [
    0x323232,
    0x3e3333,
    0x333e33,
    0x333639,
    0x333340,
    0x3c333f,
    0x393333
  ],

  VIEWPORT_ZOOM_BORDER: 128,
  VIEWPORT_LOAD_BORDER: 128,
  
  SESSION_DATA_DEFAULT: {
    map: "",
    level: "",
    tool: "place",
    place_solids: "1",
    place_bg: "1",
    place_entitites: "player:0",
    layer: "solids",
    pickerIsList: true,
  },

};
