//@ts-check
(async function() {
  console.log("[boot]", "Waiting until DOM ready.");
  await lazyman.load("DOM");
  console.log("[boot]", "DOM ready.");

  document.body.addEventListener("mousedown", e => {
    if (e.button === 1)
      e.preventDefault();
  });
  document.addEventListener("contextmenu", e => {
    e.preventDefault();
  }, false);

  let splash = document.getElementById("splash");
  let splashProgressBar = document.getElementById("splash-progress-bar");

  let loaded = new Set();
  let failed = new Set();

  /** @type {(string | string[])[]} */
  let depsBase = [
    "cogwheel.css",
  ];

  if (window["nodeRequire"]) {
    depsBase = [...depsBase,
      // https://material.io/develop/web/docs/getting-started/
      ["material-icons.css", "npm*material-design-icons/iconfont/material-icons.css"],
      ["Roboto.css", "npm*typeface-roboto/index.css"],
      "npm*material-components-web/dist/material-components-web.min.css",
      "npm*material-components-web/dist/material-components-web.min.js",

      // https://github.com/pixijs/pixi.js
      "npm*pixi.js/dist/pixi.min.js",

      // https://code.jquery.com/
      "npm*jquery/dist/jquery.min.js",

      // https://github.com/showdownjs/showdown
      "npm*showdown/dist/showdown.min.js",

      // https://github.com/josdejong/mathjs
      "npm*mathjs/dist/math.min.js",
    ];
  } else {
    depsBase = [...depsBase,
      // https://material.io/develop/web/docs/getting-started/
      ["material-icons.css", "https://fonts.googleapis.com/icon?family=Material+Icons"],
      ["Roboto.css", "https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700"],
      "https://unpkg.com/material-components-web@0.44.0/dist/material-components-web.min.css",
      "https://unpkg.com/material-components-web@0.44.0/dist/material-components-web.min.js",

      // https://github.com/pixijs/pixi.js
      "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.8.4/pixi.min.js",

      // https://code.jquery.com/
      "https://code.jquery.com/jquery-3.3.1.min.js",

      // https://github.com/showdownjs/showdown
      "https://cdnjs.cloudflare.com/ajax/libs/showdown/1.9.0/showdown.min.js",

      // https://github.com/josdejong/mathjs
      "https://unpkg.com/mathjs@5.4.2/dist/math.min.js",
    ];
  }

  depsBase = [...depsBase,
    // https://github.com/davidfig/pixi-viewport
    // 4.0.0-beta.2
    "deps/pixi-viewport.js",

    // https://github.com/pixijs/pixi-display
    // [master] 6bd9758ce9aa8739104d3b291594fe75d245bee1
    "deps/pixi-layers.js",

    // https://github.com/s-yadav/contextMenu.js
    // 8d9cbfe
    "deps/contextMenu.min.css",
    "deps/contextMenu.min.js",

    // https://github.com/ccampbell/mousetrap
    // 1.6.2
    "deps/mousetrap.js",

    // https://clusterize.js.org/
    // [master*] 0211bafaeb82d781cea320af1c2f04e36016c3d3
    // The above commit holds the last MIT licensed version.
    // Clusterize itself has also been modified to fit Cogwheel's needs.
    "deps/clusterize.css",
    "deps/clusterize.js",

    // https://jets.js.org/
    // [master] 1770f7f7f3d651095336beb4a679c60cca22ffb4
    "deps/jets.js",

    "utils/avec.js",
  ];

  /** @type {(string | string[])[]} */
  let core = [
    // Cogwheel itself.
    ["cogwheel.mjs", "cogwheel.js"]
  ];
  let depsLength = depsBase.length + core.length;

  // Update the splash screen's progress bar.
  let updateProgress = (id, success) => {
    console.log("[boot]", id, success ? "loaded." : "failed loading!");
    (success ? loaded : failed).add(id);
    if (!success) {
      splash.classList.add("failed");
      splashProgressBar.style.transform = `scaleX(1)`;
      return;
    }
    if (failed.size !== 0)
      return;
    splashProgressBar.style.transform = `scaleX(${0.25 * (loaded.size / depsLength)})`;
  }

  // Wrapper around lazyman.all which runs updateProgress.
  let load = (deps, ordered) => lazyman.all(
    deps, ordered,
    id => updateProgress(id, true),
    id => updateProgress(id, false),
  );

  console.log("[boot]", "Loading Cogwheel and all dependencies.");
  await load(depsBase, true);
  console.log("[boot]", "Base dependencies loaded.");

  await load(core, true);
  console.log("[boot]", "Cogwheel prepared.");

  await window["cogwheel"].start();
  console.log("[boot]", "Cogwheel loaded.");

  splash.classList.add("hidden");
  document.getElementsByTagName("app")[0].classList.add("ready");
  setTimeout(() => {
    splash.remove();
  }, 2000);

})().catch(e => {
  document.getElementById("splash").classList.add("failed");
  if (window["nodeRequire"])
    window["nodeRequire"]("electron").remote.getCurrentWindow().webContents.openDevTools();
  throw e;
});
