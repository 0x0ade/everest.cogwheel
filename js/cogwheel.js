//@ts-check
import mdcrd from "./utils/mdcrd.js";
/** @type {import("material-components-web")} */
const mdc = window["mdc"];
import CONSTS from "./consts.js";
import { CogContent, CogAsset } from "./content/core/content.js";
import { CogDataWebSrc, CogDataFSSrc, CogDataSrc, REGEX_BACKSLASH } from "./content/core.js";
import { CogAtlas } from "./content/atlas.js";
import { CogAutotiler } from "./content/autotiler.js";
import { CogTexture, CogSubtexture } from "./content/texture.js";
import { CogMap } from "./content/map.js";
import { CogwheelPIXI } from "./components/pixi.js";
import { CogwheelDOM } from "./components/dom.js";
import { CogwheelMap } from "./components/map.js";
import { CogwheelUtils } from "./components/utils.js";
import { CogwheelHistory } from "./components/history.js";
import { CogwheelDialog } from "./components/dialog.js";
/** @type {NodeRequire} */
const nodeRequire = window["nodeRequire"] || window["require"];

class CogwheelSession {
  /**
   * @param {Cogwheel} cogwheel
   */
  constructor(cogwheel) {
    this.cogwheel = cogwheel;

    this.id = ++this.cogwheel.gid;

    /** @type {string} */
    this.map;
    /** @type {string} */
    this.level;
    /** @type {string} */
    this.tool;
    /** @type {string} */
    this.place;
    /** @type {string} */
    this.layer;
    /** @type {boolean} */
    this.pickerIsList;

    for (let id in CONSTS.SESSION_DATA_DEFAULT) {
      Object.defineProperty(this, id, {
        configurable: true,
        enumerable: true,

        get: () => this.data[id],
        set: v => this.data[id] = v
      });
    }

    // Fool VS Code into thinking that this.data shares its type with dataDefault.
    this.data = CONSTS.SESSION_DATA_DEFAULT;
    this["data"] = Object.assign({}, CONSTS.SESSION_DATA_DEFAULT);

  }

  get _layerPlace() {
    let layer = this.layer || "";
    if (layer.endsWith("decals"))
      return "decals";
    return layer;
  }

  /**
   * @returns {string}
   */
  get place() {
    return this["place_" + this._layerPlace];
  }
  set place(value) {
    this["place_" + this._layerPlace] = value;
  }

  load() {
    try {
      this["data"] = JSON.parse(JSON.stringify(this.cogwheel.nconf.get("cog:session")));
    } catch (e) {
      console.error("Could not load session", e);
    }

    if (this.data && this.data.constructor === Object) {
      for (let key in CONSTS.SESSION_DATA_DEFAULT)
        if (!this["data"][key] && this["data"][key] != false)
          this["data"][key] = CONSTS.SESSION_DATA_DEFAULT[key];
    } else {
      this["data"] = Object.assign({}, CONSTS.SESSION_DATA_DEFAULT);
    }
  }

  save() {
    this.cogwheel.nconf.set("cog:session", this.data);
    this.cogwheel.nconf.save("config.json");
  }

  async init() {
    // Load the map.
    /** @type {CogMap} */
    let map;
    try {
      this.cogwheel.content.assets.delete("Map");
      if (this._mapSrc)
        this.cogwheel.content.sources.splice(this.cogwheel.content.sources.indexOf(this._mapSrc), 1);
      
      let mapKey = `__INTERNAL:SESSION:MAP:${this.id}`
      this._mapSrc = new CogDataFSSrc(this.cogwheel.fs, path => {
        if (path === mapKey)
          return this.map;
        return null;
      });
      this.cogwheel.content.sources.push(this._mapSrc);
      
      map = await this.cogwheel.load(CogMap, "Map", mapKey);
    } catch (e) {
      console.error("Could not load map", this.map, e);
      // Fallback to chapter 1 provided by Celeste.
      this.cogwheel.content.assets.delete("Map");
      map = await this.cogwheel.load(CogMap, "Map", "Maps/1-ForsakenCity.bin");
    }

    this.cogwheel.mapdata = map;

    // Reload the tilemaps and other map-related assets.
    try {
      await this.cogwheel.loadAssets(true);
    } catch (e) {
      console.error("Could not load custom assets for", this.map, e);
      await this.cogwheel.loadAssets();
    }

    // Re-render everything after initializing a new session.
    this.cogwheel.map.resetMap();
    this.cogwheel.render();

    this.cogwheel.map.focusLevel(map.get(this.level), true, true);

    if (this.tool === "__base")
      this.tool = "";
    if (!this.cogwheel.map.tools.has(this.tool)) {
      for (let id of this.cogwheel.map.tools.keys()) {
        if (id === "__base")
          continue;
        this.tool = id;
        break;
      }
    }
    this.cogwheel.map.currentTool = this.cogwheel.map.tools.get(this.tool);
  }

}

export class Cogwheel {
  constructor() {
    /** @type {Map<string, HTMLElement>} */
    this.alerts = new Map();
    this.ready = false;
    this.reloading = 0;

    /** @type {CogDataSrc[]} */
    this._modSrc = [];

    /** @type {CogMap} */
    this.mapdata;

    this.gid = 0;
  }

  /** @returns {string} */
  get updaterStatus() {
    return this._updaterStatus;
  }
  set updaterStatus(value) {
    this._updaterStatus = value;
    if (this.dom && this.dom.renderUpdateButton)
      this.dom.renderUpdateButton();
  }

  async start() {
    // Init mdc early.
    mdc.autoInit();

    // Check if we're running in Electron or similar.
    // Right now only Electron and compatible envs are supported.
    this.electron = nodeRequire ? nodeRequire("electron") : null;
    if (!this.electron) {
      this.alert({
        text:
`Sorry, Cogwheel 2.0 doesn't work online!  
Please [install the offline version](https://github.com/EverestAPI/cogwheel/releases) instead.`,
        buttons: [],
        dismissable: false
      });
      throw new Error("Not in Electron");
    }

    // Make cog:// xhr- and fetch-ready
    if ("registerURLSchemeAsPrivileged" in this.electron.webFrame)
      // @ts-ignore Electron 4.X
      this.electron.webFrame.registerURLSchemeAsPrivileged("cog", { bypassCSP: false });

    this.package = await fetch("./package.json").then(r => r.json());

    // Load any further modules.
    /** @type {import("path")} */
    this.path = nodeRequire("path");
    /** @type {import("fs")} */
    this.fs = nodeRequire("fs");
    /** @type {import("nconf")} */
    this.nconf = this.electron.remote.require("nconf");
    this.approot = this.electron.remote.require("process").env["COGWHEEL_APPROOT"];
    this.packaged = this.electron.remote.app.isPackaged;

    /** @type {import("../interop.js")} */
    this.interop = nodeRequire(this.approot + "/interop.js");

    // Set up any IPC magic.
    this.electron.ipcRenderer.on("celesteSelectCB", (e, paths) => {
      this.dialog._celesteSelectCB(paths);
    });
    this.electron.ipcRenderer.on("showFileOpenDialogCB", (e, paths) => {
      this.dialog._showFileOpenDialogCB(paths);
    });
    this.electron.ipcRenderer.on("showFileSaveDialogCB", (e, paths) => {
      this.dialog._showFileSaveDialogCB(paths);
    });

    // Set up pixi, which is required early to feed content into it.
    this.pixi = new CogwheelPIXI(this);
    await this.pixi.start();

    // Set up any content magic.
    this.content = new CogContent(
      new CogDataWebSrc(path => `cog://celeste/Content/${path}`),
    );

    // Who would've known that the dialog component is needed early...
    this.dialog = new CogwheelDialog(this);
    await this.dialog.start();

    // Load the Celeste assets.
    let loaded = false;
    do {
      try {
        await this.loadCoreAssets();
        loaded = true;
      } catch (e) {
        console.error(e);
        await this.dialog.selectCeleste();
      }
    } while (!loaded);
    this.reloading = 0;

    // Load the session.
    this.session = new CogwheelSession(this);
    this.session.load();

    // Set up all remaining components.
    this.utils = new CogwheelUtils(this);

    this.dom = new CogwheelDOM(this);
    await this.dom.start();

    // TODO: Move updater into its own component.
    this.updaterStatus = this.packaged ? "idle" : "disabled";
    this.electron.ipcRenderer.on("updaterStatus", (e, status, autoDownload) => {
      this.updaterStatus = status;
      switch (status) {
        case "checking":
          this.snackbar({ text: "Checking for updates..." });
          break;
        
        case "error":
          this.snackbar({ text: "The updater has encountered an error." });
          this.updaterStatus = "idle";
          break;

        case "unavailable":
          this.snackbar({ text: "Cogwheel is up to date." });
          this.updaterStatus = "idle";
          break;

        case "available":
          if (autoDownload) {
            this.snackbar({ text: "Cogwheel is now downloading an update." });
            this.updaterStatus = "downloading";
          } else {
            this.snackbar({ text: "An update is ready for download.", action: "GET" })["then"](action => {
              if (action)
                this.updaterDownload();
            });
          }
          break;

        case "downloading":
          this.snackbar({ text: "Cogwheel is now downloading an update." });
          break;

        case "downloaded":
          this.snackbar({ text: "The update is ready. It will be installed on the next start.", action: "RESTART NOW" })["then"](action => {
            if (action)
              this.electron.ipcRenderer.send("updaterRestart");
          });
          break;
      }
    });

    this.updaterCheck(false);

    this.map = new CogwheelMap(this);
    await this.map.start();

    this.history = new CogwheelHistory(this);
    await this.history.start();

    // Load all mods.
    await this.loadMods();

    // Init the session, which also re-renders everything once.
    await this.session.init();

    document.getElementById("splash-progress-bar").style.transform = "scaleX(1)";

    // Set up any keyboard shortcuts.
    Mousetrap.bind("ctrl+o", () => this.dialog.openMap());
    Mousetrap.bind("ctrl+s", () => this.saveMap());
    Mousetrap.bind("ctrl+shift+s", () => this.dialog.saveMap());
    Mousetrap.bind("f12", () => this.electron.remote.getCurrentWindow().webContents.openDevTools());
    Mousetrap.bind("ctrl+z", () => this.history.undo());
    Mousetrap.bind("ctrl+shift+z", () => this.history.redo());
    Mousetrap.bind("ctrl+y", () => this.history.redo());

    // This shit is cursed.
    let cursed = 0;
    this.dom.setContextMenu(this.pixi.app.view, [
      {
        name: "Curse",
        title: "",
        fun: () => {
          this.snackbar({ text: `You've cursed Cogwheel ${++cursed} time${cursed === 1 ? "" : "s"}.` });
        }
      },
    ]);

    setTimeout(() => {
      this.ready = true;
    }, 1200);
  }

  async loadCoreAssets() {
    this.reloading++;
    
    const splashProgressBar = document.getElementById("splash-progress-bar");
    let done = 0;
    const needed = 3;
    /**
     * @template T
     * @param {Promise<T>} p
     * @returns {Promise<T>}
     */
    const progress = (p) => {
      p.then(() => {
        done++;
        if (splashProgressBar)
          splashProgressBar.style.transform = `scaleX(${0.25 + (done / needed) * 0.5})`;
      });
      return p;
    }

    await Promise.all([
      (async () => {
        // Alias some content so that plugins can make use of type safety features.
        this.atlasGameplay = await progress(this.load(CogAtlas, "Gameplay", "Graphics/Atlases/Gameplay", "Packer"));
        this.content.assets.set("Content", this.atlasContent = new CogAtlas(null, "@content"));

        await Promise.all([
          this.pixi.feed(this.fgtiles = await progress(this.load(CogAutotiler, "FGTiles", "Graphics/ForegroundTiles.xml"))),
          this.pixi.feed(this.bgtiles = await progress(this.load(CogAutotiler, "BGTiles", "Graphics/BackgroundTiles.xml")))
        ]);
      })(),

      (async () => {

      })()
    ]);

    this.reloading--;
  }

  async loadAssets(fromMap = false) {
    this.reloading++;
    
    this.content.assets.delete("FGTiles");
    this.content.assets.delete("BGTiles");
    
    let pathFGTiles = "Graphics/ForegroundTiles.xml";
    let pathBGTiles = "Graphics/BackgroundTiles.xml";

    if (fromMap) {
      let map = this.content.get(CogMap, "Map");
      let meta = map._bin.get("meta");
      if (meta) {
        pathFGTiles = meta.getAttr("ForegroundTiles") || pathFGTiles;
        pathBGTiles = meta.getAttr("BackgroundTiles") || pathBGTiles;
      }
    }

    await Promise.all([
      (async () => {
        // Alias some content so that plugins can make use of type safety features.
        await Promise.all([
          this.pixi.feed(this.fgtiles = await this.load(CogAutotiler, "FGTiles", pathFGTiles)),
          this.pixi.feed(this.bgtiles = await this.load(CogAutotiler, "BGTiles", pathBGTiles))
        ]);
      })(),

      (async () => {

      })()
    ]);
    
    this.reloading--;
  }

  async loadMods() {
    this.reloading++;
    this.map.resetPlugins();

    const { fs, path: pathUtils } = this;

    const root = this.path.join(this.nconf.get("celeste:dir"), "Mods");

    /**
     * @param {string} path
     * @returns {Promise<boolean>}
     */
    const exists = (path) => new Promise((resolve) => fs.exists(path, (exists) => resolve(exists)));
    /**
     * @param {string} path
     * @returns {Promise<import("fs").Stats>}
     */
    const lstat = (path) => new Promise((resolve, reject) => fs.lstat(path, (err, stats) => err ? reject(err) : resolve(stats)));
    /**
     * @param {string} path
     * @returns {Promise<string[]>}
     */
    const readdir = (path) => new Promise((resolve, reject) => fs.readdir(path, (err, files) => err ? reject(err) : resolve(files.map(child => pathUtils.join(path, child)))));

    // Clean up any previously loaded mod sources.
    const prevModSrc = this._modSrc;
    this._modSrc = [];
    for (let src of prevModSrc)
      this.content.sources.splice(this.content.sources.indexOf(src), 1);

    // Add sources and load all new assets.

    let gameplay = this.get(CogAtlas, "Gameplay");
    let content = this.get(CogAtlas, "Content");

    /**
     * @param {string} modroot
     * @param {string} dir
     */
    const crawl = async (modroot, dir) => {
      await Promise.all((await readdir(dir)).map(async child => {
        const stat = await lstat(child);
        if (stat.isDirectory()) {
          await crawl(modroot, child);
          return;
        }

        let name = child.slice(modroot.length + 1).replace(REGEX_BACKSLASH, "/");
        if (name.endsWith(".png")) {
          if (name.startsWith("Graphics/Atlases/Gameplay/")) {
            const tex = await this.content.load(CogTexture, null, name);
            name = name.slice("Graphics/Atlases/Gameplay/".length, -4);
            const subtex = new CogSubtexture(tex, name);
            gameplay.set(name, subtex);
            return;
          }

          if (name.startsWith("cogwheel/content/")) {
            const tex = await this.content.load(CogTexture, null, name);
            name = name.slice("cogwheel/content/".length, -4);
            const subtex = new CogSubtexture(tex, name);
            content.set(name, subtex);
            return;
          }
        }
      }));
    }

    /**
     * @param {string} modroot
     */
    const loadModDir = async (modroot) => {
      const src = new CogDataFSSrc(fs, (modroot => path => pathUtils.join(modroot, path))(modroot));
      this._modSrc.push(src);
      this.content.sources.push(src);
      await crawl(modroot, modroot);

      modroot = pathUtils.join(modroot, "cogwheel");
      if (!await exists(modroot))
        return;

      const mainpath = pathUtils.join(modroot, "main.js");
      if (await exists(mainpath)) {
        const module = await import(this.interop.encodeFSPathToURL(mainpath));
        if (module.init) {
          const rv = module.init(this);
          if (rv instanceof Promise)
            await rv;
        }
      }

      /**
       * @param {Map<string, any>} target
       * @param {string} root
       */
      const loadPlugins = async (target, root) => {
        if (!(await exists(root)))
          return;
        let base = target.get("__base");
        await Promise.all((await readdir(root)).map(async path => {
          const module = await import(this.interop.encodeFSPathToURL(path));
          for (let id in module) {
            let plugin = module[id];
            if (id === "default")
              id = pathUtils.basename(path).slice(0, -3);
            
            if (plugin.prototype)
              plugin = new plugin(this);
            
            if (plugin.id)
              id = plugin.id;
            else
              plugin.id = id;
            
            if (!plugin.name)
              plugin.name = this.utils.humanify(id);

            if (base) {
              for (let key of Reflect.ownKeys(base))
                if (!(key in plugin)) {
                  let value = base[key];
                  if (value instanceof Function)
                    value = value.bind(plugin);
                  plugin[key] = value;
                }
            }
            
            plugin.cogwheel = this;
            target.set(id, plugin);

            if (plugin.load) {
              const rv = plugin.load(this);
              if (rv instanceof Promise)
                await rv;
            }
          }
        }));
      }

      await Promise.all([
        loadPlugins(this.map.entityTypes, pathUtils.join(modroot, "entities")),
        loadPlugins(this.map.tools, pathUtils.join(modroot, "tools")),
      ]);
    }

    await loadModDir(pathUtils.join(this.approot, "basemod"));

    if (!(await exists(root))) {
      this.reloading--;
      return;
    }

    await Promise.all((await readdir(root)).map(async modroot => {
      const stat = await lstat(modroot);
      if (!stat.isDirectory()) {
        if (!modroot.endsWith(".zip"))
          return;
        // TODO: Load data from .zips
        console.warn("[loadMods]", "Cannot load .zip", modroot);
        return;

      } else {
        await loadModDir(modroot);
      }
    }));

    this.reloading--;
  }

  render() {
    this.map.renderAll();
    this.dom.render();
  }

  alert({
    title = "",
    text = "",
    defaultButton = "yes",
    buttons = ["OK"],
    dismissable = true
  }) {
    let key = [title, text, ...buttons].join("####");

    let el = this.alerts.get(key);
    el = mdcrd.dialog({ title, body: mdcrd.markdown(text), defaultButton, buttons })(el);
    this.alerts.set(key, el);
    document.body.appendChild(el);

    
    /** @type {import("@material/dialog").MDCDialog} */
    let dialog = el["MDCDialog"];

    if (!dismissable) {
      // @ts-ignore Outdated .d.ts
      dialog.escapeKeyAction = "";
      // @ts-ignore Outdated .d.ts
      dialog.scrimClickAction = "";
    } else {
      // @ts-ignore Outdated .d.ts
      dialog.escapeKeyAction = "close";
      // @ts-ignore Outdated .d.ts
      dialog.scrimClickAction = "close";
    }

    // @ts-ignore Outdated .d.ts
    dialog.open();

    let promise = new Promise(resolve => el.addEventListener("MDCDialog:closed", e => resolve(e["detail"].action)));
    dialog["then"] = promise.then.bind(promise);
    dialog["catch"] = promise.catch.bind(promise);

    return dialog;
  }

  snackbar({
    text = "",
    action = ""
  }) {
    if (this.snackbarLast) {
      // @ts-ignore Outdated .d.ts
      if (this.snackbarLast.isOpen && this.snackbarLastText === text)
        return;
      // @ts-ignore Outdated .d.ts
      this.snackbarLast.close("replaced");
    }

    let resolve;
    let promise = new Promise(_ => resolve = _);

    this.snackbarLastText = text;
    let el = mdcrd.snackbar(text, action, null)(null);
    document.body.appendChild(el);
    
    /** @type {import("@material/snackbar").MDCSnackbar} */
    let snackbar = el["MDCSnackbar"];

    // @ts-ignore Outdated .d.ts
    snackbar.open();

    el.addEventListener("MDCSnackbar:closed", e => {
      resolve(e["detail"].reason === "action");
      setTimeout(() => {
        el.remove();
      }, 2000);
    });
    snackbar["then"] = promise.then.bind(promise);
    snackbar["catch"] = promise.catch.bind(promise);

    this.snackbarLast = snackbar;
    return snackbar;
  }

  /**
   * @template T
   * @param {T & typeof CogAsset} type
   * @param {string} id
   * @param {string} [path]
   * @param {any[]} args
   * @returns {Promise<T["prototype"]>}
   */
  load(type, id, path, ...args) {
    return this.content && this.content.load(type, id, path, ...args);
  }

  /**
   * @template T
   * @param {T & typeof CogAsset} type
   * @param {string} id
   * @returns {T["prototype"]}
   */
  get(type, id) {
    return this.content && this.content.get(type, id);
  }

  async saveMap() {
    if (!this.session.map) {
      await this.dialog.saveMap();
      return;
    }

    this.session.save();
    let buffer = this.get(CogMap, "Map").toBinary();
    await new Promise((resolve, reject) => this.fs.writeFile(this.session.map, new Uint8Array(buffer), { }, err => err ? reject(err) : resolve()));
    this.snackbar({ text: "Map saved." });
  }

  updaterStep(autoDownload = false) {
    switch (this.updaterStatus) {
      case "idle":
        this.updaterCheck(autoDownload);
        break;

      case "available":
        this.updaterDownload();
        break;

      case "downloaded":
        this.updaterRestart();
        break;
    }
  }

  updaterCheck(autoDownload = false) {
    if (this.updaterStatus !== "idle" &&
        this.updaterStatus !== "available")
      return;
    this.updaterStatus = "unknown";
    this.electron.ipcRenderer.send("updaterCheck", autoDownload);
  }
  
  updaterDownload() {
    if (this.updaterStatus !== "available")
      return;
    this.updaterStatus = "unknown";
    this.electron.ipcRenderer.send("updaterDownload");
  }

  updaterRestart() {
    if (this.updaterStatus !== "downloaded")
      return;
    this.updaterStatus = "unknown";
    this.electron.ipcRenderer.send("updaterRestart");
  }

}

const cogwheel = window["cogwheel"] = Cogwheel.instance = new Cogwheel();
