
//@ts-check
const { app, protocol, BrowserWindow, ipcMain, dialog, shell } = require("electron");
const { autoUpdater } = require("electron-updater");
const log = require("electron-log");
const os = require("os");
const path = require("path");
const fs = require("fs");
const mime = require("mime-types");
const nconf = require("nconf");
const { decodeFSPath } = require("./interop.js");

process.env["COGWHEEL_APPROOT"] = __dirname;

log.transports.file.level = "info";

autoUpdater.logger = log;
autoUpdater.autoDownload = false;

// app.commandLine.appendSwitch("disable-frame-rate-limit");
app.commandLine.appendSwitch("disable-gpu-sandbox");
app.commandLine.appendSwitch("ignore-gpu-blacklist", "true");
app.commandLine.appendSwitch("enable-precise-memory-info");

if (os.platform() === "win32") {
  // Windows GL drivers can be horrible enough to deal with.
  // Let's just use the defaults for now.
  // app.commandLine.appendSwitch("use-gl", "egl"); // Pure "egl" is slow enough, "desktop" kills pixi.
  // app.commandLine.appendSwitch("use-angle", "default"); // "gles" kills DOM rendering, "gl" kills pixi.

} else {
  // "desktop" (default) works fine pretty much everywhere, flickers in VMs though.
  // "egl" works perfectly fine when the drivers support it. Not all do.
  // "angle" works same as egl with additional performance loss.
  // app.commandLine.appendSwitch("use-gl", "egl");
}

// Only use this if your GPU is dead.
// Unfortunately, it didn't help with someone's random DM crashes...
// app.commandLine.appendSwitch("disable-gpu");

// TODO: Make this optional. It shows total app performance rendering performance as well.
// app.commandLine.appendSwitch("show-fps-counter");


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
/** @type {Electron.BrowserWindow} */
let mainWindow;

mime.extensions["application/javascript"].push("mjs");
mime.types["mjs"] = "application/javascript";

nconf
  .argv()
  .env()
  .file({ file: "config.json" });

if (!nconf.get("celeste:dir") || !fs.existsSync(nconf.get("celeste:dir"))) {
  nconf.set("celeste:dir", "");

  // Auto-detect the game folder. Let the render process handle a miss.
  (async function findCelesteSteam() {
    let plat = os.platform();

    function exists(path) {
      return new Promise(resolve => !path ? resolve(false) : fs.exists(path, exists => resolve(exists)));
    }

    async function getSteamAppsCommon(root) {
      if (!root)
        return null;

      let dir;
      dir = path.join(root, "SteamApps");
      if (!(await exists(dir)))
        dir = path.join(root, "steamapps");
      if (!(await exists(dir)))
        return null;
      
      dir = path.join(dir, "common");
      if (!(await exists(dir)))
        return null;
      return dir;
    }

    let dirs = [];

    let steamDir = null;
    if (plat === "win32") {
      const Registry = require("winreg");
      steamDir =
      await new Promise(resolve =>
        new Registry({
          hive: Registry.HKLM,
          key: "\\SOFTWARE\\WOW6432Node\\Valve\\Steam"
        }).get(
          "InstallPath",
          (err, result) => err ? resolve(null) : resolve(result.value)
        )
      )
      ||
      await new Promise(resolve =>
        new Registry({
          hive: Registry.HKLM,
          key: "\\SOFTWARE\\Valve\\Steam"
        }).get(
          "InstallPath",
          (err, result) => err ? resolve(null) : resolve(result.value)
        )
      );

    } else if (plat === "darwin") {
      steamDir = path.join(process.env["HOME"], "Library/Application Support/Steam");
    } else {
      steamDir = path.join(process.env["HOME"], ".local/share/Steam");
      if (!(await exists(steamDir)))
        steamDir = path.join(process.env["HOME"], ".steam/steam");
    }

    dirs.push(steamDir);
    
    let configPath = path.join(steamDir, "config", "config.vdf");
    if (await exists(configPath)) {
      await new Promise(resolve => fs.readFile(configPath, (err, buffer) => {
        if (err)
          return resolve();

        let baseInstallFolderRegex = /BaseInstallFolder[^"]*"\s*"([^"]*)"/;
        dirs.push(...buffer.toString().split("\n").map(line => {
          let match = baseInstallFolderRegex.exec(line);
          return !match ? null : JSON.parse(`"${match[1]}"`);
        }).filter(dir => dir));
        resolve();
      }));
    }

    let gamedir = null;
    for (let dir of dirs) {
      if (!dir)
        continue;
      dir = path.join(await getSteamAppsCommon(dir) || dir, "Celeste");
      if (!(await exists(dir)))
        continue;
      gamedir = dir;
      break;
    }

    if (gamedir) {
      nconf.set("celeste:dir", gamedir);
      nconf.save("config.json");
    }
  })();
}

ipcMain.on("celesteSelect", (e) => {
  dialog.showOpenDialog(mainWindow, {
    title: "Open the Celeste folder",
    properties: ["openDirectory"],
  }, paths => e.sender.send("celesteSelectCB", paths));
});

ipcMain.on("showFileOpenDialog", (e, options) => {
  dialog.showOpenDialog(mainWindow, options, paths => e.sender.send("showFileOpenDialogCB", paths));
});

ipcMain.on("showFileSaveDialog", (e, options) => {
  dialog.showSaveDialog(mainWindow, options, paths => e.sender.send("showFileSaveDialogCB", paths));
});

autoUpdater.on("error", () => {
  mainWindow.webContents.send("updaterStatus", "error");
});

autoUpdater.on("update-not-available", () => {
  mainWindow.webContents.send("updaterStatus", "unavailable");
});

autoUpdater.on("update-available", () => {
  mainWindow.webContents.send("updaterStatus", "available", autoUpdater.autoDownload);
});

autoUpdater.on("update-downloaded", () => {
  mainWindow.webContents.send("updaterStatus", "downloaded");
});

ipcMain.on("updaterCheck", (e, autoDownload) => {
  autoUpdater.autoDownload = autoDownload ? true : false;
  try {
    mainWindow.webContents.send("updaterStatus", "checking");
    autoUpdater.checkForUpdates();
  } catch (e) {
    mainWindow.webContents.send("updaterStatus", "error");
  }
});

ipcMain.on("updaterDownload", () => {
  try {
    mainWindow.webContents.send("updaterStatus", "downloading");
    autoUpdater.downloadUpdate();
  } catch (e) {
    mainWindow.webContents.send("updaterStatus", "error");
  }
});

ipcMain.on("updaterRestart", () => {
  setImmediate(() => autoUpdater.quitAndInstall());
});


if ("registerStandardSchemes" in protocol)
  // @ts-ignore Electron 4.X
  protocol.registerStandardSchemes(["cog"], { secure: true });
else if ("registerSchemesAsPrivileged" in protocol)
  protocol.registerSchemesAsPrivileged([
    {
      scheme: "cog",
      privileges: {
        secure: true,
        bypassCSP: true,
        allowServiceWorkers: false,
        supportFetchAPI: true,
        corsEnabled: true
      }
    }
  ]);

function registerFSBufferProtocol(opt) {
  protocol.registerBufferProtocol(opt.name,
    (request, callback) => {
      let file = request.url.slice(opt.name.length + 3);
      file = opt.path(file);
      fs.exists(file, exists => {
        if (!exists) {
          callback({
            data: Buffer.from("Not found"),
            /*
            statusCode: 404,
            headers: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Expose-Headers": "*",
            },
            */
            mimeType: "text/error",
          });
          return;
        }

        fs.stat(file, (err, stats) => {
          if (err) {
            callback({
              data: Buffer.from("Can't stat"),
              /*
              statusCode: 503,
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Expose-Headers": "*",
              },
              */
              mimeType: "text/error",
            });
            return;
          }

          fs.readFile(file, (err, data) => {
            if (err) {
              callback({
                data: Buffer.from("Can't read"),
                /*
                statusCode: 503,
                headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Expose-Headers": "*",
                },
                */
                mimeType: "text/error",
              });
              return;
            }

            callback({
              data: data,
              // statusCode: 200,
              /*
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Expose-Headers": "*",
                "Content-Type": mime.lookup(request.url) || "application/octet-stream",
                "Content-Length": stats.size,
              },
              */
               mimeType: mime.lookup(request.url) || "application/octet-stream",
            });
          });

        });
      });
    },
    (error) => {
      if (!error)
        return;
      console.error(`Failed to register protocol ${opt.name}://`);
    }
  );
}

function getCogFilePath(file) {
  if (file.startsWith("/")) {
    file = file.slice(1);
  } else {
    if (file.startsWith("fs/"))
      file = decodeFSPath(file.slice(3));
    if (file.startsWith("celeste/"))
      return path.normalize(path.join(nconf.get("celeste:dir"), path.normalize(file.slice(8))));
  }
  file = path.normalize(file);
  if (path.isAbsolute(file))
    return file;
  return path.join(__dirname, file);
}

function createWindow() {
  registerFSBufferProtocol({
    name: "cog",
    path: getCogFilePath
  });

  // Create the browser window.
  mainWindow = new BrowserWindow({
    title: "Everest.Cogwheel",
    icon: path.join(__dirname, "icon.png"), // A slightly incorrect icon can kill some Linux DMs!
    width: 1024,
    height: 768,
    frame: true,
    backgroundColor: "#3b2d4a",
    webPreferences: {
      // preload: path.join(__dirname, "preload.js"),
      // @ts-ignore
      blinkFeatures: "OverlayScrollbars,PreciseMemoryInfo",
      overlayScrollbars: true,
      nodeIntegration: true,
      contextIsolation: false,
      experimentalFeatures: true,
    }
  });

  mainWindow.setMenuBarVisibility(!app.isPackaged);

  // and load the index.html of the app.
  mainWindow.loadURL("cog://./index.html");

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on("closed", () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  });

  let linkHandler = (e, url) => {
    if (url === mainWindow.webContents.getURL())
      return; 
    if (url.startsWith("cog://")) {
      e.preventDefault();
      shell.openItem(getCogFilePath(url.slice(3 + 3)))
    } else {
      e.preventDefault();
      shell.openExternal(url);
    }
  };
  mainWindow.webContents.on("will-navigate", linkHandler);
  mainWindow.webContents.on("new-window", linkHandler);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin")
    app.quit();
});

app.on("activate", () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null)
    createWindow();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
