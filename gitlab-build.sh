#!/bin/bash

sed -ri 's/"version": "(.*).0"/"version": "\1.'$CI_PIPELINE_IID'"/' package.json

npm i
npm run dist -- --publish never --win
