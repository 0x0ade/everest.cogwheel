//@ts-check
import { cogwheel } from "../main.js";
const { map, session } = cogwheel;
const { viewport } = cogwheel.pixi;

const EPSILON = 0.0000001;

export default class {
  icon = "select_all";
  index = 1000;

  get entries() {
    return [];
  }

  get layers() {
    return map.defaultEntityLayers;
  }

  constructor() {
    document.addEventListener("keydown", e => {
      if (e.key === "Control")
        this.draggingOffset = true;
      if (e.key === "Shift")
        this.selectingAdd = true;

      if (!this.dragging)
        return;
      
      let offs;
      switch (e.key) {
        case "ArrowUp": offs = [0, -8]; break;
        case "ArrowDown": offs = [0, 8]; break;
        case "ArrowLeft": offs = [-8, 0]; break;
        case "ArrowRight": offs = [8, 0]; break;
      }
      if (!offs)
        return;
      /** @type {AVec} */
      this.from = this.from.vadd(offs);
    });
    document.addEventListener("keyup", e => {
      if (e.key === "Control")
        this.draggingOffset = false;
      if (e.key === "Shift")
        this.selectingAdd = false;
    });

    document.addEventListener("keydown", e => {
      if (!map.selectedEntries.length)
        return;
      
      let dist = e.getModifierState("Control") ? 1 : 8;
      switch (e.key) {
        case "ArrowUp": this.selectedMove(true, 0, -dist); break;
        case "ArrowDown": this.selectedMove(true, 0, dist); break;
        case "ArrowLeft": this.selectedMove(true, -dist, 0); break;
        case "ArrowRight": this.selectedMove(true, dist, 0); break;
        case "Delete": this.selectedRemove(); break;
      }
    });
  }

  start() {
    this.preview = new PIXI.Container();
    this.preview.name = "preview";
    this.preview.parentGroup = map.getDepthGroup("overlay");

    this.previewBox = new PIXI.Graphics();
    this.previewBox.name = "box";
    this.preview.addChild(this.previewBox);

    viewport.addChild(this.preview);

    this.dragging = false;
    this.from = null;
    this.to = null;
    this.deltaRaw = new AVec(0, 0);
    this.deltaLast = new AVec(0, 0);
  }

  end() {
    viewport.removeChild(this.preview);
    map.selectedEntries = [];
  }

  /** @param {number} deltaTime */
  update(deltaTime) {
    // Remove and add so it always shows up on top.
    viewport.removeChild(this.preview);
    viewport.addChild(this.preview);
  }

  /** @param {AVec} at */
  move(at) {
    if (!this.dragging) {
      this.from = at;
    }

    this.preview.visible = this.selecting;

    if (this.selecting) {
      let { from, to } = this;
      if (!to)
        to = from;
      let min = from.vmap((x, i) => Math.floor(Math.min(x, to[i]))); // $$`floor(min(${from}, ${to}));
      let max = from.vmap((x, i) => Math.ceil(Math.max(x, to[i]))); // $$`ceil(max(${from}, ${to}))`;

      at = this.at = min;
      let size = this.size = max.vsub(min);

      this.preview.position.set(...at);

      this.previewBox.clear();
      this.previewBox.lineStyle(1, 0x00adee, 0.9);
      this.previewBox.beginFill(0x00adee, 0.3);
      // @ts-ignore
      this.previewBox.drawRect(0, 0, ...size);
    }
  }

  /** @param {AVec} at */
  down(at) {
    this.dragging = true;
    this.selecting = true;
    this.to = at;

    at = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...at))
      )
    ).vmap(x => Math.floor(x + EPSILON));

    if (!this.selectingAdd && map.selectedEntries.findIndex(({ entity, node }) => {
      if (!map.entityContainers.has(entity || (node ? node._entity : null)))
        return false;

      return map.isOverlap(at, node || entity);
    }) !== -1) {
      this.selecting = false;
    }
  }

  /** @param {AVec} at */
  up(at) {
    this.dragging = false;
    this.to = null;

    at = this.at;
    at = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...at))
      )
    ).vmap(x => Math.floor(x + EPSILON));

    if (this.selecting) {
      const size = this.size.vmap(x => Math.floor(x));
      const bounds = [...at, ...size];

      let selectedPrev = map.selectedEntries;
      map.selectEntities(null, session.layer, bounds);
      if (this.selectingAdd) {
        map.selectedEntries = Array.from(new Set([...selectedPrev, ...map.selectedEntries]));
      }

    } else {
      this.selectedMove(false, ...this.deltaLast.vmul(-1));
      this.selectedMove(true, ...this.deltaLast);
    }
    
    this.selecting = true;
    this.deltaRaw = new AVec(0, 0);
    this.deltaLast = new AVec(0, 0);
  }

  /**
   * @param {AVec} at
   * @param {AVec} delta
   */
  drag(at, delta) {
    if (this.draggingOffset) {
      this.from = this.from.vadd(delta);
    }
    this.to = at;

    if (!this.selecting) {
      this.selectedMove(false, ...this.deltaLast.vmul(-1));
      this.deltaRaw = this.deltaRaw.vadd(delta);
      this.deltaLast = this.deltaRaw;
      if (!this.draggingOffset)
        this.deltaLast = this.deltaLast.vmap(x => Math.round(x / 8) * 8);
      this.selectedMove(false, ...this.deltaLast);
    }
  }

  /**
   * @param {boolean} store
   * @param {number[]} dir
   */
  selectedMove(store, ...dir) {
    const vdir = new AVec(dir);
    const selected = map.selectedEntries;

    const move = (dir) => {
      for (let { entity, node } of selected) {
        if (entity) {
          new AVec(entity).vadd(dir).vcopyTo(entity);

        } else if (node) {
          entity = node._entity;
          new AVec(node).vadd(dir).vcopyTo(node);
        }

        if (entity)
          map.renderEntity(entity);
      }
    }

    if (store) {
      cogwheel.history.push(
        () => move(vdir.vmul(-1)),
        () => move(vdir),
      );
    } else {
      move(vdir);
    }
  }

  selectedRemove() {
    const selected = map.selectedEntries;

    /** @type {{ list: any[], item: any, index: number, entity: typeof selected[0]["entity"] }[]} */
    let entries = [];

    for (let { entity, node } of selected) {
      /** @type {any[]} */
      let list;
      let item;

      if (entity) {
        list = entity._level[entity._group];
        item = entity;

      } else if (node) {
        entity = node._entity;
        list = entity._nodes;
        item = node;

        let type = map.getEntityType(entity);
        if (type.nodesMin && entity._nodes.length - 1 < type.nodesMin) {
          list = entity._level[entity._group];
          item = entity;
        }
      }

      let index = list.indexOf(item);
      if (index === -1)
        continue;
      entries.push({
        list,
        item,
        index,
        entity
      });
    }

    entries = entries.sort((a, b) => b.index - a.index);
    let removed = false;
    cogwheel.history.push(
      () => {
        if (!removed)
          return;
        removed = false;
        for (let { list, item, index, entity } of entries) {
          if (index === -1)
            continue;
          list.splice(index, 0, item);
          map.renderEntity(entity);
        }
      },

      () => {
        if (removed)
          return;
        removed = true;
        for (let entry of entries) {
          let { list, item, index, entity } = entry;
          entry.index = index = list.indexOf(item);
          if (index === -1)
            continue;
          list.splice(index, 1);
          map.renderEntity(entity);
        }
      }
    );
  }
}
