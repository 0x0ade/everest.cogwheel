//@ts-check
import { cogwheel } from "../main.js";
const { map, session } = cogwheel;
const { viewport } = cogwheel.pixi;

const EPSILON = 0.0000001;

export default class {
  icon = "crop_square";
  index = 200;

  get entries() {
    return map.defaultTileEntries;
  }

  get layers() {
    return map.defaultTileLayers;
  }

  constructor() {
    document.addEventListener("keydown", e => {
      if (e.key === "Control")
        this.draggingOffset = true;

      if (!this.dragging)
        return;
      
      let offs;
      switch (e.key) {
        case "ArrowUp": offs = [0, -8]; break;
        case "ArrowDown": offs = [0, 8]; break;
        case "ArrowLeft": offs = [-8, 0]; break;
        case "ArrowRight": offs = [8, 0]; break;
      }
      if (!offs)
        return;
      /** @type {AVec} */
      this.from = this.from.vadd(offs);
    });
    document.addEventListener("keyup", e => {
      if (e.key === "Control")
        this.draggingOffset = false;
    });
  }

  start() {
    this.preview = new PIXI.Container();
    this.preview.name = "preview";
    this.preview.parentGroup = map.getDepthGroup("overlay");

    this.previewBox = new PIXI.Graphics();
    this.previewBox.name = "box";
    this.preview.addChild(this.previewBox);

    this.previewTile = new PIXI.Sprite();
    this.previewTile.name = "tile";
    this.previewTile.alpha = 0.3;
    this.preview.addChild(this.previewTile);

    /** @type {import("../../../js/content/autotiler.js").CogAutotiler} */
    // @ts-ignore
    this.autotilerBG = cogwheel.content.assets.get("BGTiles");
    /** @type {import("../../../js/content/autotiler.js").CogAutotiler} */
    // @ts-ignore
    this.autotilerFG = cogwheel.content.assets.get("FGTiles");

    viewport.addChild(this.preview);

    this.dragging = false;
    this.from = null;
    this.to = null;

    this.previewBackup = [];

  }

  end() {
    viewport.removeChild(this.preview);
  }

  /** @param {number} deltaTime */
  update(deltaTime) {
    // Remove and add so it always shows up on top.
    viewport.removeChild(this.preview);
    viewport.addChild(this.preview);
  }

  /**
   * @param {typeof map.currentLevel.solids} layer
   * @param {AVec} at
   * @param {AVec} size
   * @param {string | string[]} tile
   * @param {string[]} [copyTo]
   * @param {boolean} [render]
   */
  apply(layer, at, size, tile, copyTo, render = true) {
    for (let y = 0; y < size.y; y++) {
      for (let x = 0; x < size.x; x++) {
        if (!layer.isInside(at.x + x, at.y + y))
          continue;
        if (copyTo)
          copyTo[x + y * size.x] = layer.get(at.x + x, at.y + y);
        layer.set(at.x + x, at.y + y, typeof tile === "string" ? tile : tile[x + y * size.x]);
      }
    }
    if (render)
      map.renderTilemap(layer._level, layer, [...at.vsub(2), ...size.vadd(5)]);
  }

  /**
   * @param {AVec} at
   * @param {AVec} delta
   */
  move(at, delta) {
    if (!this.dragging) {
      this.from = at;
    }

    let { from, to } = this;
    if (!to)
      to = from;
    let min = from.vmap((x, i) => Math.floor(Math.min(x, to[i]) / 8) * 8); // $$`floor(min(${from}, ${to}) / 8) * 8`;
    let max = from.vmap((x, i) => Math.ceil(Math.max(x, to[i]) / 8) * 8); // $$`ceil(max(${from}, ${to}) / 8) * 8`;
    max = max.vmap((x, i) => min[i] === x ? x + 8 : x);

    at = this.at = min;
    let size = this.size = max.vsub(min);

    this.preview.position.set(...at);

    this.previewBox.clear();
    this.previewBox.lineStyle(1, 0x00adee, 0.9);
    this.previewBox.beginFill(0x00adee, 0.3);
    // @ts-ignore
    this.previewBox.drawRect(0, 0, ...size);

    // Temporarily render the tilemap as if the tiles were there.

    at = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...at))
      )
    ).vmap(x => Math.floor(x / 8 + EPSILON));
    size = this.size.vmap(x => Math.floor(x / 8));

    const level = map.currentLevel;
    /** @type {typeof level.solids} */
    const layer = level[session.layer];

    this.previewBackup.length = 0;

    this.apply(layer, at, size, session.place, this.previewBackup);
    this.apply(layer, at, size, this.previewBackup, null, false);
    cogwheel.pixi.renderer.once("postrender", () => {
      map.renderTilemap(null, layer, [...at.vsub(2), ...size.vadd(5)]);
    });
  }

  /** @param {AVec} at */
  down(at) {
    this.dragging = true;
    this.to = at;
  }

  /** @param {AVec} at */
  up(at) {
    this.dragging = false;
    this.to = null;

    at = this.at;
    at = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...at))
      )
    ).vmap(x => Math.floor(x / 8 + EPSILON));
    const size = this.size.vmap(x => Math.floor(x / 8));

    const level = map.currentLevel;
    /** @type {typeof level.solids} */
    const layer = level[session.layer];

    let old = new Array(size.x * size.y);
    this.apply(layer, at, size, session.place, old);
    const gen = (tile) => () => this.apply(layer, at, size, tile || old, null);
    cogwheel.history.push(
      gen(null),
      gen(session.place),
      false
    );
  }

  /**
   * @param {AVec} at
   * @param {AVec} delta
   */
  drag(at, delta) {
    if (this.draggingOffset) {
      this.from = this.from.vadd(delta);
    }
    this.to = at;
  }
}
