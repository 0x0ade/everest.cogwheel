//@ts-check
import { cogwheel } from "../main.js";
const { map, session } = cogwheel;
const { viewport } = cogwheel.pixi;

export default class {
  icon = "edit";
  index = 0;

  get entries() {
    return map.defaultTileEntries;
  }

  get layers() {
    return map.defaultTileLayers;
  }

  constructor() {
  }

  start() {
    this.preview = new PIXI.Container();
    this.preview.name = "preview";
    this.preview.parentGroup = map.getDepthGroup("overlay");

    this.previewBox = new PIXI.Graphics();
    this.previewBox.name = "box";
    this.previewBox.lineStyle(1, 0x00adee, 0.9);
    this.previewBox.beginFill(0x00adee, 0.3);
    this.previewBox.drawRect(0, 0, 8, 8);
    this.preview.addChild(this.previewBox);

    /** @type {import("../../../js/content/autotiler.js").CogAutotiler} */
    // @ts-ignore
    this.autotilerBG = cogwheel.content.assets.get("BGTiles");
    /** @type {import("../../../js/content/autotiler.js").CogAutotiler} */
    // @ts-ignore
    this.autotilerFG = cogwheel.content.assets.get("FGTiles");

    viewport.addChild(this.preview);

    /** @type {{ at: AVec, type: string }[]} */
    this.history = [];
  }

  end() {
    viewport.removeChild(this.preview);
  }

  /** @param {number} deltaTime */
  update(deltaTime) {
    // Remove and add so it always shows up on top.
    viewport.removeChild(this.preview);
    viewport.addChild(this.preview);
  }

  /** @param {AVec} at */
  move(at) {
    this.preview.position.set(...
      at.vmap(x => Math.floor(x / 8) * 8)
    );

    at = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...at))
      )
    ).vmap(x => Math.floor(x / 8));

    const level = map.currentLevel;
    /** @type {typeof level.solids} */
    const layer = level[session.layer];
    if (!layer.isInside(at.x, at.y))
      return;

    /** @type {[number, number, number, number]} */
    const bounds = [at.x - 2, at.y - 2, 5, 5];
    const old = layer.get(at.x, at.y);
    
    // Temporarily render the tilemap as if the tile was there.

    layer.set(at.x, at.y, session.place);
    map.renderTilemap(null, layer, bounds);
    layer.set(at.x, at.y, old);
    cogwheel.pixi.renderer.once("postrender", () => {
      map.renderTilemap(null, layer, bounds);
    });
  }

  /** @param {AVec} at */
  down(at) {
    this.drag(at);
  }

  /** @param {AVec} at */
  up(at) {
    this.drag(at);

    if (!this.history.length)
      return;
    
    const currentPlacement = session.place;
    const history = this.history;
    this.history = [];

    const level = map.currentLevel;
    /** @type {typeof level.solids} */
    const layer = level[session.layer];

    const gen = (type) => () => {
      for (let entry of history) {
        layer.set(entry.at.x, entry.at.y, type || entry.type);
        map.renderTilemap(level, layer, [entry.at.x - 2, entry.at.y - 2, 5, 5]);
      }
    };
    cogwheel.history.push(
      gen(null),
      gen(currentPlacement),
      false
    );
  }

  /** @param {AVec} at */
  drag(at) {
    const level = map.currentLevel;

    at = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...at))
      )
    ).vmap(x => Math.floor(x / 8));

    /** @type {typeof level.solids} */
    const layer = level[session.layer];
    if (!layer.isInside(at.x, at.y))
      return;

    const old = layer.get(at.x, at.y);
    if (old === session.place)
      return;
    layer.set(at.x, at.y, session.place);
    // map.renderTilemap(null, layer, [at.x - 2, at.y - 2, 5, 5]);
    this.history.push({ at, type: old });
  }
}
