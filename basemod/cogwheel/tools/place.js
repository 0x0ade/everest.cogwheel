//@ts-check
import { cogwheel } from "../main.js";
const { map, session } = cogwheel;
const { viewport } = cogwheel.pixi;

const EPSILON = 0.0000001;

/** @type {typeof import("../../../js/content/map.js").CogMap} */
let CogMap;

export default class {
  icon = "add";
  index = 500;

  get entries() {
    return map.defaultEntityEntries;
  }

  get layers() {
    return map.defaultEntityLayers;
  }

  constructor() {
    document.addEventListener("keydown", e => {
      if (e.key === "Control")
        this.draggingOffset = true;

      if (!this.dragging)
        return;
      
      let offs;
      switch (e.key) {
        case "ArrowUp": offs = [0, -8]; break;
        case "ArrowDown": offs = [0, 8]; break;
        case "ArrowLeft": offs = [-8, 0]; break;
        case "ArrowRight": offs = [8, 0]; break;
      }
      if (!offs)
        return;
      /** @type {AVec} */
      this.from = this.from.vadd(offs);

      this.dirty = 2;
    });
    document.addEventListener("keyup", e => {
      if (e.key === "Control")
        this.draggingOffset = false;
    });
  }

  start() {
    // @ts-ignore
    CogMap = cogwheel.content.types.get("CogMap");

    /** @type {string} */
    this.previewID = "";
    this.preview = null;

    this.from = null;
    this.to = null;
    this.dirty = 0;
  }

  end() {
    this.previewID = "";
    if (this.preview) {
      viewport.removeChild(this.preview);
      this.preview = null;
    }
  }

  genEntity() {
    this.previewID = cogwheel.session.place;
    if (!this.previewID) {
      this.type = null;
      this.entry = null;
      this.entity = null;
      return;
    }

    let indexOfSplit = this.previewID.indexOf(":");
    this.type = map.entityTypes.get(this.previewID.slice(0, indexOfSplit));
    this.entry = this.type.getEntry(this.previewID.slice(indexOfSplit + 1));
    this.entity = new CogMap.Entity(null, session.layer, this.type.id, this.entry.data);
    if (this.entry.nodes) {
      this.entity._nodes = this.entry.nodes.map(node => new CogMap.EntityNode(this.entity, ...node));
    }
  }

  genPreview() {
    if (this.preview) {
      viewport.removeChild(this.preview);
      this.preview = null;
    }

    if (!this.entity || this.dragging)
      return;

    this.preview = map.renderEntity(this.entity);
    this.preview.parentGroup = map.getDepthGroup("overlay");
    this.preview.alpha = 0.75;
    viewport.addChild(this.preview);
  }

  /** @param {number} deltaTime */
  update(deltaTime) {
    this.previewID = this.previewID || "";

    if (cogwheel.session.place !== this.previewID) {
      this.genEntity();
      this.genPreview();
    }

    if (!this.preview)
      return;

    // Remove and add so it always shows up on top.
    viewport.removeChild(this.preview);
    viewport.addChild(this.preview);
  }

  /**
   * @param {AVec} at
   * @param {AVec} delta
   */
  move(at, delta) {
    // FIXME: Store draggingOffsetFrom for whether draggingOffset was true during movement.

    let _at = at;
    if (!this.dragging) {
      this._from = at;
      if (this.draggingOffset) {
        this.from = at.vmap(x => Math.floor(x));
      } else {
        this.from = at.vmap(x => Math.floor(x / 8) * 8);
      }
    }

    let { from, to } = this;
    if (!to)
      to = from;
    let min = from.vmap((x, i) => Math.floor(Math.min(x, to[i]))); // $$`floor(min(${from}, ${to}));
    let max = from.vmap((x, i) => Math.ceil(Math.max(x, to[i]))); // $$`ceil(max(${from}, ${to}))`;
    max = max.vmap((x, i) => min[i] === x ? x + 8 : x);

    at = this.at = min;
    let size = this.size = max.vsub(min);

    if (!this.entity)
      return;

    if ("width" in this.entity) {
      this.entity.width = size.x;
    } else if (to.x < from.x) {
      at.x += size.x;
    }

    if ("height" in this.entity) {
      this.entity.height = size.y;
    } else if (to.y < from.y) {
      at.y += size.y;
    }

    if (!("width" in this.entity) && !("height" in this.entity)) {
      at = this.entity._nodes.length ? this._from : _at;
      if (this.draggingOffset) {
        at = at.vmap(x => Math.round(x));
      } else {
        at = at.vmap(x => Math.round(x / 8) * 8);
      }
    }

    this.at = at;

    let atLocal = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...at))
      )
    ).vmap(x => Math.floor(x + EPSILON));
    
    atLocal.vcopyTo(this.entity);
    if (this.entity._nodes.length) {
        to = this._to || to;
        if (this.draggingOffset) {
          to = to.vmap(x => Math.round(x));
        } else {
          to = to.vmap(x => Math.round(x / 8) * 8);
        }
      for (let i in this.entry.nodes) {
        new AVec(this.entry.nodes[i]).vadd(to).vsub(from).vadd(atLocal).vcopyTo(this.entity._nodes[i]);
      }
    }

    if (!this.preview)
      return;

    if (delta.x !== 0 || delta.y !== 0)
      this.genPreview();
    this.preview.position.set(...atLocal.vadd(map.currentLevel));
  }

  /** @param {AVec} at */
  down(at) {
    this.dragging = true;

    this.genPreview();

    let entity = this.entity;
    if (!entity)
      return;

    let pos = new AVec(
      map.currentLevelContainer.toLocal(
        viewport.toScreen(new PIXI.Point(...this.at))
      )
    ).vmap(x => Math.round(x + EPSILON));
    pos.vcopyTo(entity);

    let currentLevel = map.currentLevel;
    /** @type {typeof currentLevel.entities} */
    let list = currentLevel[entity._group];

    entity.id = map.getNewID();
    entity._level = currentLevel;
    cogwheel.history.push(
      () => {
        list.splice(list.indexOf(entity), 1);
        map.renderEntity(entity);
      },
      () => {
        list.push(entity);
        map.renderEntity(entity);
      }
    );
  }

  /** @param {AVec} at */
  up(at) {
    this.dragging = false;
    this.to = null;
    this._to = null;

    this.genEntity();
    this.genPreview();
  }

  /**
   * @param {AVec} at
   * @param {AVec} delta
   */
  drag(at, delta) {
    let dirty = this.dirty;
    if (dirty)
      this.dirty--;

    this._to = at;
    if (this.draggingOffset) {
      // this.from = this.from.vadd(delta);
      this.to = at.vmap(x => Math.ceil(x));
    } else {
      this.to = at.vmap(x => Math.ceil(x / 8) * 8);
    }

    if (!this.entity)
      return;

    if (delta.x !== 0 || delta.y !== 0 || dirty)
      map.renderEntity(this.entity);
  }
}
