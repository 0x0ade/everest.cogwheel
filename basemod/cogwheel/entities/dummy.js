//@ts-check
import { cogwheel } from "../main.js";
export default class {
  getBounds(entity) {
    return [
      -2, -2,
      4, 4
    ];
  }

  getNodeBounds(node) {
    return [
      -2, -2,
      4, 4
    ];
  }

  render(entity) {
    const container = new PIXI.Container();

    let graphics = new PIXI.Graphics();
    graphics.lineStyle(
      1, 0x00adee,
      0.3, 0
    );
    container.addChild(graphics);

    let sprite = new PIXI.Sprite(PIXI.Texture.WHITE);
    sprite.tint = 0x00adee;
    sprite.position.set(-2, -2);
    sprite.scale.set(4 / 16, 4 / 16);
    container.addChild(sprite);

    graphics.moveTo(0, 0);
    for (let node of entity._nodes) {
      let sprite = new PIXI.Sprite(PIXI.Texture.WHITE);
      sprite.tint = 0x00adee;
      sprite.alpha = 0.3;
      sprite.position.set(
        node.x - entity.x - 2,
        node.y - entity.y - 2,
      );
      sprite.scale.set(4 / 16, 4 / 16);
      container.addChild(sprite);

      // graphics.moveTo(0, 0);
      graphics.lineTo(sprite.x + 2, sprite.y + 2);
    }

    return container;
  }
}
