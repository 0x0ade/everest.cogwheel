//@ts-check
import { cogwheel } from "../main.js";

const REGEX_NUMSUFFIX = /\d+$/;

export class _spikes {
  dir = this.constructor.name[0] === "_" ? "" : this.constructor.name.slice(6).toLowerCase();
  dimensionSize = (this.dir === "left" || this.dir === "right") ? "height" : "width";

  get entries() {
    if (!this.dir)
      return;

    let entries = {};

    for (let key of cogwheel.atlasGameplay.textures.keys()) {
      if (!key.startsWith("danger/spikes/"))
        continue;
      key = key.slice(14);
      key = key.replace(REGEX_NUMSUFFIX, "");

      let indexOfDir = key.indexOf("_");
      let type = key.slice(0, indexOfDir);
      if (key in entries)
        continue;

      let dir = key.slice(indexOfDir + 1);
      if (dir !== this.dir)
        continue;
      entries[type] = this.getEntry(type);
    }

    return entries;
  }

  getEntry(key) {
    let dir = this.dir;
    let entry = {
      name: `Spikes (${cogwheel.utils.humanify(key)} ${cogwheel.utils.humanify(dir)})`,
      data: {
        type: key,
        dir
      }
    };
    entry.data[this.dimensionSize] = 8;
    return entry;
  }

  render(entity) {
    let isTentacles = entity.type === "tentacles";
    let textures = cogwheel.atlasGameplay.getSubtextures(
      isTentacles ? "danger/tentacles" :
      "danger/spikes/" + ((entity.type && entity.type !== "default") ? entity.type : "default") + "_" + this.dir);
    if (!textures.length)
      return cogwheel.utils.sprite(null);
    
    let sizeSpike = isTentacles ? 16 : 8;
    let size = entity[this.dimensionSize] / sizeSpike;

    let container = new PIXI.Container();

    let prng = cogwheel.utils.prng(entity.id);
    for (let i = 0; i < size; i++) {
      let sprite = cogwheel.utils.sprite(prng.pick(textures));

      if (isTentacles) {
        sprite.anchor.set(0.5, 1);
        switch (this.dir) {
          case "up":
            sprite.rotation = 0;
            sprite.position.set((i + 0.5) * 16, +17);
            break;

          case "down":
            sprite.rotation = Math.PI;
            sprite.position.set((i + 0.5) * 16, -17);
            break;

          case "left":
            sprite.rotation = -Math.PI / 2;
            sprite.position.set(+17, (i + 0.5) * 16);
            break;

          case "right":
            sprite.rotation = Math.PI / 2;
            sprite.position.set(-17, (i + 0.5) * 16);
            break;
        }

      } else {
        switch (this.dir) {
          case "up":
            sprite.anchor.set(0.5, 1);
            sprite.position.set((i + 0.5) * 8, +1);
            break;

          case "down":
            sprite.anchor.set(0.5, 0);
            sprite.position.set((i + 0.5) * 8, -1);
            break;

          case "left":
            sprite.anchor.set(1, 0.5);
            sprite.position.set(+1, (i + 0.5) * 8);
            break;

          case "right":
            sprite.anchor.set(0, 0.5);
            sprite.position.set(-1, (i + 0.5) * 8);
            break;
        }
      }

      container.addChild(sprite);
    }

    return container;
  }
}

export class spikesUp extends _spikes { };
export class spikesDown extends _spikes { };
export class spikesLeft extends _spikes { };
export class spikesRight extends _spikes { };
