//@ts-check
import { cogwheel } from "../main.js";
export default class {
  nodesMin = 1;
  nodesMax = 1;

  entries = [
    {
      name: "Wire",
      data: { },
      nodes: [
        [ 0, 0 ]
      ]
    }
  ]

  getBounds(entity) {
    // @ts-ignore
    return this.getNodeBounds(entity);
  }

  render(entity) {
    let graphics = new PIXI.Graphics();
    graphics.lineStyle(
      1, entity.color ? cogwheel.utils.hexcolor(entity.color) : 0x595866,
      1, 0
    );

    let from = new AVec(0, 0);
    let to = new AVec(entity._nodes[0]).vsub(entity);
    let curve = cogwheel.utils.curve(
      from, to,
      $$`(${from} + ${to}) / 2 + [0, 24]`
    );

    graphics.moveTo(0, 0);
    for (let i = 0; i <= 16; i++) {
      let p = curve(i / 16);
      // @ts-ignore p is guaranteed to be Vec2
      graphics.lineTo(...p);
    }

    return graphics;
  }
}
