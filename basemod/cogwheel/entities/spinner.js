//@ts-check
import { cogwheel } from "../main.js";

const REGEX_NUMSUFFIX = /\d+$/;

export default class {
  get entries() {
    let entries = {
      "__dust": this.getEntry("__dust")
    };

    for (let key of cogwheel.atlasGameplay.textures.keys()) {
      if (!key.startsWith("danger/crystal/fg_"))
        continue;
      key = key.slice(18);
      key = key.replace(REGEX_NUMSUFFIX, "");

      if (key in entries)
        continue;
      entries[key] = this.getEntry(key);
    }

    return entries;
  }

  getEntry(key) {
    if (key === "__dust") {
      return {
        name: "Dust Spinner",
        data: { dust: true }
      };
    }

    return {
      name: `Crystal Spinner (${cogwheel.utils.humanify(key)})`,
      data: {
        color: key
      }
    };
  }

  getBounds(entity) {
    return [
      -8, -8,
      16, 16
    ]
  }

  render(entity) {
    let prng = cogwheel.utils.prng(entity.x, entity.y);

    if (entity.dust) {
      let container = new PIXI.Container();

      let base = cogwheel.utils.sprite(prng.pick(cogwheel.atlasGameplay.getSubtextures("danger/dustcreature/base")));
      base.anchor.set(0.5, 0.5);
      container.addChild(base);

      let center = cogwheel.utils.sprite(prng.pick(cogwheel.atlasGameplay.getSubtextures("danger/dustcreature/center")));
      center.anchor.set(0.5, 0.5);
      container.addChild(center);

      return container;
    }

    let textures = cogwheel.atlasGameplay.getSubtextures("danger/crystal/fg_" + (entity.color || "blue").toLowerCase());
    textures = textures.length === 0 ? cogwheel.atlasGameplay.getSubtextures("danger/crystal/fg_blue") : textures;
    let sprite = cogwheel.utils.sprite(prng.pick(textures));
    sprite.anchor.set(0.5, 0.5);
    return sprite;
  }
}