//@ts-check
import { cogwheel } from "../main.js";
export class strawberry {
  partStraw = "straw";
  partGhost = "ghost";
  partIdle = "idle00";
  partNormal = "normal00";
  partWings = "wings01";
  winged = false;
  dashless = false;

  entries = [
    {
      name: "Strawberry",
      data: { }
    },
    {
      name: "Strawberry (Winged)",
      data: { winged: true }
    }
  ]
  
  getBounds(entity) {
    return [
      -7, -7,
      14, 14
    ]
  }

  getNodeBounds(node) {
    return [
      -5, -5,
      9, 12
    ]
  }

  render(entity) {
    const container = new PIXI.Container();

    let graphics = new PIXI.Graphics();
    graphics.lineStyle(
      1, 0xedfa33,
      0.3, 0
    );
    container.addChild(graphics);

    let winged = entity.winged || this.winged;
    let sprite = cogwheel.utils.sprite(`collectables/${entity._nodes.length ? this.partGhost : this.partStraw}berry/${entity._nodes.length ? this.partIdle : winged ? this.partWings : this.partNormal}`);
    sprite.anchor.set(0.5, 0.5);
    if (this.dashless)
      sprite.tint = 0x00adee;
    container.addChild(sprite);

    for (let node of entity._nodes) {
      let sprite = cogwheel.utils.sprite("collectables/strawberry/seed00");
      sprite.position.set(
        node.x - entity.x,
        node.y - entity.y,
      );
      sprite.anchor.set(0.5, 0.5);
      container.addChild(sprite);

      graphics.moveTo(0, 0);
      graphics.lineTo(sprite.x, sprite.y);
    }

    return container;
  }
}

export class goldenBerry extends strawberry {
  partStraw = "gold";
  partGhost = "ghostgold";
  partNormal = "idle00";

  entries = [
    {
      name: "Golden Strawberry",
      data: { }
    },
    {
      name: "Golden Strawberry (Winged)",
      data: { winged: true }
    }
  ]
}

export class memorialTextController extends goldenBerry {
  winged = true;
  dashless = true;

  entries = [
    {
      name: "Dashless Strawberry",
      data: { }
    }
  ]
}
