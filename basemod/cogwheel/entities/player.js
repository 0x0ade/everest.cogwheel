//@ts-check
import { cogwheel } from "../main.js";
export default class {
  entries = [
    {
      name: "Player",
      data: { }
    }
  ]

  getBounds(entity) {
    return [
      -8, -18,
      16, 18
    ]
  }

  render(entity) {
    let sprite = cogwheel.utils.sprite("characters/player/sitDown00");
    sprite.anchor.set(0.5, 1);
    return sprite;
  }
}
