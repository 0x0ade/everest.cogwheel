//@ts-check
import { cogwheel } from "../main.js";
export default class {
  minNodes = 0;
  maxNodes = 0;

  getEntry(key) {
    return {
      name: key,
      data: {
        texture: key,
        scaleX: 1,
        scaleY: 1
      }
    }
  }

  render(entity) {
    let sprite = cogwheel.utils.sprite("decals/" + entity.texture);
    sprite.anchor.set(0.5, 0.5);
    sprite.scale.set(entity.scaleX, entity.scaleY);
    return sprite;
  }
}
