//@ts-check
import { cogwheel } from "../main.js";
export default class {
  get entries() {
    let entries = {};

    for (let key of cogwheel.atlasGameplay.textures.keys()) {
      if (!key.startsWith("objects/jumpthru/"))
        continue;
      key = key.slice(17);
      entries[key] = this.getEntry(key);
    }

    return entries;
  }

  getEntry(key) {
    return {
      name: `Jumpthru (${cogwheel.utils.humanify(key)})`,
      data: {
        texture: key,
        width: 8
      }
    };
  }

  render(entity) {
    let texture = cogwheel.atlasGameplay.get("objects/jumpthru/" + ((entity.texture && entity.texture !== "default") ? entity.texture : "wood"));
    if (!texture)
      return cogwheel.utils.sprite(null);

    let container = new PIXI.Container();

    let twidth = Math.floor(texture.width / 8);
    let columns = Math.floor(entity.width / 8);
    let prng = cogwheel.utils.prng(entity.id);
    for (let i = 0; i < columns; i++) {
      let tx;
      let ty;
      if (i === 0) {
        tx = 0;
        ty = cogwheel.map.isSolid(entity._level, entity.x - 1, entity.y) ? 0 : 1;
      } else if (i === columns - 1) {
        tx = twidth - 1;
        ty = cogwheel.map.isSolid(entity._level, entity.x + entity.width + 1, entity.y) ? 0 : 1;
      } else {
        tx = 1 + Math.floor(prng() * (twidth - 2));
        ty = Math.round(prng());
      }
      
      let sprite = cogwheel.utils.sprite(texture.getSubtexture(tx * 8, ty * 8, 8, 8));
      sprite.position.x = i * 8;
      container.addChild(sprite);
    }

    return container;
  }
}
