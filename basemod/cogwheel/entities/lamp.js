//@ts-check
import { cogwheel } from "../main.js";
export default class {
  entries = [
    {
      name: "Lamp",
      data: {
        broken: false
      }
    },

    {
      name: "Lamp (Broken)",
      data: {
        broken: true
      }
    }
  ]

  render(entity) {
    let sprite = cogwheel.utils.sprite(
      cogwheel.atlasGameplay.get("scenery/lamp").getSubtexture(entity.broken ? 16 : 0, 0, 16, 80)
    );
    sprite.anchor.set(0.5, 1);
    return sprite;
  }
}
