//@ts-check
import { cogwheel } from "../main.js";
export class spring {
  entries = [
    {
      name: "Spring (Up)",
      data: { }
    }
  ]

  constructor() {
    if (this.constructor.name.startsWith("wallSpring"))
      this.dir = this.constructor.name.replace("Spring", "");
    else
      this.dir = "floor";
  }

  getBounds(entity) {
    return [
      -6, -3,
      12, 3
    ]
  }

  render(entity) {
    let sprite = cogwheel.utils.sprite("objects/spring/00");
    sprite.anchor.set(0.5, 1);
    return sprite;
  }
}

export class wallSpringLeft extends spring {
  entries = [
    {
      name: "Spring (Right)",
      data: { }
    }
  ]

  getBounds(entity) {
    return [
      0, -6,
      3, 12
    ]
  }

  render(entity) {
    let sprite = super.render(entity);
    sprite.rotation = Math.PI / 2;
    return sprite;
  }
}

export class wallSpringRight extends spring {
  entries = [
    {
      name: "Spring (Left)",
      data: { }
    }
  ]

  getBounds(entity) {
    return [
      -3, -6,
      3, 12
    ]
  }

  render(entity) {
    let sprite = super.render(entity);
    sprite.rotation = -Math.PI / 2;
    return sprite;
  }
}
