//@ts-check
import { cogwheel } from "../main.js";
export default class {
  render(entity) {
    let sprite = cogwheel.utils.sprite(entity.twoDash ? "objects/refillTwo/idle00" : "objects/refill/idle00");
    sprite.anchor.set(0.5, 0.5);
    return sprite;
  }
}
